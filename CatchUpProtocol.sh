#! /bin/sh

find ../protocol -name "*.thrift" -exec thrift --gen csharp {} \;
rm -rf Assets/OrbitX/Packets
mv gen-csharp/CloopOnline/Packets Assets/OrbitX
rm -rf gen-csharp
