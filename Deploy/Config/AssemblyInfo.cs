﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("OrbitX Unity SDK")]
[assembly: AssemblyDescription("OrbitX SDK for Unity3D platform")]
[assembly: AssemblyCompany("OrbitX")]
[assembly: AssemblyProduct("orbitx-unity-sdk")]
[assembly: AssemblyVersion("1.2.3.4")]
[assembly: AssemblyFileVersion("5.6.7.8")]
