#! /bin/sh

VERSION=""
TRIM_VERSION=""
ASSEMBLY_FILE="Assets/OrbitX/AssemblyInfo.cs"
SetupVersion()
{
	if [ -z "$1" ] ; then
	  echo "Supply version dumbass."
	  exit 1
	fi

	IFS='.' read -ra parts <<< "$1"
	if [ ${#parts[@]} != 4 ] ; then
		echo "Version should have 4 parts dumbass. Like 1.0.0.0"
		exit 1
	fi
	
	VERSION="$1"
	TRIM_VERSION=`echo "$VERSION" | sed -s 's/\.0$//g' | sed -s 's/\.0$//g'`
	sed "s|Version.*$|Version\(\"$VERSION\"\)\]|g" < Deploy/Config/AssemblyInfo.cs > $ASSEMBLY_FILE
}


PLAYTIME_SOURCES="playtime_sources"
ListSources()
{
	TEMP_FILE="temp_list"
	find Assets/OrbitX -name *.cs | sed 's|/|\\|g' | sed 's|\(.*\)|    <Compile Include=\"\1\" />|g' > $TEMP_FILE
	cat $TEMP_FILE | grep -v 'Editor' > $PLAYTIME_SOURCES
	rm -f $TEMP_FILE
}

SOLUTION_FILE="orbitx-sdk-unity.sln"
PROJECT_FILE="orbitx-sdk-unity.CSharp.csproj"
BuildSolutionFile()
{
	RAW_PROJECT_FILE="Deploy/Config/sdk-unity.CSharp.csproj.inc"
	cp -f Deploy/Config/sdk-unity.sln.inc $SOLUTION_FILE

	while IFS='' read -r line ; do
	  if [ "$line" = "FILE_LIST" ]; then
	    cat "$1"
	  else
	    echo "$line"
	  fi
	done  < $RAW_PROJECT_FILE > $PROJECT_FILE
}

BuildSolution()
{
	rm -rf Deploy/Build
	export msBuildDir=$WINDIR/Microsoft.NET/Framework/v4.0.30319
	$msBuildDir/msbuild.exe $SOLUTION_FILE "//p:Configuration=Debug" 
}

ILMerge()
{
	OPTIONS="/ndebug:false /lib:Deploy\\Build /attr:Deploy\\Build\\OrbitX-SDK.dll"
	OUTPUT="/out:Deploy\\OrbitX-Unity-SDK-$TRIM_VERSION.dll"
	INPUT="Deploy\\Build\\Thrift.dll Deploy\\Build\\OrbitX-SDK.dll Deploy\\Build\\Newtonsoft.Json.dll"
	Deploy/ThirdParty/ILMerge.exe $OPTIONS $OUTPUT $INPUT
}

GeneratePlayDLL()
{
	BuildSolutionFile $PLAYTIME_SOURCES
	BuildSolution
	ILMerge
}

CleanUp()
{
	OUTPUT_FOLDER="Deploy/Build-$TRIM_VERSION/OrbitX"
	mkdir -p $OUTPUT_FOLDER/Editor

	mv Deploy/OrbitX-Unity-SDK-$TRIM_VERSION.dll $OUTPUT_FOLDER
	cp -f Assets/OrbitX/Objects/Editor/SyncObjectEditor.cs $OUTPUT_FOLDER/Editor

	rm -f $SOLUTION_FILE $PROJECT_FILE $PLAYTIME_SOURCES $ASSEMBLY_FILE
	rm -rf Deploy/Build
}

cd ..
SetupVersion "$@"
ListSources
GeneratePlayDLL
CleanUp
