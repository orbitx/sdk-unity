﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
using System;
using System.IO;

[CustomEditor(typeof(TestManager))]
public class TestManagerEditor : Editor
{
    #region Scan
    private void UpdateTestList()
    {
        Undo.RecordObject(target, "scan and update tests");

        var scenarios = TestManager.Scenarios.OrderBy(s => s.ToString()).ToList();
        var mgr = target as TestManager;
        var memo = new List<string>(mgr.MemorizedScenarios);
        var testlist = new List<string>(mgr.TestList);
        foreach (var name in mgr.MemorizedScenarios)
            if (!scenarios.Exists(s => s.ToString() == name))
            {
                memo.Remove(name);
                testlist.Remove(name);
            }

        foreach (var scenario in scenarios)
            if (!memo.Contains(scenario.ToString()))
            {
                memo.Add(scenario.ToString());
                testlist.Add(scenario.ToString());
            }

        memo.Sort();
        testlist.Sort();

        mgr.MemorizedScenarios = memo.ToArray();
        mgr.TestList = testlist.ToArray();
    }
    #endregion Scan


    #region Display
    public override void OnInspectorGUI()
    {
        var mgr = target as TestManager;
        var memo = mgr.MemorizedScenarios;
        var testlist = mgr.TestList;

        GUILayout.Space(10);

        if (Application.isPlaying)
            DisplayInPlayMode(mgr, memo, testlist);
        else
            DisplayInEditMode(mgr, memo, testlist);

        GUILayout.Space(10);
    }

    private Dictionary<int, bool> mFoldouts;

    private void DisplayInEditMode(TestManager mgr, string [] memo, string [] testlist)
    {
        if (GUILayout.Toggle(mgr.BeginOnStart, " Run selected tests on start") != mgr.BeginOnStart)
        {
            Undo.RecordObject(target, "change test run settings");
            mgr.BeginOnStart = !mgr.BeginOnStart;
        }
        if (GUILayout.Toggle(mgr.SkipLongTests, " Skip long tests") != mgr.SkipLongTests)
        {
            Undo.RecordObject(target, "toggle skip long tests");
            mgr.SkipLongTests = !mgr.SkipLongTests;
        }

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("All"))
            SetAllSelection(true);
        if (GUILayout.Button("None"))
            SetAllSelection(false);
        if (GUILayout.Button("Scan"))
        {
            TestManager.ScanAllTests();
            UpdateTestList();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(15);
        EditorGUILayout.LabelField("Tests");

        int group_base = 0;
        for (int i = 0; i < memo.Length; ++i)
        {
            var name = memo[i];
            var number = int.Parse(name) / 10 * 10;
            if (number > group_base)
            {
                group_base = number;
                var foldout = mFoldouts.ContainsKey(group_base) ? mFoldouts[group_base] : true;
                var newFoldout = EditorGUILayout.Foldout(foldout, TestGroupNames.Get(group_base));
                if(newFoldout != foldout)
                {
                    mFoldouts[group_base] = newFoldout;
                    SaveFoldouts();
                }
            }

            if (!mFoldouts.ContainsKey(group_base) || mFoldouts[group_base])
            {
                var prevCheck = testlist.Contains(name);
                GUILayout.BeginHorizontal();
                if (GUILayout.Toggle(prevCheck, "", GUILayout.Width(15)) != prevCheck)
                    ToggleTestItem(name, !prevCheck);
                GUILayout.Label("#" + name);
                GUILayout.EndHorizontal();
            }
        }
    }

    private string GetFoldoutSavePath()
    {
        var result = Path.Combine(Application.dataPath, "Tests");
        result = Path.Combine(result, "Editor");
        result = Path.Combine(result, "Foldouts.json");
        return result;
    }

    private void SaveFoldouts()
    {
        File.WriteAllText(GetFoldoutSavePath(), JSON.ToJson(mFoldouts));
    }

    private void OnEnable()
    {
        string contents = "{}";
        try
        {
            contents = File.ReadAllText(GetFoldoutSavePath());
        }
        catch
        { }

        mFoldouts = JSON.Parse<Dictionary<int, bool>>(contents);
    }
    

    private void DisplayInPlayMode(TestManager mgr, string[] memo, string[] testlist)
    {
        if (Application.isPlaying && GUILayout.Button("Run"))
            TestManager.RunAll();

        GUILayout.Space(15);
        EditorGUILayout.LabelField("Tests");

        GUI.color = Color.green;
        foreach (var name in memo)
            if(testlist.Contains(name))
                EditorGUILayout.LabelField("#" + name);
        GUI.color = Color.white;
    }
    #endregion Display


    #region Handlers
    private void ToggleTestItem(string name, bool tick)
    {
        var action = tick ? "enable test #" + name : "disable test #" + name;
        Undo.RecordObject(target, action);

        var mgr = (target as TestManager);
        var testlist = mgr.TestList.ToList();
        if (tick)
            testlist.Add(name);
        else
            testlist.Remove(name);
        mgr.TestList = testlist.ToArray();
    }

    private void SetAllSelection(bool selected)
    {
        var action = selected ? "select all tests" : "deselect all tests";
        Undo.RecordObject(target, action);

        var mgr = (target as TestManager);
        mgr.TestList = selected
            ? new List<string>(mgr.MemorizedScenarios).ToArray()
            : new string[] { };
    }
    #endregion Handlers
}
