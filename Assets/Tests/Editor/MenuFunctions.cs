﻿using UnityEditor;

namespace OrbitXSDK
{
    static public class MenuFunctions
    {
        [MenuItem("OrbitX/Clear Local Data")]
        static public void ClearLocalData()
        {
            LocalData.EditorClearCommand();
        }
    }
}
