﻿using OrbitXSDK;

internal class LeaveSimulator : Scenario
{
    internal void SkipToFoundActiveSessions(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenSessionStartedForTwo(api1, api2, () =>
        {
            api1.Events.OnConnectionStateChanged.TearDown();
            api1.Disconnect();
            UnityBroker.PostToUnityDelayed(100, () => ContinueSimulationAfterDisconnection(api1));
        });
    }

    private void ContinueSimulationAfterDisconnection(OrbitXAPI api)
    {
        WhenConnected(api, () => { });
        api.Connect();
    }
}

[ScenarioTitle("SDK receives 'active_sessions'")]
internal class Test_310 : LeaveSimulator
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        SkipToFoundActiveSessions(api1, api2);

        api1.Events.OnActiveSessionFound += eventArgs =>
        {
            Assert.That(eventArgs.Sessions.Length, Is.EqualTo(1));
            eventArgs.Decide(eventArgs.Sessions[0].SessionID, ActiveSessionDecision.Discard);
            Test.Pass();
        };
    }
}

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Response: Join")]
internal class Test_311 : LeaveSimulator
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        SkipToFoundActiveSessions(api1, api2);

        api1.Events.OnActiveSessionFound += eventArgs =>
        {
            Assert.That(eventArgs.Sessions.Length, Is.EqualTo(1));

            api1.Events.OnMatchmakeDone.TearDown();
            api1.Events.OnMatchmakeDone += args =>
            {
                Assert.That((int)args.Tag, Is.EqualTo(234));
                Test.Pass();
            };
            eventArgs.Decide(eventArgs.Sessions[0].SessionID, ActiveSessionDecision.Rejoin, 234);
        };
    }
}

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Response: Leave")]
internal class Test_312 : LeaveSimulator
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        SkipToFoundActiveSessions(api1, api2);

        api1.Events.OnActiveSessionFound += eventArgs =>
        {
            Assert.That(eventArgs.Sessions.Length, Is.EqualTo(1));
            eventArgs.Decide(eventArgs.Sessions[0].SessionID, ActiveSessionDecision.Discard);
        };
    }
    
    private void TimeoutHandler(OrbitXAPI api1, OrbitXAPI api2)
    {
        Test.Pass();
    }
}
