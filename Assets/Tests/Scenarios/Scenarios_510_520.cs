﻿using OrbitXSDK;
using System.Linq;

[ScenarioTitle("Set properties")]
internal class Test_510 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            api.Session.Properties["hello"] = "world";
            Assert.That(api.Session.Properties["hello"].AsString, Is.EqualTo("world"));
            Test.Pass();
        });

        api.Connect();
    }
}

[ScenarioTitle("Try set properties")]
internal class Test_511 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            api.Session.Properties.TryToSet("hello", "world", null);
            Assert.That(api.Session.Properties["hello"].AsString, Is.EqualTo("world"));
            Test.Pass();
        });

        api.Connect();
    }
}

[ScenarioTitle("When Properties change")]
internal class Test_513 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenConnected(api1, () =>
        {
            api1.CreateSession(SampleMatchmakeOptions, "test_session");
        });

        WhenJoinedSession(api1, () =>
        {
            api2.Connect();
        });

        WhenConnected(api2, () =>
        {
            api2.GetSessionList(sessions =>
            {
                api2.JoinSessionByID(sessions.First(s => s.Label == "test_session").SessionID);
            });
        });

        WhenJoinedSession(api2, () =>
        {
            api2.Session.Properties["hello"] = "world";
        });

        api1.Events.OnSessionPropertyChanged += eventArgs =>
        {
            Assert.That(eventArgs.PropertyChanges.Length, Is.EqualTo(1));
            Assert.That(eventArgs.PropertyChanges[0].Modification, Is.EqualTo(Modification.Insert));
            Assert.That(eventArgs.PropertyChanges[0].NewValue.AsString, Is.EqualTo("world"));
            Test.Pass();
        };

        api1.Connect();
    }
}

[ScenarioTitle("Initial properties")]
internal class Test_514 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenConnected(api1, () =>
        {
            api1.CreateSession(SampleMatchmakeOptions, "test_session");
        });

        WhenJoinedSession(api1, () =>
        {
            api1.Session.Properties["hello"] = "world";
            api2.Connect();
        });

        WhenConnected(api2, () =>
        {
            api2.GetSessionList(sessions =>
            {
                api2.JoinSessionByID(sessions.First(s => s.Label == "test_session").SessionID);
            });
        });

        WhenJoinedSession(api2, () =>
        {
            Assert.That(api2.Session.Properties["hello"].AsString, Is.EqualTo("world"));
            Test.Pass();
        });

        api1.Connect();
    }
}
