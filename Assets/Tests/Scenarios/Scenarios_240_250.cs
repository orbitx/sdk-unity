﻿using OrbitXSDK;
using System.Linq;

[ScenarioTitle("SDK receives 'joined'")]
internal class Test_240 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        api1.Events.OnPlayerListUpdated += eventArgs =>
        {
            Assert.That(eventArgs.UpdatedPlayers.First().Value, Is.EqualTo(PlayerUpdateEvent.Joined));
            Assert.That(api1.Session.PlayerCount, Is.EqualTo(2));
            Assert.That(api1.Session.Players.Length, Is.EqualTo(2));
            Test.Pass();
        };

        WhenTwoAPIsJoinOneSession(api1, api2, () => { });
    }
}

[ScenarioTitle("SDK receives 'left'")]
internal class Test_241 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            bool expectsLeave = false;

            api1.Events.OnPlayerListUpdated += eventArgs =>
            {
                if (expectsLeave)
                {
                    Assert.That(eventArgs.UpdatedPlayers.First().Value, Is.EqualTo(PlayerUpdateEvent.Left));
                    Assert.That(api1.Session.PlayerCount, Is.EqualTo(1));
                    Test.Pass();
                }
                else
                {
                    Assert.That(eventArgs.UpdatedPlayers.First().Value, Is.EqualTo(PlayerUpdateEvent.Joined));
                    Assert.That(api1.Session.PlayerCount, Is.EqualTo(2));
                    expectsLeave = true;
                }
            };

            api2.Session.Leave();
        });
    }
}

[ScenarioTitle("Changing sides")]
internal class Test_242 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        bool expectChangeSide = false;

        WhenConnected(api1, () =>
        {
            var options = SampleMatchmakeOptions;
            options.DefineSide(3, 1);
            api1.CreateSession(options, "test_session", "test_password");
        });

        WhenJoinedSession(api1, () =>
        {
            api2.Connect();
        });

        WhenConnected(api2, () =>
        {
            api1.Events.OnPlayerListUpdated += eventArgs =>
            {
                if (expectChangeSide)
                {
                    Assert.That(eventArgs.UpdatedPlayers.First().Value, Is.EqualTo(PlayerUpdateEvent.ChangedTeam));
                    Assert.That(api1.Session.PlayerCount, Is.EqualTo(2));
                    Test.Pass();
                }
                else
                {
                    Assert.That(eventArgs.UpdatedPlayers.First().Value, Is.EqualTo(PlayerUpdateEvent.Joined));
                    Assert.That(api1.Session.PlayerCount, Is.EqualTo(2));
                    expectChangeSide = true;
                }
            };

            api2.GetSessionList(sessions =>
            {
                var session = sessions.First(s => s.Label == "test_session");
                api2.JoinSessionByID(session.SessionID, 0, "test_password");
            });
        });

        WhenJoinedSession(api2, () =>
        {
            api2.Session.ChangeMySide(3);
            Assert.That(api2.Session.PlayerCount, Is.EqualTo(2));
            Assert.That(api2.Session.PlayerSides[api2.Player], Is.EqualTo(3));
        });

        api1.Connect();
    }
}

[ScenarioTitle("When a player side changes")]
internal class Test_243 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            var options = OrbitX.DefineMatchOptions();
            options.DefineSide(1, 1);
            api.CreateSession(options, "", 1);
        });

        WhenJoinedSession(api, () =>
        {
            int calls = 0;
            api.Events.OnPlayerListUpdated += eventArgs =>
            {
                if (++calls == 2)
                {
                    Assert.That(api.Session.PlayerSides[api.Player], Is.EqualTo(1));
                    Test.Pass();
                }
            };

            api.Session.ChangeMySide(2);
            Assert.That(api.Session.PlayerSides[api.Player], Is.EqualTo(2));
        });

        api.Connect();
    }
}
