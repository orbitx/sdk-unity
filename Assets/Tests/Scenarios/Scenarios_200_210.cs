﻿using OrbitXSDK;

[ScenarioTitle("Connect to game server")]
internal class Test_201 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        api.Events.OnConnectionStateChanged += eventArgs =>
        {
            if (eventArgs.State == ConnectionState.Connecting)
                return;

            Assert.That(eventArgs.State, Is.EqualTo(ConnectionState.Connected));
            Test.Pass();
        };

        api.Connect();
    }
}

[ScenarioSettings(Timeout = 0)]
[ScenarioTitle("Can not connect to game server")]
internal class Test_202 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        api.Events.OnConnectionStateChanged += eventArgs =>
        {
            if (eventArgs.State == ConnectionState.Connecting)
                return;

            Assert.That(eventArgs.State, Is.EqualTo(ConnectionState.Disconnected));
            Test.Pass();
        };

        api.ServerAddress = "127.0.0.1";
        api.ServerPort = 1;
        api.Connect();
    }
}

[ScenarioTitle("Identify player on game server")]
internal class Test_203 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        api.Events.OnConnectionStateChanged += eventArgs =>
        {
            Assert.That(eventArgs.State, Is.NotEqualTo(ConnectionState.Disconnected));
        };

        api.Connect();
    }

    internal void TimeoutHandler(OrbitXAPI api)
    {
        Assert.That(api.ConnectionState, Is.EqualTo(ConnectionState.Connected));
        Test.Pass();
    }
}
