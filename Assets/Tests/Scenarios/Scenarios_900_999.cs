﻿using OrbitXSDK;
using OrbitXSDK.Packets;

[ScenarioSettings(AbortOnFailure = false, Timeout = 0)]
[ScenarioTitle("Queries with optional responses: With response")]
internal class Test_900 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        bool expectationSatisfied = false;

        WhenConnected(api, () => api.CreateSession(SampleMatchmakeOptions, RandomString));

        api.Events.OnProblem += eventArgs =>
        {
            Assert.That(eventArgs.ErrorCode, Is.EqualTo((int)ExceptionCode.NOT_A_PARTICIPANT));
            expectationSatisfied = true;
        };

        WhenMadeManager(api, () =>
        {
            var player = new Player(api, 0);
            api.Session.Manager.Kick(player);
            var timeout = (GameClient.RETRY_COUNT + 1) * GameClient.RETRY_INTERVAL;
            UnityBroker.PostToUnityDelayed(timeout, () =>
            {
                Assert.That(expectationSatisfied, Is.EqualTo(true));
                Test.Pass();
            });
        });

        api.Connect();
    }
}

[ScenarioSettings(Timeout = 0)]
[ScenarioTitle("Queries with optional responses: Without response")]
internal class Test_901 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        api1.Events.OnConnectionStateChanged += ConnectionStateChanged;
        api2.Events.OnConnectionStateChanged += ConnectionStateChanged;

        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall();
        });

        var timeout = (GameClient.RETRY_COUNT + 1) * GameClient.RETRY_INTERVAL;
        UnityBroker.PostToUnityDelayed(timeout, Test.Pass);
    }

    private void ConnectionStateChanged(ConnectionStateChangedArgs args)
    {
        Assert.That(args.State, Is.NotEqualTo(ConnectionState.Disconnected));
    }
}

[ScenarioSettings(Timeout = 0)]
[ScenarioTitle("Queries with strict responses: With response")]
internal class Test_902 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        bool expectationSatisified = false;

        WhenConnected(api, () =>
        {
            api.GameClient.RegisterPacketIntercepter(packet =>
            {
                if (packet.MsgType == PacketType.SUBSCRIBE_RESPONSE)
                    expectationSatisified = true;
                return true;
            });

            api.Matchmake(SampleMatchmakeOptions, 1, 1, 1);
            var timeout = (GameClient.RETRY_COUNT + 1) * GameClient.RETRY_INTERVAL;
            UnityBroker.PostToUnityDelayed(timeout, () =>
            {
                Assert.That(expectationSatisified, Is.EqualTo(true));
                Test.Pass();
            });
        });

        api.Connect();
    }
}

[ScenarioSettings(Timeout = 0)]
[ScenarioTitle("Queries with strictresponses: Without response")]
internal class Test_903 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.GameClient.RegisterPacketIntercepter(packet =>
            {
                return packet.MsgType != PacketType.SUBSCRIBE_RESPONSE;
            });

            api.Events.OnConnectionStateChanged.TearDown();
            api.Events.OnConnectionStateChanged += eventArgs => {
                Test.Pass();
            };
            var timeout = (GameClient.RETRY_COUNT + 1) * GameClient.RETRY_INTERVAL;
            UnityBroker.PostToUnityDelayed(timeout, () =>
            {
                Test.Fail("Expected to disconnect when a strict query did not get its response in maximum wait time");
            });

            api.Matchmake(SampleMatchmakeOptions, 1, 1, 1);

        });

        api.Connect();
    }
}
