﻿using OrbitXSDK;

[ScenarioTitle("Leave a session permanently/temporarily")]
internal class Test_230 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            api.Session.Leave();
        });

        api.Events.OnLeftSession += eventArgs =>
        {
            Assert.That(eventArgs.Reason, Is.EqualTo(SessionLeaveReason.Voluntarily));
            Test.Pass();
        };

        api.Connect();
    }
}

[ScenarioSettings(AbortOnFailure = false)]
[ScenarioTitle("Leave a session permanently/temporarily: Failure")]
internal class Test_231 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        Session session = null;
        bool alreadyLeft = false;

        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            session = api.Session;
            session.Leave();
        });

        api.Events.OnLeftSession += eventArgs =>
        {
            if (alreadyLeft)
                return;

            alreadyLeft = true;

            Assert.That(eventArgs.Reason, Is.EqualTo(SessionLeaveReason.Voluntarily));
            session.Leave();
        };

        api.Events.OnProblem += eventArgs =>
        {
            Assert.That(eventArgs.ErrorCode, Is.EqualTo((int)OrbitXSDK.Packets.ExceptionCode.INVALID_SID));
            Test.Pass();
        };

        api.Connect();
    }
}
