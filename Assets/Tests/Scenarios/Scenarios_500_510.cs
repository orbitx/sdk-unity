﻿using OrbitXSDK;
using OrbitXSDK.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ScenarioTitle("Calling RPCs")]
internal class Test_500 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        var go = AddSyncObject(1);
        RPCPassBehaviour.SetupObject(go, Test);

        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            go.RPC(RPCFlags.ForMeToo, "SomeRPC");
        });

        api.Connect();
    }
}

[ScenarioTitle("When RPC is called")]
internal class Test_501 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        RPCPassBehaviour.SetupObject(AddSyncObject(1), Test);

        WhenConnected(api1, () =>
        {
            api1.CreateSession(SampleMatchmakeOptions, "test_session");
        });

        WhenJoinedSession(api1, () =>
        {
            api2.Connect();
        });

        WhenConnected(api2, () =>
        {
            api2.GetSessionList(sessions =>
            {
                api2.JoinSessionByID(sessions.First(s => s.Label == "test_session").SessionID);
            });
        });

        WhenJoinedSession(api2, () =>
        {
            var go = AddSyncObject(1);
            api2.Session.AttachObject(go);
            go.RPC("SomeRPC");
        });

        api1.Connect();
    }
}

[ScenarioTitle("RPC order")]
internal class Test_502 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "test_session");
        });

        WhenJoinedSession(api, () =>
        {
            api.Session.QueueRPC(GeneratePacket(api, api.Session.SessionID, 3));
            api.Session.QueueRPC(GeneratePacket(api, api.Session.SessionID, 2));
            api.Session.QueueRPC(GeneratePacket(api, api.Session.SessionID, 1));
        });

        int counter = 0;
        ParameteredFutureCallbackBehaviour.SetupObject(AddSyncObject(1), arg =>
        {
            Assert.That(arg, Is.EqualTo(++counter));

            if (counter == 3)
                Test.Pass();
        });

        
        api.Connect();
    }

    private RPCCalled GeneratePacket(OrbitXAPI api, int sessionId, int arg)
    {
        var prms = new Dictionary<sbyte, DOMNode>();
        prms[0] = Extensions.ConvertToDOMNode(arg);
        return new RPCCalled(api.Player.UserID, sessionId, arg, "1|Method", prms);
    }
}

[ScenarioTitle("Cached RPC")]
internal class Test_503 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        var go1 = AddSyncObject(1);
        RPCPassBehaviour.SetupObject(go1, Test);

        WhenConnected(api1, () =>
        {
            api1.CreateSession(SampleMatchmakeOptions, "test_session");
        });

        WhenJoinedSession(api1, () =>
        {
            var tgo = AddSyncObject(2, api1.Session);
            FutureCallbackBehaviour.SetupObject(tgo, api2.Connect);
            tgo.RPC(RPCFlags.Persistent | RPCFlags.ForMeToo, "Method");
        });

        GameObject go2 = null;
        WhenConnected(api2, () =>
        {
            go2 = AddSyncObject(1);
            api2.GetSessionList(sessions =>
            {
                api2.JoinSessionByID(sessions.First(s => s.Label == "test_session").SessionID);
            });
        });

        WhenJoinedSession(api2, () =>
        {
            Assert.That(go1.GetComponent<SyncObject>().Session, Is.EqualTo(api1.Session));
            Assert.That(go2.GetComponent<SyncObject>().Session, Is.EqualTo(api2.Session));
            go2.RPC("SomeRPC");
        });

        api1.Connect();
    }
}

internal class RPCPassBehaviour : MonoBehaviour
{
    private TestOutcome mOutcome;

    [OrbitXRPC]
    private void SomeRPC()
    {
        mOutcome.Pass();
    }

    static public void SetupObject(GameObject go, TestOutcome test)
    {
        go.AddComponent<RPCPassBehaviour>().mOutcome = test;
    }
}

internal class FutureCallbackBehaviour : MonoBehaviour
{
    private Action mThen;

    [OrbitXRPC]
    private void Method()
    {
        mThen();
    }

    static public void SetupObject(GameObject go, Action then)
    {
        go.AddComponent<FutureCallbackBehaviour>().mThen = then;
    }
}

internal class ParameteredFutureCallbackBehaviour : MonoBehaviour
{
    private Action<int> mThen;

    [OrbitXRPC]
    private void Method(int arg)
    {
        mThen(arg);
    }

    static public void SetupObject(GameObject go, Action<int> then)
    {
        go.AddComponent<ParameteredFutureCallbackBehaviour>().mThen = then;
    }
}
