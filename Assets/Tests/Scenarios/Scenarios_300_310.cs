﻿using OrbitXSDK;
using OrbitXSDK.Packets;

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Manager start call")]
internal class Test_300 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall();
        });
    }

    private void TimeoutHandler(OrbitXAPI api1, OrbitXAPI api2)
    {
        Test.Pass();
    }
}

[ScenarioSettings(AbortOnFailure = false)]
[ScenarioTitle("Client start call: Failure")]
internal class Test_301 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        api2.Events.OnProblem += eventArgs =>
        {
            Assert.That(eventArgs.ErrorCode, Is.EqualTo((int)ExceptionCode.NOT_MANAGER));
            Test.Pass();
        };

        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api2.Session.FireStartCall();
        });
    }
}

[ScenarioTitle("Ready call")]
internal class Test_302 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall();
        });

        api1.Events.OnSessionReadyToStart += SessionReadyToStart;
        api2.Events.OnSessionReadyToStart += SessionReadyToStart;
    }

    private void SessionReadyToStart(SessionReadyToStartArgs eventArgs)
    {
        Assert.That(eventArgs.Session.State, Is.EqualTo(SessionState.StartCall));
        Test.PassWhenCalled(2);
    }
}

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Respond with ready")]
internal class Test_303 : Scenario
{
    private bool mRespondedYet = false;

    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall();
        });

        api2.Events.OnSessionReadyToStart += eventArgs =>
        {
            mRespondedYet = true;
            eventArgs.Session.RespondToStartCall(true);
        };
    }

    private void TimeoutHandler(OrbitXAPI api1, OrbitXAPI api2)
    {
        Assert.That(mRespondedYet, Is.EqualTo(true));
        Test.Pass();
    }
}

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Respond with cancel")]
internal class Test_304 : Scenario
{
    private bool mRespondedYet = false;

    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall();
        });

        api2.Events.OnSessionReadyToStart += eventArgs =>
        {
            api2.Events.OnSessionStateChanged += e =>
            {
                Assert.That(api2.Session.State, Is.EqualTo(SessionState.Joined));
            };

            mRespondedYet = true;
            eventArgs.Session.RespondToStartCall(false);
        };
    }

    private void TimeoutHandler(OrbitXAPI api1, OrbitXAPI api2)
    {
        Assert.That(mRespondedYet, Is.EqualTo(true));
        Test.Pass();
    }
}

[ScenarioSettings(AbortOnFailure = false)]
[ScenarioTitle("Responding: Failure")]
internal class Test_305 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            api.Session.RespondToStartCall(false);
        });

        api.Events.OnProblem += eventArgs =>
        {
            Assert.That(eventArgs.ErrorCode, Is.EqualTo((int)ExceptionCode.INVALID_SESSION_STATE));
            Test.Pass();
        };

        api.Connect();
    }
}

[ScenarioSettings(Timeout = 2000)]
[ScenarioTitle("Respond with nothing")]
internal class Test_306 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall(true, true, 1);
        });

        api2.Events.OnSessionReadyToStart += eventArgs =>
        { };

        api1.Events.OnSessionStartCancelled += eventArgs =>
        {
            Assert.That(api1.Session.State, Is.EqualTo(SessionState.Joined));
            Test.PassWhenCalled(2);
        };

        api2.Events.OnSessionStartCancelled += eventArgs =>
        {
            Assert.That(api2.Session.State, Is.EqualTo(SessionState.Joined));
            Test.PassWhenCalled(2);
        };

        api2.Events.OnProblem += eventArgs =>
        {
            Log.d(eventArgs);
        };
    }
}

[ScenarioTitle("When start process gets cancelled")]
internal class Test_307 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall(true, true, 1);
        });

        api2.Events.OnSessionReadyToStart += eventArgs =>
        {
            api2.Session.RespondToStartCall(false);
        };

        api1.Events.OnSessionStartCancelled += eventArgs =>
        {
            Assert.That(api1.Session.State, Is.EqualTo(SessionState.Joined));
            Test.PassWhenCalled(2);
        };

        api2.Events.OnSessionStartCancelled += eventArgs =>
        {
            Assert.That(api2.Session.State, Is.EqualTo(SessionState.Joined));
            Test.PassWhenCalled(2);
        };

        api2.Events.OnProblem += eventArgs =>
        {
            Log.d(eventArgs);
        };
    }
}

[ScenarioTitle("When start process executes to the end")]
internal class Test_308 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall(true, true, 1);
        });

        api1.Events.OnSessionReadyToStart += eventArgs =>
        {
            api1.Session.RespondToStartCall(true);
        };
        api2.Events.OnSessionReadyToStart += eventArgs =>
        {
            api2.Session.RespondToStartCall(true);
        };

        api1.Events.OnSessionStarted += eventArgs =>
        {
            Assert.That(api1.Session.State, Is.EqualTo(SessionState.Running));
            Test.PassWhenCalled(2);
        };
        api2.Events.OnSessionStarted += eventArgs =>
        {
            Assert.That(api2.Session.State, Is.EqualTo(SessionState.Running));
            Test.PassWhenCalled(2);
        };

        api2.Events.OnProblem += eventArgs =>
        {
            Log.d(eventArgs);
        };
    }
}
