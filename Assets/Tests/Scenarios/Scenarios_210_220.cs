﻿using OrbitXSDK;

[ScenarioTitle("Request matchmaking")]
internal class Test_210 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            var options = OrbitX.DefineMatchOptions();
            options.OnTrackIdReceived = () => { Test.Pass(); };

            api.SimpleMatchmake(options);
        });

        api.Connect();
    }
}

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Cancel matchmaking")]
internal class Test_211 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        const string tag = "sample tag";

        WhenConnected(api, () =>
        {
            var options = SampleMatchmakeOptions;
            options.OnTrackIdReceived = () => { api.CancelMatchmake(tag); };

            api.SimpleMatchmake(options, tag);
        });

        api.Connect();
    }

    internal void TimeoutHandler(OrbitXAPI api)
    {
        Test.Pass();
    }
}

[ScenarioSettings(Timeout = 3000)]
[ScenarioTitle("Inform developer of matchmaking response")]
internal class Test_212 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenConnected(api1, () => { api1.SimpleMatchmake(SampleMatchmakeOptions); });
        WhenConnected(api2, () => { api2.SimpleMatchmake(SampleMatchmakeOptions); });

        api1.Events.OnMatchmakeDone += args =>
        {
            Test.Pass();
        };

        api1.Connect();
        UnityBroker.PostToUnityDelayed(1000, api2.Connect);
    }
}
