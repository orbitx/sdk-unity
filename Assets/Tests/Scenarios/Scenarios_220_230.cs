﻿[ScenarioTitle("Request to create a session")]
internal class Test_220 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenJoinedSession(api, () =>
        {
            Test.Pass();
        });

        api.Connect();
    }
}

[ScenarioTitle("Request session list")]
internal class Test_221 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.GetSessionList(sessions =>
            {
                Test.Pass();
            });
        });

        api.Connect();
    }
}

[ScenarioTitle("Request to join session")]
internal class Test_222 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            Test.Pass();
        });
    }
}

[ScenarioSettings(AbortOnFailure = false)]
[ScenarioTitle("Request to join session: Failure")]
internal class Test_223 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.JoinSessionByID(12345);
        });

        api.Events.OnProblem += eventArgs =>
        {
            Test.Fail(eventArgs.ToString());
        };

        api.Events.OnMatchmakeFailed += eventArgs =>
        {
            Test.Pass();
        };

        api.Connect();
    }
}
