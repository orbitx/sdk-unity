﻿using OrbitXSDK;
using OrbitXSDK.Packets;
using System.Linq;

[ScenarioTitle("Manager assignment")]
internal class Test_400 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenMadeManager(api, () =>
        {
            Assert.That(api.Session.IsManager, Is.EqualTo(true));
            Test.Pass();
        });

        api.Connect();
    }
}

[ScenarioTitle("Kick")]
internal class Test_401 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.Manager.Kick(api2.Player);
            api1.Events.OnPlayerListUpdated += eventArgs =>
            {
                var details = eventArgs.UpdatedPlayers.First();
                if (details.Value == PlayerUpdateEvent.Joined)
                    return;

                Assert.That(details.Value, Is.EqualTo(PlayerUpdateEvent.Left));
                Test.Pass();
            };
        });
    }
}

[ScenarioSettings(AbortOnFailure = false)]
[ScenarioTitle("Kick: Failure")]
internal class Test_402 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenMadeManager(api, () =>
        {
            api.Session.Manager.Kick(new Player(api, 0));
        });

        api.Events.OnProblem += eventArgs =>
        {
            Assert.That(eventArgs.ErrorCode, Is.EqualTo((int)ExceptionCode.NOT_A_PARTICIPANT));
            Test.Pass();
        };

        api.Connect();
    }
}

[ScenarioTitle("Change side for a player")]
internal class Test_403 : Scenario
{
    protected override MatchmakeOptions SampleMatchmakeOptions
    {
        get
        {
            var result = base.SampleMatchmakeOptions;
            result.DefineSide(3, 1);
            return result;
        }
    }

    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Events.OnPlayerListUpdated += eventArgs =>
            {
                var details = eventArgs.UpdatedPlayers.First();
                if (details.Value == PlayerUpdateEvent.Joined)
                    api1.Session.Manager.ChangePlayerSide(details.Key, 3);
                else
                {
                    Assert.That(details.Value, Is.EqualTo(PlayerUpdateEvent.ChangedTeam));
                    Assert.That(api1.Session.PlayerSides[details.Key], Is.EqualTo(3));
                    Test.Pass();
                }
            };
        });
    }
}

[ScenarioTitle("Terminate")]
internal class Test_404 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall(true, true, 1);
        });

        api1.Events.OnSessionReadyToStart += RespondYes;
        api2.Events.OnSessionReadyToStart += RespondYes;

        api1.Events.OnSessionStarted += eventArgs =>
        {
            eventArgs.Session.Manager.Terminate();
        };

        api1.Events.OnLeftSession += eventArgs =>
        {
            Assert.That(api1.Sessions.Length, Is.EqualTo(0));
            Test.PassWhenCalled(2);
        };
        api2.Events.OnLeftSession += eventArgs =>
        {
            Assert.That(api2.Sessions.Length, Is.EqualTo(0));
            Test.PassWhenCalled(2);
        };
    }

    private void RespondYes(SessionReadyToStartArgs eventArgs)
    {
        eventArgs.Session.RespondToStartCall(true);
    }
}

[ScenarioSettings(AbortOnFailure = false)]
[ScenarioTitle("Terminate: Failure")]
internal class Test_405 : Scenario
{
    internal void Execute(OrbitXAPI api1, OrbitXAPI api2)
    {
        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall(true, true, 1);
        });

        api1.Events.OnSessionReadyToStart += RespondYes;
        api2.Events.OnSessionReadyToStart += RespondYes;

        api1.Events.OnSessionStarted += eventArgs =>
        {
            eventArgs.Session.Manager.Terminate();
            api1.Events.OnLeftSession += args =>
            {
                args.Session.Manager.Terminate();
            };
        };

        api1.Events.OnProblem += eventArgs =>
        {
            Assert.That(eventArgs.ErrorCode, Is.EqualTo((int)ExceptionCode.INVALID_SID));
            Test.Pass();
        };

    }

    private void RespondYes(SessionReadyToStartArgs eventArgs)
    {
        eventArgs.Session.RespondToStartCall(true);
    }
}

[ScenarioSettings(Timeout = 1000)]
[ScenarioTitle("Change session configuration")]
internal class Test_406 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenMadeManager(api, () =>
        {
            var config = api.Session.Manager.GetSessionConfiguration();
            config.AllowJoinWithinGame = !config.AllowJoinWithinGame;
            api.Session.Manager.ChangeSessionConfiguration(config);
        });

        api.Connect();
    }

    private void TimeoutHandler(OrbitXAPI api)
    {
        Test.Pass();
    }
}

[ScenarioTitle("When session configuration changes")]
internal class Test_407 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        bool newValue = true;

        WhenConnected(api, () =>
        {
            api.CreateSession(SampleMatchmakeOptions, "");
        });

        WhenMadeManager(api, () =>
        {
            var config = api.Session.Manager.GetSessionConfiguration();
            newValue = !config.AllowJoinWithinGame;
            config.AllowJoinWithinGame = newValue;
            api.Session.Manager.ChangeSessionConfiguration(config);
        });

        api.Events.OnSessionConfigurationChanged += eventArgs =>
        {
            var config = api.Session.Manager.GetSessionConfiguration();
            Assert.That(config.AllowJoinWithinGame, Is.EqualTo(newValue));
            Test.Pass();
        };

        api.Connect();
    }
}
