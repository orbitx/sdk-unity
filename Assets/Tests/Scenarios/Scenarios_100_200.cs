﻿using OrbitXSDK;

[ScenarioSettings(FixtureFolder = "Empty")]
[ScenarioTitle("Quick register player on AAA server")]
internal class Test_101 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        api.AuthClient.QuickRegister(success => {
            Assert.That(success, Is.EqualTo(true));
            Test.Pass();
        });
    }
}

[ScenarioTitle("")]
internal class Test_102 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        api.AuthClient.RequestOTP((success, otp) =>
        {
            Assert.That(success, Is.EqualTo(true));
            Test.Pass();
        });
    }
}

[ScenarioTitle("Change player details")]
internal class Test_103 : Scenario
{
    private string GenerateRandomAlias()
    {
        var sb = new System.Text.StringBuilder();
        for (int i = 0; i < 6; ++i)
            sb.Append((char)('A' + UnityEngine.Random.Range(0, 26)));
        return sb.ToString();
    }

    internal void Execute(OrbitXAPI api)
    {
        api.AuthClient.UpdatePlayerDetail(PlayerDetail.Alias, GenerateRandomAlias());
        api.AuthClient.SyncPlayerDetails(success =>
        {
            Assert.That(success, Is.EqualTo(true));
            Test.Pass();
        });
    }
}

[ScenarioSettings(FixtureFolder = "Empty", AbortOnFailure = false)]
[ScenarioTitle("Change player details: Failure")]
internal class Test_104 : Scenario
{
    internal void Execute(OrbitXAPI api)
    {
        int state = 0;
        const string newAlias = "Some Alias";
        api.Events.OnMyPlayerAccountUpdated += args =>
        {
            if(state == 1)
            {
                Assert.That(api.Player.Alias, Is.EqualTo(newAlias));
                state = 2;
            }
            else if(state == 2)
            {
                Assert.That(api.Player.Alias, Is.NotEqualTo(newAlias));
                Test.Pass();
            }
        };

        api.AuthClient.QuickRegister(success => {
            Assert.That(success, Is.EqualTo(true));
            api.AuthClient.ForgetAuthentication();

            state = 1;
            api.ChangePlayerDetail(PlayerDetail.Alias, newAlias);
        });
    }
}
