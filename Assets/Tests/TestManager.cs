﻿using OrbitXSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

public struct ServerContext
{
    public string Address;
    public int Port;
    public string ApplicationID;
}

public class TestManager : MonoBehaviour
{
    #region Test scan
    private const BindingFlags MethodFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

    static private List<Scenario> mScenarios = new List<Scenario>();
    static public Scenario[] Scenarios
    {
        get { return mScenarios.ToArray(); }
    }

    private void Awake()
    {
        mInstance = this;
        mServerContext = JSON.Parse<ServerContext>((Resources.Load("Fixtures/ServerContext") as TextAsset).text);
        ScanAllTests();
    }

    static public void ScanAllTests()
    {
        mScenarios.Clear();
        var baseType = typeof(Scenario);
        var assembly = AppDomain.CurrentDomain.GetAssemblies().First(ass => ass.GetName().Name == "Assembly-CSharp");
        foreach (var type in assembly.GetTypes())
            if (type.IsSubclassOf(baseType) && type.Name.StartsWith("Test_"))
                mScenarios.Add(Activator.CreateInstance(type) as Scenario);
    }

    public string[] MemorizedScenarios = new string[] { };
    public string[] TestList = new string[] { };
    #endregion Test scan


    #region Startup
    public bool BeginOnStart = true;
    public bool SkipLongTests = false;

    private void Start()
    {
        if(BeginOnStart)
            RunAll();
    }
    #endregion Startup


    #region Test execution
    static private TestManager mInstance;
    static private ServerContext mServerContext;
    static private int mCurrentScenario;

    static public Scenario GetActiveScenario()
    {
        var name = mInstance.TestList[mCurrentScenario];
        return mScenarios.First(s => s.ToString() == name);
    }

    static private void SkipAllLongTests()
    {
        mInstance.TestList = mScenarios
            .Where(s => mInstance.TestList.Contains(s.ToString()))
            .Where(s => (s.Timeout <= 500 && s.Timeout > 0))
            .Select(s => s.ToString())
            .ToArray();
    }

    static public void RunAll()
    {
        if (mInstance.SkipLongTests)
            SkipAllLongTests();

        if (mInstance.TestList.Length == 0)
            Log.e("[TestManager] No scenario to run");
        else
        {
            Log.d("[TestManager] Starting test run...");
            Run(0);
        }
    }

    static private void Run(int scenarioIndex)
    {
        mCurrentScenario = scenarioIndex;
        var scenario = GetActiveScenario();

        if(scenario.Timeout != 0)
            mInstance.StartCoroutine(BeginTimeoutCheck(scenario));
        scenario.Run(mServerContext);
    }

    static public void RunNextTest()
    {
        GetActiveScenario().TearDown();
        if (mCurrentScenario + 1 < mInstance.TestList.Length)
            Run(mCurrentScenario + 1);
        else
            mInstance.StartCoroutine(Finish());
    }

    static private IEnumerator Finish()
    {
        // This wait is here to capture all logs after tear down.
        yield return new WaitForSeconds(0.5f);

        var scenarios = mScenarios.Where(s => mInstance.TestList.Contains(s.ToString())).ToList();
        var fails = mScenarios.Count(s => !s.Passed);
        var totalStr = string.Format("Total duration: {0}, traffic: {1}(R), {2}(S), {3}(T)", 
            GetTimeString(scenarios.Sum(s => s.Duration)), 
            GetByteString(scenarios.Sum(s => s.TotalRecievedBytes)),
            GetByteString(scenarios.Sum(s => s.TotalSentBytes)),
            GetByteString(scenarios.Sum(s => s.TotalSentBytes + s.TotalRecievedBytes))
        );
        if (fails == 0)
            Log.d("[TestManager] All tests passed succesfully. " + totalStr);
        else
            Log.df("[TestManager] {0}/{1} tests failed. {2}", fails, scenarios.Count, totalStr);

        for(int i = 0; i < scenarios.Count; ++i)
        {
            var scenario = scenarios[i];
            var sb = new StringBuilder();
            sb.AppendFormat(
                "[TestManager] Test #{0}({1}/{2}) duration: {3}, traffic: {4}(R), {5}(S), {6}(T)\n\n",
                scenario,
                i + 1,
                scenarios.Count,
                GetTimeString(scenario.Duration),
                GetByteString(scenario.TotalRecievedBytes),
                GetByteString(scenario.TotalSentBytes),
                GetByteString(scenario.TotalRecievedBytes + scenario.TotalSentBytes));
            
            foreach (var entry in scenario.Logs)
                sb.AppendFormat("API {0} -> {1}: {2}\n", entry.Key, entry.Value.LogLevel, entry.Value.Message);

            if (scenario.Exception != null)
            {
                sb.AppendLine(scenario.Exception.Message);
                sb.AppendLine();
            }

            if (scenario.Passed)
                Log.d(sb.ToString());
            else
                Log.e(sb.ToString());
        }
    }

    static private IEnumerator BeginTimeoutCheck(Scenario scenario)
    {
        yield return new WaitForSecondsRealtime(scenario.Timeout * 0.001f + 0.03f);
        if (!scenario.Decided)
            scenario.Timedout();
    }
    #endregion Test execution


    #region Aux
    static private string GetTimeString(double duration)
    {
        int iduration = (int)duration;
        int millis = (int)((duration - iduration) * 1000);
        int minutes = iduration / 60;
        int seconds = iduration - minutes * 60;

        if (minutes > 0)
            return string.Format("{0}:{1}.{2}", minutes, seconds, millis);
        else
            return string.Format("{0}.{1}", seconds, millis);
    }

    static private string GetByteString(int bytes)
    {
        int mega = 1024 * 1024;
        int kilo = 1024;

        if (bytes >= mega)
            return GetTwoPartByteString(bytes, mega) + "M";
        else if (bytes >= kilo)
            return GetTwoPartByteString(bytes, kilo) + "K";
        else
            return bytes.ToString() + "B";
    }

    static private string GetTwoPartByteString(int value, int divisor)
    {
        int temp = value / divisor;
        double rem = temp + ((double)(value - temp * divisor) / divisor);
        return rem.ToString("F0.00");
    }
    #endregion Aux
}
