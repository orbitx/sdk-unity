﻿using System.Collections.Generic;

static public class TestGroupNames
{
    static private Dictionary<int, string> GroupNames = new Dictionary<int, string>()
    {
        {100, "AAA interactions"},
        {200, "Connection"},
        {210, "Automatic matchmaking"},
        {220, "Manual matchmaking"},
        {230, "Leaving sessions"},
        {240, "Player updates in sessions"},
        {300, "Starting game session"},
        {310, "Active sessions"},
        {400, "Manager"},
        {500, "RPC"},
        {510, "Session properties"},
        {900, "Functionality tests"},
    };

    static public string Get(int groupIndex)
    {
        return groupIndex.ToString() + "  " + (GroupNames.ContainsKey(groupIndex) ? GroupNames[groupIndex] : "");
    }
}
