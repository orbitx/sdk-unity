﻿using System;

public class TestOutcome
{
    private int mPassCount = 0;
    private Action<Exception> mResolver;

    public TestOutcome(Action<Exception> resolver)
    {
        mResolver = resolver;
    }

    public void Pass()
    {
        mResolver(null);
    }

    public void PassWhenCalled(int numberOfCalls)
    {
        if(++mPassCount >= numberOfCalls)
            Pass();
    }

    public void Fail(Exception ex)
    {
        mResolver(ex);
    }

    public void Fail(string ex, params object[] prms)
    {
        Fail(new Exception(string.Format(ex, prms)));
    }
}

public class Assertion
{
    private TestOutcome mOutcome;

    public Assertion(TestOutcome outcome)
    {
        mOutcome = outcome;
    }

    public void That<T>(T argument, Action<T> comparator)
    {
        try
        {
            comparator(argument);
        }
        catch (AssertionException ex)
        {
            mOutcome.Fail(ex);
        }
    }
}

public class AssertionException : Exception
{
    public AssertionException(string formatStr, params object [] prms)
        : base(string.Format("Assertion failed: {0}", string.Format(formatStr, prms)))
    { }

    public override string ToString()
    {
        return Message;
    }
}

static public class Is
{
    static public Action<T> EqualTo<T>(T value)
    {
        return (T inputValue) =>
        {
            if (!inputValue.Equals(value))
                throw new AssertionException("expected '{0}' but got '{1}'", value, inputValue);
        };
    }

    static public Action<T> NotEqualTo<T>(T value)
    {
        return (T inputValue) =>
        {
            if (inputValue.Equals(value))
                throw new AssertionException("expected anything but '{0}' but got '{1}'", value, inputValue);
        };
    }
}
