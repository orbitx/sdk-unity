﻿using OrbitXSDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

public class Scenario
{
    #region Data
    private bool mDecided = false;
    public bool Decided
    {
        get { return mDecided; }
    }

    private Exception mException;
    public Exception Exception
    {
        get { return mException; }
    }

    private double mDuration;
    public double Duration
    {
        get { return mDuration; }
    }

    private int mTotalRecievedBytes;
    public int TotalRecievedBytes
    {
        get { return mTotalRecievedBytes; }
    }

    private int mTotalSentBytes;
    public int TotalSentBytes
    {
        get { return mTotalSentBytes; }
    }

    public bool Passed
    {
        get { return mException == null; }
    }

    private ScenarioSettings mAttributes;
    public int Timeout
    {
        get { return mAttributes.Timeout; }
    }
    #endregion Data


    #region Scan
    private const BindingFlags MethodFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
    private MethodInfo mTestBody;
    private MethodInfo mTimeoutBody;

    public Scenario()
    {
        var apiType = typeof(OrbitXAPI);

        ExtractName();
        mOutcome = new TestOutcome(OutcomeResolved);
        mAssertion = new Assertion(mOutcome);

        var attrs = GetType().GetCustomAttributes(typeof(ScenarioSettings), false);
        if (attrs.Length == 1)
            mAttributes = attrs[0] as ScenarioSettings;
        else
            mAttributes = new ScenarioSettings();

        mTestBody = GetType().GetMethod("Execute", MethodFlags);
        if(mTestBody == null)
            throw new Exception("[Scenario] Invalid test method body for type " + GetType().Name);

        foreach (var prm in mTestBody.GetParameters())
            if (prm.ParameterType != apiType)
                throw new Exception("[Scenario] Invalid test method decleration in type " + GetType().Name);

        mTimeoutBody = GetType().GetMethod("TimeoutHandler", MethodFlags);
        if (mTimeoutBody != null)
        {
            if(mTimeoutBody.GetParameters().Length != mTestBody.GetParameters().Length)
                throw new Exception("[Scenario] TimedoutHandler's parameter count does not match test body's parameter count for type " + GetType().Name);

            foreach (var prm in mTimeoutBody.GetParameters())
                if (prm.ParameterType != apiType)
                    throw new Exception("[Scenario] Invalid test method decleration in type " + GetType().Name);
        }
    }
    #endregion Scan


    #region Execution
    private int mStartTick;
    private List<OrbitXAPI> mRunningAPIs = new List<OrbitXAPI>();
    private List<KeyValuePair<int, LogArgs>> mLogs;
    public List<KeyValuePair<int, LogArgs>> Logs
    {
        get { return mLogs; }
    }

    private string LoadFixture(int apiNumber)
    {
        var fixturePath = string.Format("Fixtures/{0}/API {1}", mAttributes.FixtureFolder, apiNumber);
        var fixtureData = UnityEngine.Resources.Load(fixturePath) as UnityEngine.TextAsset;
        if(fixtureData == null)
            Test.Fail(new AssertionException("{0} not found", fixturePath));

        return fixtureData.text;
    }

    public void Run(ServerContext serverContext)
    {
        mLogs = new List<KeyValuePair<int, LogArgs>>();

        int count = mTestBody.GetParameters().Length;
        List<object> prms = new List<object>();
        for (int i = 1; i <= count; ++i)
        {
            var api = new OrbitXAPI(i, LoadFixture(i));
            mRunningAPIs.Add(api);
            prms.Add(api);

            api.ServerAddress = serverContext.Address;
            api.ServerPort = serverContext.Port;
            api.ApplicationToken = serverContext.ApplicationID;
            api.SetMinimumLogLevel(LogLevel.Debug);

            int apiIndex = i;
            api.Events.OnLog += eventArgs =>
            {
                mLogs.Add(new KeyValuePair<int, LogArgs>(apiIndex, eventArgs));
            };

            if (mAttributes.AbortOnFailure)
            {
                api.Events.OnProblem += eventArgs =>
                {
                    Test.Fail(eventArgs.ToString());
                };
                api.Events.OnMatchmakeFailed += eventArgs =>
                {
                    Test.Fail(eventArgs.ToString());
                };
            }
        }

        mStartTick = Environment.TickCount;
        mTestBody.Invoke(this, prms.ToArray());
    }

    public void Timedout()
    {
        if(mTimeoutBody != null)
            mTimeoutBody.Invoke(this, mRunningAPIs.ToArray());
        else
            Test.Fail("[Scenario] Timed-out after {0} millis", Timeout);
    }

    public void TearDown()
    {
        CleanUpGameObjects();

        foreach (var api in mRunningAPIs)
            api.TearDown();

        mRunningAPIs.Clear();
    }

    protected double ExecutionTime
    {
        get { return (Environment.TickCount - mStartTick) * 0.001; }
    }
    #endregion Execution


    #region Outcome resolution
    private TestOutcome mOutcome;
    public TestOutcome Test
    {
        get { return mOutcome; }
    }

    private Assertion mAssertion;
    public Assertion Assert
    {
        get { return mAssertion; }
    }

    public void OutcomeResolved(Exception exception)
    {
        if (mDecided)
            return;

        mDuration = ExecutionTime;
        mTotalRecievedBytes = mRunningAPIs.Sum(api => api.NetworkUsage.ReceivedBytes);
        mTotalSentBytes = mRunningAPIs.Sum(api => api.NetworkUsage.SentBytes);
        mException = exception;
        mDecided = true;

        UnityBroker.PostToUnity(() =>
        {
            TestManager.RunNextTest();
        });

        if (mException != null)
            throw mException;
    }
    #endregion Outcome resolution


    #region Shortcuts
    internal void WhenConnected(OrbitXAPI api, Action then)
    {
        api.Events.OnConnectionStateChanged += eventArgs =>
        {
            if (eventArgs.State == ConnectionState.Connecting)
                return;

            Assert.That(eventArgs.State, Is.EqualTo(ConnectionState.Connected));
            then();
        };
    }

    internal void WhenJoinedSession(OrbitXAPI api, Action then)
    {
        api.Events.OnMatchmakeDone += eventArgs =>
        {
            then();
        };
    }

    internal void WhenTwoAPIsJoinOneSession(OrbitXAPI api1, OrbitXAPI api2, Action then)
    {
        var sessionName = RandomString;
        var sessionPassword = RandomString;

        WhenConnected(api1, () =>
        {
            api1.CreateSession(SampleMatchmakeOptions, sessionName, sessionPassword);
        });

        WhenJoinedSession(api1, () =>
        {
            api2.Connect();
        });

        WhenConnected(api2, () =>
        {
            api2.GetSessionList(sessions =>
            {
                api2.JoinSessionByID(sessions.First(s => s.Label == sessionName).SessionID, 0, sessionPassword);
            });
        });

        WhenJoinedSession(api2, () =>
        {
            then();
        });

        api1.Connect();
    }

    internal void WhenSessionStartedForTwo(OrbitXAPI api1, OrbitXAPI api2, Action then)
    {
        api1.Events.OnSessionReadyToStart += eventArgs =>
        {
            eventArgs.Session.RespondToStartCall(true);
        };

        api2.Events.OnSessionReadyToStart += eventArgs =>
        {
            eventArgs.Session.RespondToStartCall(true);
        };

        int count = 0;
        api1.Events.OnSessionStarted += eventArgs =>
        {
            if (++count == 2)
                then();
        };
        api2.Events.OnSessionStarted += eventArgs =>
        {
            if (++count == 2)
                then();
        };

        WhenTwoAPIsJoinOneSession(api1, api2, () =>
        {
            api1.Session.FireStartCall(true, true, 1);
        });
    }

    internal void WhenMadeManager(OrbitXAPI api, Action then)
    {
        api.Events.OnMadeManager += eventArgs =>
        {
            then();
        };
    }
    #endregion Shortcuts


    #region Aux
    protected string RandomString
    {
        get
        {
            string result = "";

            for (int i = 0; i < 6; ++i)
                result += ('A' + UnityEngine.Random.Range(0, 26));
            
            return result;
        }
    }

    virtual protected MatchmakeOptions SampleMatchmakeOptions
    {
        get
        {
            var options = OrbitX.DefineMatchOptions();
            options.DefineSide(1, 1);
            options.DefineSide(2, 1);
            return options;
        }
    }

    #endregion Aux

    #region Name
    private string mName;

    private void ExtractName()
    {
        mName = GetType().Name.Replace("Test_", "");
        mName = mName.Replace("Test", "");
    }

    public override string ToString()
    {
        return mName;
    }
    #endregion Name


    #region GameObjects
    private List<UnityEngine.GameObject> mGameObjects = new List<UnityEngine.GameObject>();

    protected UnityEngine.GameObject AddSyncObject(int objectId, Session desiredSession = null)
    {
        var go = new UnityEngine.GameObject();
        mGameObjects.Add(go);

        var syncObj = go.AddComponent<SyncObject>();
        syncObj.AssignObjectId(objectId);

        if (desiredSession != null)
            desiredSession.AttachObject(syncObj);

        return go;
    }

    private void CleanUpGameObjects()
    {
        foreach (var go in mGameObjects)
            UnityEngine.Object.Destroy(go);

        mGameObjects.Clear();
    }

    #endregion GameObjects
}
