﻿using System;

[AttributeUsage(AttributeTargets.Class)]
public class ScenarioSettings : Attribute
{
    private int mTimeout = 500;
    public int Timeout
    {
        get { return mTimeout; }
        set { mTimeout = value; }
    }

    private string mFixtureFolder = "Default";
    public string FixtureFolder
    {
        get { return mFixtureFolder; }
        set { mFixtureFolder = value; }
    }

    private bool mAbortOnFailure = true;
    public bool AbortOnFailure
    {
        get { return mAbortOnFailure; }
        set { mAbortOnFailure = value; }
    }

    public ScenarioSettings()
    { }
}

[AttributeUsage(AttributeTargets.Class)]
public class ScenarioTitle : Attribute
{
    private string mTitle;
    public string Title
    {
        get { return mTitle; }
    }

    public ScenarioTitle(string title)
    {
        mTitle = title;
    }
}
