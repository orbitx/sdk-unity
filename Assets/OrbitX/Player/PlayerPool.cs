﻿using System.Collections.Generic;

namespace OrbitXSDK
{
    internal class PlayerPool
    {
        private OrbitXAPI mAPI;
        private Dictionary<long, Player> mPlayers = new Dictionary<long, Player>();

        internal PlayerPool(OrbitXAPI api)
        {
            mAPI = api;
        }

        internal Player GetPlayerByUID(long uid)
        {
            if (mPlayers.ContainsKey(uid))
                return mPlayers[uid];

            var result = new Player(mAPI, uid);
            mPlayers[uid] = result;
            mAPI.AuthClient.FillPlayerInfo(result);
            return result;
        }

        internal List<Player> GetPlayersByUID(IEnumerable<long> uids)
        {
            var result = new List<Player>();
            foreach (var uid in uids)
                result.Add(GetPlayerByUID(uid));
            return result;
        }

        internal void Add(Player player)
        {
            if (mPlayers.ContainsKey(player.UserID))
                mAPI.Log.ef("[PlayerPool] Player '{0}' is already pooled", player);
            else
                mPlayers[player.UserID] = player;
        }

        internal void Remove(Player player)
        {
            if (mPlayers.ContainsKey(player.UserID))
                mPlayers.Remove(player.UserID);
            else
                mAPI.Log.ef("[PlayerPool] player {0} is not in pool", player);
        }
    }
}
