﻿using System;

namespace OrbitXSDK
{
    public enum PlayerDetail
    {
        Alias,
        Avatar,
        Email
    }

    public enum AccountEvent
    {
        Registered,
        DetailChanged
    }

    public class Player
    {
        private long mUserID;
        public long UserID
        {
            get { return mUserID; }
        }

        public bool IsValid
        {
            get { return mUserID != 0; }
        }

        private OrbitXAPI mAPI;
        public bool IsMe
        {
            get { return IsValid && mUserID == mAPI.AuthClient.UserID; }
        }

        private string mAlias = "";
        public string Alias
        {
            get { return string.IsNullOrEmpty(mAlias) ? mUserID.ToString() : mAlias; }
        }

        private string mAvatar = "";
        public string Avatar
        {
            get { return mAvatar; }
        }

        private string mEmail = "";
        public string Email
        {
            get { return mEmail; }
        }

        internal Player(OrbitXAPI api, long userId)
        {
            mAPI = api;
            mUserID = userId;
        }

        internal void LoadValues(string alias, string avatar, string email)
        {
            mAlias = alias;
            mAvatar = avatar;
            mEmail = email;
        }

        internal void ChangeDetail(PlayerDetail detailType, object newValue)
        {
            if (detailType == PlayerDetail.Alias)
                mAlias = newValue as string;
            else if (detailType == PlayerDetail.Avatar)
                mAvatar = newValue as string;
            else if (detailType == PlayerDetail.Email)
                mEmail = newValue as string;
            else
                mAPI.Log.ef("[Player] Changing detail type {0} is not implemented", detailType);
        }

        internal object GetDetail(PlayerDetail detailType)
        {
            if (detailType == PlayerDetail.Alias)
                return mAlias;
            else if (detailType == PlayerDetail.Avatar)
                return mAvatar;
            else if (detailType == PlayerDetail.Email)
                return mEmail;
            else
                return null;
        }

        public override string ToString()
        {
            return Alias;
        }
    }
}
