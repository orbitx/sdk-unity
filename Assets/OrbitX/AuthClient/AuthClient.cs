﻿using System;
using System.Collections.Generic;

namespace OrbitXSDK
{
    internal class AuthClient
    {
        #region Local User Properties
        private Player mSelf;
        internal Player Self
        {
            get
            {
                if (mSelf == null)
                {
                    mSelf = new Player(mAPI, UserID);
                    FillPlayerInfoFromLocalCache(mSelf);
                    mAPI.PlayerPool.Add(mSelf);
                }

                return mSelf;
            }
        }

        const string KEY_UID = "AuthClient.UID";
        internal long UserID
        {
            get { return mAPI.LocalData.GetLong(KEY_UID); }
            private set { mAPI.LocalData.Set(KEY_UID, value); }
        }

        const string KEY_PASSWORD = "AuthClient.Pass";
        private string Password
        {
            get { return mAPI.LocalData.GetString(KEY_PASSWORD); }
            set { mAPI.LocalData.Set(KEY_PASSWORD, value); }
        }
        #endregion Local User Properties


        #region Initialization
        private OrbitXAPI mAPI;
        internal AuthClient(OrbitXAPI api)
        {
            mAPI = api;
        }

        private bool mRegistrationInProgress = false;

        private string DedicatedServerAddress
        {
            get { return string.Format("http://{0}:14448/AAA/user", mAPI.ServerAddress); }
        }

        internal delegate void SuccessFailCallback(bool success);
        internal void Initialize(SuccessFailCallback callback)
        {
            if (StartWithStandaloneClient())
                ProceedToNextInitializationStep(callback, true);
            else if (UserID == 0 || Password == "")
                QuickRegister((success) => { ProceedToNextInitializationStep(callback, success); });
            else
                ProceedToNextInitializationStep(callback, true);
        }

        private void ProceedToNextInitializationStep(SuccessFailCallback callback, bool success)
        {
            callback(success);
        }

        private bool StartWithStandaloneClient()
        {
            return false;
        }
        #endregion Initialization


        #region AAA Functions
        internal void QuickRegister(SuccessFailCallback callback)
        {
            if (mRegistrationInProgress)
                return;

            mRegistrationInProgress = true;
            var prms = new Dictionary<string, object>();
            prms["appToken"] = mAPI.ApplicationToken;
            JsonRpc.CreateCall(DedicatedServerAddress, "generateAccount", prms).Execute(result =>
            {
                bool success = false;

                if (result.IsSuccessful())
                {
                    var previousMe = Self;

                    mAPI.LocalData.Remove(GetLocalSaveKey(previousMe));
                    mAPI.PlayerPool.Remove(previousMe);

                    UserID = (int)result.Response["uid"];
                    Password = result.Response["pass"] as string;
                    mSelf = new Player(mAPI, UserID);
                    mAPI.PlayerPool.Add(mSelf);
                    SyncPlayerDetails(null);

                    success = true;
                    mAPI.Log.d("[AuthClient] Quickly registered. Player UID: " + UserID);
                }
                else
                {
                    var ex = new Packets.Exception(Packets.ExceptionCode.QUICK_REGISTER_ERROR);
                    ex.Description = result.GetErrorMessage();
                    mAPI.Events.OnProblem.Fire("[AuthClient] Quick register failed.", ex);
                }

                mRegistrationInProgress = false;
                callback(success);
            });
        }

        internal void RequestOTP(Action<bool, string> callback)
        {
            var prms = new Dictionary<string, object>();
            prms["uid"] = UserID;
            prms["pass"] = Password;
            prms["appToken"] = mAPI.ApplicationToken;
            JsonRpc.CreateCall(DedicatedServerAddress, "login", prms).ExecuteAsync((result) =>
            {
                if (result.IsSuccessful())
                    callback(true, result.Response["OTP"].ToString());
                else
                {
                    var ex = new Packets.Exception(Packets.ExceptionCode.AUTH_OTP_ERROR);
                    ex.Description = result.GetErrorMessage();
                    mAPI.Events.OnProblem.Fire("[AuthClient] Login failed.", ex);
                    callback(false, null);
                }
            });
        }

        internal void UpdatePlayerDetail(PlayerDetail detailType, object newValue)
        {
            var self = Self;
            self.ChangeDetail(detailType, newValue);
            SavePlayerInfoToLocalCache(self);

            var evData = new MyPlayerAccountUpdatedArgs(mAPI.Player, AccountEvent.DetailChanged);
            mAPI.Events.OnMyPlayerAccountUpdated.Fire(evData);
        }

        internal void SyncPlayerDetails(Action<bool> onDone)
        {
            var self = Self;
            if (self.IsValid)
            {
                var prms = new Dictionary<string, object>();
                prms["uid"] = self.UserID;
                prms["pass"] = Password;
                prms["newpass"] = Password;
                prms["newNickname"] = self.Alias;
                prms["newEmail"] = self.Email;
                prms["newAvatar"] = self.Avatar;
                JsonRpc.CreateCall(DedicatedServerAddress, "updateUser", prms).Execute(result =>
                {
                    var success = result.IsSuccessful();
                    if (!success)
                        mAPI.Events.OnProblem.Fire(
                            "[AuthClient] Player information update failed",
                            Packets.ExceptionCode.PLAYER_INFORMATION_UPDATE_FAILED,
                            result.GetErrorMessage()
                        );

                    if (onDone != null)
                        onDone(success);
                });
            }
            else
            {
                mAPI.Events.OnProblem.Fire(
                    "[AuthClient] Player detail sync failed",
                    Packets.ExceptionCode.PLAYER_INFORMATION_UPDATE_FAILED,
                    "Player has not been registered"
                );

                if (onDone != null)
                    onDone(false);
            }
        }

        private void SavePlayerInfoToLocalCache(Player player)
        {
            var data = new Dictionary<string, object>();
            data["Alias"] = player.Alias;
            data["Avatar"] = player.Avatar;
            data["Email"] = player.Email;
            mAPI.LocalData.Set(GetLocalSaveKey(player), JSON.ToJson(data, false));
        }
        #endregion AAA Functions


        #region Player Information Retrieval
        internal void FillPlayerInfo(Player player)
        {
            var prms = new Dictionary<string, object>();
            prms["uids"] = new List<long>() { player.UserID };
            JsonRpc.CreateCall(DedicatedServerAddress, "getUserProfiles", prms).Execute(result =>
            {
                if (result.OK)
                {
                    if (result.Response["result"].ToString() == "ok")
                    {
                        var data = (result.Response["infos"] as List<object>)[0] as Dictionary<string, object>;
                        var alias = data["nickname"].ToString();
                        var avatar = data["avatar"].ToString();

                        if(player.IsMe)
                        {
                            if (player.Alias != alias)
                                UpdatePlayerDetail(PlayerDetail.Alias, alias);
                            if (player.Avatar != avatar)
                                UpdatePlayerDetail(PlayerDetail.Avatar, avatar);
                        }
                        else
                            player.LoadValues(alias, avatar, "");
                        mAPI.Log.df("[AuthClient] User information for {0} loaded with alias {1} and avatar {2}", player.UserID, alias, avatar);
                    }
                    else
                        mAPI.Log.ef("[AuthClient] User information load failed with reason: {0}", result.Response["reason"].ToString());
                }
                else
                    mAPI.Log.ef("[AuthClient] User information load failed with error: {0}", result.Exception.Message);
            });
        }

        private void FillPlayerInfoFromLocalCache(Player player)
        {
            var dataJson = mAPI.LocalData.GetString(GetLocalSaveKey(player));
            if (dataJson == "")
            {
                mAPI.Log.ef("[AuthClient] No cached data for player {0} in device memory", player.UserID);
                player.LoadValues("", "", "");
            }
            else
            {
                var data = JSON.ToDictionary(dataJson);
                var alias = "";
                var avatar = "";
                var email = "";
                try
                {
                    alias = data["Alias"] as string;
                    avatar = data["Avatar"] as string;
                    email = data["Email"] as string;
                }
                catch
                {
                    mAPI.Log.ef("[PlayerPool] Excpetion while loading player data for {0} from cache {1}", player.UserID, dataJson);
                }
                player.LoadValues(alias, avatar, email);
            }
        }

        private string GetLocalSaveKey(Player player)
        {
            return "AuthClient.Players." + player.UserID;
        }
        #endregion Player Information Retrieval


        #region Aux
        internal void ForgetAuthentication()
        {
            Password = "";
        }
        #endregion Aux
    }
}
