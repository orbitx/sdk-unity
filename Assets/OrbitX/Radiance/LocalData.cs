﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrbitXSDK
{
    public class LocalData
    {
        #region Types and Data
        private enum DataType
        {
            Bool = 1,
            Int = 2,
            Long = 3,
            String = 4,
            Float = 5,
        }

        const string OrbitPrefix = "!OX";
        private string mPrefix;

        private Dictionary<string, DataType> mKeys = new Dictionary<string, DataType>();
        private Dictionary<string, object> mValues = new Dictionary<string, object>();
        #endregion Types and Data


        #region Load/Save
        private bool mUsePlayerPrefs;
        private string mCache;
        public string Cache
        {
            get { return mCache; }
        }

        internal LocalData(int instanceNumber, string dataFixture)
        {
            if (string.IsNullOrEmpty(dataFixture))
            {
                mUsePlayerPrefs = true;
                mPrefix = (Application.isEditor ? "*" : "!") + instanceNumber.ToString();
                mCache = PlayerPrefs.GetString(OrbitPrefix + mPrefix, "{'data': []}");
            }
            else
            {
                mUsePlayerPrefs = false;
                mCache = dataFixture;
            }
            LoadImp();
        }

        private void LoadImp()
        {
            var root = JSON.ToDictionary(mCache);
            var data = root["data"] as List<object>;

            for (int i = 0; i < data.Count; ++i)
            {
                var node = data[i] as List<object>;
                var name = node[0] as string;
                var type = (DataType)(int)node[1];
                mKeys[name] = type;

                if (type == DataType.Bool)
                    mValues[name] = (int)node[2] == 1;
                else if (type == DataType.Int)
                    mValues[name] = (int)node[2];
                else if (type == DataType.Long)
                    mValues[name] = long.Parse(node[2] as string);
                else if (type == DataType.String)
                    mValues[name] = node[2] as string;
                else if (type == DataType.Float)
                    mValues[name] = (float)node[2];
                else
                    Log.e("[LocalData] Unknown type: " + type);
            }
        }

        private void SaveImp()
        {
            var data = new List<object>();
            foreach (var typeEntry in mKeys)
            {
                var node = new List<object>();

                var type = typeEntry.Value;
                node.Add(typeEntry.Key);
                node.Add(type);

                var value = mValues[typeEntry.Key];
                if (type == DataType.Bool)
                    node.Add((bool)value ? 1 : 0);
                else if (type == DataType.Long)
                    node.Add(value.ToString());
                else
                    node.Add(value);

                data.Add(node);
            }

            var dic = new Dictionary<string, object>() { { "data", data } };
            mCache = JSON.ToJson(dic, !mUsePlayerPrefs);

            if (mUsePlayerPrefs)
            {
                PlayerPrefs.SetString(OrbitPrefix + mPrefix, mCache);
                PlayerPrefs.Save();
            }
        }

        internal void Save(bool fromUnityThread)
        {
            if (fromUnityThread)
                SaveImp();
            else
                UnityBroker.PostToUnity(() => { SaveImp(); });
        }
        #endregion Load/Save


        #region Set
        static private DataType GetType(object value)
        {
            if (value is bool)
                return DataType.Bool;
            else if (value is long)
                return DataType.Long;
            else if (value is int)
                return DataType.Int;
            else if (value is float)
                return DataType.Float;
            else if (value is string)
                return DataType.String;
            else
                throw new ArgumentException("Can not store value of type " + value.GetType().Name + " automatically");
        }

        public void Set(string key, object value)
        {
            mKeys[key] = GetType(value);
            mValues[key] = value;
        }
        #endregion Set


        #region Get
        public bool GetBool(string key, bool defaultValue = false)
        {
            if (mKeys.ContainsKey(key))
                return (bool)mValues[key];
            else
                return defaultValue;
        }

        public int GetInt(string key, int defaultValue = 0)
        {
            if (mKeys.ContainsKey(key))
                return (int)mValues[key];
            else
                return defaultValue;
        }

        public long GetLong(string key, long defaultValue = 0)
        {
            if (mKeys.ContainsKey(key))
                return (long)mValues[key];
            else
                return defaultValue;
        }

        public float GetFloat(string key, float defaultValue = 0)
        {
            if (mKeys.ContainsKey(key))
                return (float)mValues[key];
            else
                return defaultValue;
        }

        public string GetString(string key, string defaultValue = "")
        {
            if (mKeys.ContainsKey(key))
                return (string)mValues[key];
            else
                return defaultValue;
        }
        #endregion Get


        #region Deletion
        internal void Remove(string keyName)
        {
            mKeys.Remove(keyName);
            mValues.Remove(keyName);
        }

        internal void Clear()
        {
            mKeys.Clear();
            mValues.Clear();
            Save(true);
        }

        static public void EditorClearCommand()
        {
            new LocalData(0, null).Clear();
            new LocalData(1, null).Clear();
            Log.d("[LocalData] Cleared.");
        }
        #endregion Deletion
    }
}
