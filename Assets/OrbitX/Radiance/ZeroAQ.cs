﻿using System.Threading;
using System.Collections.Generic;
using System;

public class ZeroAQ
{
    private bool mRunning;
    private Thread mThread;
    private AutoResetEvent mProcessEvent;
    private List<DelayedAction> mQueue = new List<DelayedAction>();

    public bool QueueRunning
    {
        get { return mRunning; }
    }

    public Thread Thread
    {
        get { return mThread; }
    }

    public ZeroAQ(string name)
    {
        mRunning = false;
        mQueue = new List<DelayedAction>();
        mProcessEvent = new AutoResetEvent(false);
        mThread = new Thread(ThreadRoutine);
        mThread.Name = name;
        mThread.Start();
    }

    private void ThreadRoutine()
    {
        mRunning = true;
        List<DelayedAction> actionsToProcess = new List<DelayedAction>();

        while (mRunning)
        {
            // Caluculate the time for next wake
            int nextTimeout = GetNextTimeout();
            
            // Wait for a call
            if(nextTimeout != 0)
                mProcessEvent.WaitOne(nextTimeout);

            // Move calls to a local list
            lock (mQueue)
            {
                int now = Environment.TickCount;
                while (mQueue.Count > 0)
                    if (mQueue[0].IsDue(now))
                    {
                        actionsToProcess.Add(mQueue[0]);
                        mQueue.RemoveAt(0);
                    }
                    else
                        break;
            }

            // Process the calls
            for (int i = 0; i < actionsToProcess.Count; ++i)
                actionsToProcess[i].Action();

            // Clear
            actionsToProcess.Clear();
        }
    }

    private int GetNextTimeout()
    {
        int now = Environment.TickCount;
        lock (mQueue)
        {
            if (mQueue.Count > 0)
            {
                if (mQueue[0].AbsoluteTicks - now <= 10)
                    return 0;
                else
                    return mQueue[0].AbsoluteTicks - now;
            }
            else
                return 1000;
        }
    }

    public void PostAction(Action action)
    {
        PostActionDelayed(0, action);
    }

    public void PostActionDelayed(int millis, Action action)
    {
        var delayedAction = new DelayedAction()
        {
            Action = action,
            AbsoluteTicks = Environment.TickCount + millis
        };

        lock (mQueue)
        {
            mQueue.Add(delayedAction);
            mQueue.Sort((action1, action2) => DelayedAction.Compare(action1, action2));
        }

        mProcessEvent.Set();
    }

    public void DropQueue()
    {
        mRunning = false;
    }

    private class DelayedAction
    {
        public int AbsoluteTicks;
        public Action Action;

        static public int Compare(DelayedAction action1, DelayedAction action2)
        {
            if (action1.AbsoluteTicks > action2.AbsoluteTicks)
                return 1;
            else if (action1.AbsoluteTicks == action2.AbsoluteTicks)
                return 0;
            else
                return -1;
        }

        public bool IsDue(int now)
        {
            return AbsoluteTicks - now <= 10;
        }
    }
}
