﻿using System.Collections.Generic;

namespace OrbitXSDK
{
    public enum Modification
    {
        Insert,
        Modify,
        Delete
    }

    public class ModifiedEntry
    {
        public string Path;
        public Modification Modification;
        public Variant PreviousValue;
        public Variant NewValue;

        public override string ToString()
        {
            return string.Format("{0} \"{1}\": {2} -> {3}", Modification, Path, PreviousValue, NewValue);
        }
    }
}
