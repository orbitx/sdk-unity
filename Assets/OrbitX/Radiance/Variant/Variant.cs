﻿using System;
using System.Collections.Generic;

namespace OrbitXSDK
{
    public enum VariantTypes
    {
        Null,
        Bool,
        Int,
        Long,
        Float,
        Double,
        Byte,
        Char,
        String,
        List,
        Dictionary,
        Object
    }

    public class Variant : IEquatable<Variant>
    {
        #region Internal Data
        static private Variant mNullVariant = new Variant(null, VariantTypes.Null);

        private object mData;
        private VariantTypes mType;

        public VariantTypes Type
        {
            get { return mType; }
        }
        #endregion Internal Data


        #region Constructor
        private Variant(object data, VariantTypes type)
        {
            mData = data;
            mType = type;
        }
        #endregion Constructor


        #region Handling Simple types
        public bool IsNull
        {
            get { return mType == VariantTypes.Null; }
        }

        static public Variant Null
        {
            get { return mNullVariant; }
        }

        public bool AsBool
        {
            get { return mType == VariantTypes.Bool ? (bool)mData : false; }
        }

        static public implicit operator Variant(bool value)
        {
            return new Variant(value, VariantTypes.Bool);
        }

        public int AsInt
        {
            get { return mType == VariantTypes.Int ? (int)mData : 0; }
        }

        static public implicit operator Variant(int value)
        {
            return new Variant(value, VariantTypes.Int);
        }

        public long AsLong
        {
            get { return mType == VariantTypes.Long ? (long)mData : (mType == VariantTypes.Int ? (int)mData : 0); }
        }

        static public implicit operator Variant(long value)
        {
            return new Variant(value, VariantTypes.Long);
        }

        public float AsFloat
        {
            get { return mType == VariantTypes.Float ? (float)mData : (mType == VariantTypes.Double ? (float)(double)(mData) : 0); }
        }

        static public implicit operator Variant(float value)
        {
            return new Variant(value, VariantTypes.Float);
        }

        public double AsDouble
        {
            get { return mType == VariantTypes.Double ? (double)mData : 0; }
        }

        static public implicit operator Variant(double value)
        {
            return new Variant(value, VariantTypes.Double);
        }

        public byte AsByte
        {
            get { return mType == VariantTypes.Byte ? (byte)mData : (byte)0; }
        }

        static public implicit operator Variant(byte value)
        {
            return new Variant(value, VariantTypes.Byte);
        }

        public char AsChar
        {
            get { return mType == VariantTypes.Char ? (char)mData : (char)0; }
        }

        static public implicit operator Variant(char value)
        {
            return new Variant(value, VariantTypes.Char);
        }

        public string AsString
        {
            get { return mType == VariantTypes.String ? (string)mData : ""; }
        }

        static public implicit operator Variant(string value)
        {
            return new Variant(value, VariantTypes.String);
        }

        public object AsObject
        {
            get { return mData; }
        }

        static public Variant FromRawObject(object value)
        {
            return new Variant(value, VariantTypes.Object);
        }
        #endregion Value type retrievals


        #region Handling Lists
        private List<Variant> mList
        {
            get { return mData as List<Variant>; }
        }

        static public Variant CreateList()
        {
            return new Variant(new List<Variant>(), VariantTypes.List);
        }

        public void Add(Variant item, bool fireCallback = true)
        {
            if (mType != VariantTypes.List)
                throw new InvalidOperationException("[Variant] Can not add a list item to variant of type " + mType);

            var list = mList;
            list.Add(item);

            if (fireCallback)
            {
                var mod = new ModifiedEntry() { Modification = Modification.Insert };
                mod.Path = GetModificationPath(list.Count - 1);
                mod.PreviousValue = Null;
                mod.NewValue = item;
                FireCallback(mod);
            }

            if (mChangedCallback != null)
                item.Bind(mChangedCallback, GetModificationPath(list.Count - 1));
        }

        public void RemoveAt(int index, bool fireCallback = true)
        {
            if (mType != VariantTypes.List)
                throw new InvalidOperationException("[Variant] Can not remove by index on a variant of type " + mType);

            var list = mList;
            if (index < 0 || index >= list.Count)
                throw new IndexOutOfRangeException();

            if (fireCallback)
            {
                var mod = new ModifiedEntry() { Modification = Modification.Delete };
                mod.Path = GetModificationPath(index);
                mod.PreviousValue = list[index];
                mod.NewValue = Null;
                FireCallback(mod);
            }

            list[index].Unbind();
            list.RemoveAt(index);
        }

        public void SetListItem(int index, Variant value, bool fireCallback = true)
        {
            if (mType != VariantTypes.List)
                throw new InvalidOperationException("[Variant] List indexer is not applicable to variant of type " + mType);

            var list = mList;
            if (index < 0 || index >= list.Count)
                throw new IndexOutOfRangeException();

            list[index].Unbind();
            if (fireCallback)
            {
                ModifiedEntry mod = new ModifiedEntry() { Modification = Modification.Modify };
                mod.Modification = Modification.Modify;
                mod.PreviousValue = list[index];

                list[index] = mod.NewValue = value;
                mod.Path = GetModificationPath(index);
                FireCallback(mod);
            }
            else
                list[index] = value;

            if (mChangedCallback != null)
                value.Bind(mChangedCallback, GetModificationPath(index));
        }

        public Variant this[int index]
        {
            get
            {
                if (mType == VariantTypes.List)
                    return mList[index];
                else
                    throw new InvalidOperationException("[Variant] List indexer is not applicable to variant of type " + mType);
            }

            set { SetListItem(index, value, true); }
        }

        private string GetModificationPath(int key)
        {
            var result = "[" + key + "]";
            return mCachedPath == null ? result : mCachedPath + result;
        }
        #endregion Handling Lists


        #region Handling Dictionaries
        private Dictionary<string, Variant> mDictionary
        {
            get { return mData as Dictionary<string, Variant>; }
        }

        static public Variant CreateDictionary(ChangedCallback callbackOnChange = null)
        {
            var result = new Variant(new Dictionary<string, Variant>(), VariantTypes.Dictionary);
            result.mChangedCallback = callbackOnChange;
            return result;
        }

        public bool TryGetValue(string key, out Variant value)
        {
            return mDictionary.TryGetValue(key, out value);
        }

        public void Add(string key, Variant value, bool fireCallback = true)
        {
            if (mType != VariantTypes.Dictionary)
                throw new InvalidOperationException("[Variant] Dictionary indexer is not applicable to variant of type " + mType);

            var dic = mDictionary;
            if (dic.ContainsKey(key))
                dic[key].Unbind();

            if (!fireCallback)
                mDictionary[key] = value;
            else
            {
                ModifiedEntry mod = new ModifiedEntry();
                if (dic.ContainsKey(key))
                {
                    mod.Modification = Modification.Modify;
                    mod.PreviousValue = dic[key];
                }
                else
                {
                    mod.Modification = Modification.Insert;
                    mod.PreviousValue = Null;
                }

                dic[key] = mod.NewValue = value;
                mod.Path = GetModificationPath(key);
                FireCallback(mod);
            }

            if (mChangedCallback != null)
                value.Bind(mChangedCallback, GetModificationPath(key));
        }

        static private string GetKeyFromGivenArgument(object key)
        {
            try
            {
                var result = key as string;
                if (result == null)
                    throw new InvalidOperationException();
                return result;
            }
            catch
            {
                throw new ArgumentException("[Variant] Need a string key to access dictionary");
            }
        }

        public Dictionary<string, Variant>.KeyCollection Keys
        {
            get
            {
                if (mType == VariantTypes.Dictionary)
                    return mDictionary.Keys;

                throw new InvalidOperationException("[Variant] Retrieval of keys in a dictionary is not applicable to variant of type " + mType);
            }
        }

        public bool ContainsKey(string key)
        {
            if (mType == VariantTypes.Dictionary)
                return mDictionary.ContainsKey(key);

            throw new InvalidOperationException("[Variant] Checking for a given key is not applicable to variant of type " + mType);
        }

        public Variant this[string key]
        {
            get
            {
                if (mType == VariantTypes.Dictionary)
                    return mDictionary[key];
                else
                    throw new InvalidOperationException("[Variant] Dictionary indexer is not applicable to variant of type " + mType);
            }

            set { Add(key, value, true); }
        }

        private string GetModificationPath(string key)
        {
            return mCachedPath == null ? key : (mCachedPath + "." + key);
        }
        #endregion Handling Dictionaries


        #region Lists and Dictionaries common operations
        public int Count
        {
            get
            {
                if (mType == VariantTypes.List)
                    return mList.Count;
                else if (mType == VariantTypes.Dictionary)
                    return mDictionary.Count;
                else
                    throw new InvalidOperationException("[Variant] Count is not applicable to variant of type " + mType);
            }
        }

        public bool Remove(object keyOrItem, bool fireCallback = true)
        {
            bool result;
            if (mType == VariantTypes.List)
            {
                int index = mList.IndexOf(FromRawObject(keyOrItem));
                if (index != -1)
                {
                    result = true;
                    RemoveAt(index, fireCallback);
                }
                else
                    result = false;
            }
            else if (mType == VariantTypes.Dictionary)
            {
                var key = GetKeyFromGivenArgument(keyOrItem);
                var dic = mDictionary;
                if (dic.ContainsKey(key))
                {
                    result = true;
                    var prevValue = dic[key];
                    prevValue.Unbind();
                    dic.Remove(key);

                    if (fireCallback)
                    {
                        ModifiedEntry mod = new ModifiedEntry() { Modification = Modification.Delete };
                        mod.PreviousValue = prevValue;
                        mod.NewValue = Null;
                        mod.Path = GetModificationPath(key);
                        FireCallback(mod);
                    }
                }
                else
                    result = false;
            }
            else
                throw new InvalidOperationException("[Variant] Removing elements is not supported for type " + mType);

            return result;
        }

        public void Clear(bool fireCallback = true)
        {
            if (mType == VariantTypes.List)
            {
                if (fireCallback)
                    while (Count > 0)
                        RemoveAt(0);
                else
                    mList.Clear();
            }
            else if (mType == VariantTypes.Dictionary)
            {
                var dic = mDictionary;
                foreach (var key in dic.Keys)
                {
                    var prevValue = dic[key];
                    prevValue.Unbind();

                    if (fireCallback)
                    {
                        var mod = new ModifiedEntry() { Modification = Modification.Delete };
                        mod.PreviousValue = prevValue;
                        mod.NewValue = Null;
                        mod.Path = GetModificationPath(key);
                        FireCallback(mod);
                    }
                }
                mDictionary.Clear();
            }
            else
                throw new InvalidOperationException("[Variant] Can not clear a variant which is not a list nor dictionary: " + mType);
        }
        #endregion Lists and Dictionaries common operations


        #region Change callback
        public delegate void ChangedCallback(ModifiedEntry modifiedEntry);
        private ChangedCallback mChangedCallback;

        private string mCachedPath = null;

        private void Bind(ChangedCallback callback, string cachedPath)
        {
            mCachedPath = cachedPath;
            mChangedCallback = callback;
        }

        private void Unbind()
        {
            mChangedCallback = null;
            mCachedPath = null;
        }

        private void FireCallback(ModifiedEntry entry)
        {
            if (mChangedCallback != null)
                mChangedCallback(entry);
        }
        #endregion Change callback


        #region System overrides
        public override bool Equals(object obj)
        {
            if (obj == null)
                return this == null;

            Variant x = obj as Variant;
            if (ReferenceEquals(x, null))
                return false;

            return mData.Equals(x.mData);
        }

        public bool Equals(Variant other)
        {
            return mData == other.mData;
        }

        public override int GetHashCode()
        {
            return mType == VariantTypes.Null ? 0 : mData.GetHashCode();
        }

        public override string ToString()
        {
            if (mType == VariantTypes.List)
                return "VariantList(" + mList.Count + ")";
            else if (mType == VariantTypes.Dictionary)
                return mDictionary.ToHumanReadable();
            else
                return mData == null ? "(null)" : string.Format("{0} ({1})", mData, mType);
        }
        #endregion System overrides
    }
}
