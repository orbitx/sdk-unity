﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrbitXSDK
{
    internal class UnityBroker : ZeroAQ
    {
        #region Singleton
        static private MonoHandler mHandler;
        static private UnityBroker mInstance;
        static internal UnityBroker Instance
        {
            get
            {
                if (mInstance == null)
                    mInstance = new UnityBroker();

                return mInstance;
            }
        }
        #endregion Singleton


        #region Startup
        static internal bool IsEditing
        {
            get { return Application.isEditor && !Application.isPlaying; }
        }

        private GameObject mRootObject;

        private UnityBroker() : base("UnityBroker")
        {
            mRootObject = new GameObject("UnityBroker");
            mHandler = mRootObject.AddComponent<MonoHandler>();
            mRootObject.hideFlags = HideFlags.HideInHierarchy;
            UnityEngine.Object.DontDestroyOnLoad(mRootObject);
        }

        internal GameObject SDKRootObject
        {
            get { return mRootObject; }
        }

        internal void Start()
        { }
        #endregion Startup


        #region Session frame updates
        private List<Session> mSessions = new List<Session>();

        static internal void RegisterSession(Session session)
        {
            Instance.mSessions.Add(session);
        }

        static internal void UnregisterSession(Session session)
        {
            Instance.mSessions.Remove(session);
        }
        #endregion

        #region Calls from engine
        internal void Update()
        {
            ProcessUnityCalls();
            foreach (var session in mSessions)
                session.FrameRenderUpdate();
        }

        internal void ApplicationExiting()
        {
            DropQueue();
            Network.Instance.DropQueue();
            OrbitX.StopAllConnections(false);
        }
        #endregion Calls from engine


        #region X-Thread synchronization
        static private List<Action> mUnityCalls = new List<Action>();

        static internal void PostToUnity(Action action)
        {
            lock (mUnityCalls)
                mUnityCalls.Add(action);
        }

        static internal void PostToUnityDelayed(float delayMillis, Action action)
        {
            mHandler.StartCoroutine(DelayAndPost(delayMillis, action));
        }

        static System.Collections.IEnumerator DelayAndPost(float delayMillis, Action action)
        {
            yield return new WaitForSecondsRealtime(delayMillis * 0.001f);
            action();
        }

        private void ProcessUnityCalls()
        {
            List<Action> calls = new List<Action>();
            lock (mUnityCalls)
            {
                calls.AddRange(mUnityCalls);
                mUnityCalls.Clear();
            }

            for (int i = 0; i < calls.Count; ++i)
                try
                {
                    calls[i]();
                }
                catch(Exception ex)
                {
                    Debug.LogException(ex);
                }
        }
        #endregion X-Thread synchronization


        #region Private Classes
        public class MonoHandler : MonoBehaviour
        {
            void Update()
            {
                Instance.Update();
            }

            public void OnApplicationFocus(bool focus)
            {
                if (!OnWindows)
                {
                    if (focus)
                        OrbitX.ResumeAllConnections();
                    else
                    {
                        OrbitX.SaveAllLocalData();
                        OrbitX.StopAllConnections(false);
                    }
                }
            }

            public void OnApplicationQuit()
            {
                if (OnWindows)
                {
                    OrbitX.SaveAllLocalData();
                    Instance.ApplicationExiting();
                }
            }

            private bool OnWindows
            {
                get { return Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer; }
            }
        }
        #endregion Private Classes
    }
}