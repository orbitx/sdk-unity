﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

static public class JSON
{
    #region Write JSON
    static public string ToJson(Dictionary<string, object> value, bool indent)
    {
        return JsonConvert.SerializeObject(value, indent ? Formatting.Indented : Formatting.None);
    }

    static public string ToJson(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }
    #endregion Write JSON


    #region Read JSON
    static public T Parse<T>(string strJson)
    {
        return JsonConvert.DeserializeObject<T>(strJson);
    }

    static public Dictionary<string, object> ToDictionary(string strJson)
    {
        var rawObject = JsonConvert.DeserializeObject(strJson);
        return ProcessJsonObject(rawObject as JObject);
    }

    static private object ProcessJsonValue(JToken entry)
    {
        switch (entry.Type)
        {
            case JTokenType.Boolean:
                return (bool)entry;

            case JTokenType.Float:
                return (float)entry;

            case JTokenType.Integer:
                return (int)entry;

            case JTokenType.String:
                return (string)entry;

            case JTokenType.Array:
                return ProcessJsonArray(entry as JArray);

            case JTokenType.Object:
                return ProcessJsonObject(entry as JObject);

            default:
                return null;
        }
    }

    static private Dictionary<string, object> ProcessJsonObject(JObject rawJson)
    {
        var result = new Dictionary<string, object>();
        var x = rawJson.GetEnumerator();
        while (x.MoveNext())
            result[x.Current.Key] = ProcessJsonValue(x.Current.Value);
        return result;
    }

    static private List<object> ProcessJsonArray(JArray rawJson)
    {
        List<object> result = new List<object>();
        for (int i = 0; i < rawJson.Count; ++i)
            result.Add(ProcessJsonValue(rawJson[i]));
        return result;
    }
    #endregion Read JSON
}
