﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrbitXSDK
{
    static public class JsonRpc
    {
        #region Internal functionality
        static private int mCallID = 0;

        static public string GenerateRPCPayload(string methodName, Dictionary<string, object> prms)
        {
            var payload = new Dictionary<string, object>();
            payload["jsonrpc"] = "2.0";
            payload["id"] = mCallID++;
            payload["method"] = methodName;
            payload["params"] = prms != null ? prms : new Dictionary<string, object>();
            return JSON.ToJson(payload, false);
        }

        static internal Call CreateCall(string url, string method, Dictionary<string, object> prms)
        {
            return new Call(url, method, prms);
        }
        #endregion Internal functionality


        #region Interface
        static public Call CreateCall(string url, string method)
        {
            return new Call(url, method, null);
        }
        #endregion Interface

        public class Call
        {
            #region Internal data
            internal delegate void Callback(Call response);

            private string mMethodName;
            private Dictionary<string, object> mRequestParams;
            private Dictionary<string, object> mResponseParams;
            private HTTP.Call mHttpCall;
            private Callback mCallback;
            private Exception mException;
            #endregion Internal data


            #region Information
            internal bool OK
            {
                get { return mResponseParams != null; }
            }

            internal Dictionary<string, object> RequestParameters
            {
                get { return mRequestParams; }
            }

            internal Dictionary<string, object> Response
            {
                get { return mResponseParams; }
            }

            internal string MethodName
            {
                get { return mMethodName; }
            }
            public Exception Exception
            {
                get { return mException != null ? mException : mHttpCall.Exception; }
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(mHttpCall.Url).Append(" ").AppendLine(mMethodName);
                sb.Append("With: ").AppendLine(mRequestParams == null ? "nothing" : mRequestParams.ToString());
                sb.Append("Resulting: ").AppendLine(mResponseParams == null ? "nothing" : mResponseParams.ToString());
                return sb.ToString();
            }
            #endregion Information


            #region Internal functionality
            internal Call(string url, string methodName, Dictionary<string, object> prms)
            {
                mRequestParams = prms;
                mMethodName = methodName;
                mHttpCall = HTTP.Post(url, GenerateRPCPayload(methodName, prms), "application/json");
            }

            internal void Execute(ExecutionMode execMode, Callback callback)
            {
                mCallback = callback;

                mHttpCall.Execute(execMode, (response) =>
                {
                    if (!response.OK)
                        ProcessResult(null);
                    else
                    {
                        try
                        {
                            mResponseParams = JSON.ToDictionary(response.ResponseString);

                            if (mResponseParams.ContainsKey("error"))
                            {
                                var errorStr = mResponseParams["error"].ToString();
                                ProcessResult(new Exception("JSON-RPC call failed with error: " + errorStr));
                            }
                            else if (mResponseParams.ContainsKey("result"))
                                mResponseParams = mResponseParams["result"] as Dictionary<string, object>;

                            ProcessResult(null);
                        }
                        catch (Exception ex)
                        {
                            ProcessResult(ex);
                        }
                    }
                });
            }

            private void ProcessResult(Exception ex)
            {
                mException = ex;
                if(mCallback != null)
                    mCallback(this);
            }

            internal void Execute(Callback callback)
            {
                Execute(ExecutionMode.InUnity, callback);
            }

            internal void ExecuteAsync(Callback callback)
            {
                Execute(ExecutionMode.Async, callback);
            }
            #endregion Internal functionality
        }
    }
}
