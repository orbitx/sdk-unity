﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace OrbitXSDK
{
    static public class HTTP
    {
        #region Post call interface
        /// <summary>
        /// Creates a call to specified address via post method. 
        /// Execute it synchroneously via Call.Execute() or asynchroneously via Call.ExecuteAsync
        /// </summary>
        /// <param name="url">Address to perform post call</param>
        /// <param name="contentType">Content type</param>
        /// <returns>A Call object ready to perform its operation</returns>
        static public Call Post(string url, string contentType = "application/x-www-form-urlencoded")
        {
            return Post(url, "", contentType);
        }

        static internal Call Post(string url, Variant prms, string contentType = "application/x-www-form-urlencoded")
        {
            StringBuilder postBody = new StringBuilder();
            foreach(var key in prms.Keys)
                postBody
                    .Append(Uri.EscapeUriString(key))
                    .Append('=')
                    .Append(Uri.EscapeUriString(prms[key].ToString()))
                    .Append('&');

            return Post(url, postBody.ToString(), contentType);
        }

        /// <summary>
        /// Creates a call to specified address via post method and the given body. 
        /// Execute it synchroneously via Call.Execute() or asynchroneously via Call.ExecuteAsync
        /// </summary>
        /// <param name="url">Address to perform post call</param>
        /// <param name="body">Request body as a string</param>
        /// <param name="contentType">Content type</param>
        /// <returns>A Call object ready to perform its operation</returns>
        static public Call Post(string url, string body, string contentType = "application/x-www-form-urlencoded")
        {
            return new Call(Calls.Post, url, body, contentType);
        }
        #endregion Post call interface

        public class Call
        {
            public delegate void Callback(Call response);

            #region Data
            private Calls mCallType;
            private string mUrl;
            private string mParameters;
            private string mContentType;
            private Exception mException;
            private string mResponse;
            private int mStatusCode;
            private ExecutionMode mExecutionMode;
            private Callback mCallback;
            private AsyncCallback mResponseCallback = new AsyncCallback(ResponseCallback);
            private WebRequest mRequestInst;
            #endregion Data

            #region Call Information
            public string Url
            {
                get { return mUrl; }
            }

            public string Parameters
            {
                get { return mParameters; }
            }

            public string ContentType
            {
                get { return mContentType; }
            }

            public Calls Type
            {
                get { return mCallType; }
            }

            public Exception Exception
            {
                get { return mException; }
            }

            public string ResponseString
            {
                get { return mResponse; }
            }

            public int Status
            {
                get { return mStatusCode; }
            }

            public bool Done
            {
                get { return mException == null && mStatusCode > 0; }
            }

            public bool OK
            {
                get { return mException == null && mStatusCode == 200; }
            }
            #endregion Call Information

            #region Implementation
            public Call(Calls callType, string url, string parametersString, string contentType)
            {
                mCallType = callType;
                mUrl = url;
                mParameters = parametersString;
                mContentType = contentType;
                mStatusCode = 0;
                mException = null;
                mResponse = null;
            }

            private void ProcessResult(Exception ex, string response, int statusCode)
            {
                mException = ex;
                mResponse = response;
                mStatusCode = statusCode;

                if (mExecutionMode == ExecutionMode.Async)
                    FireCallback();
                else
                    UnityBroker.PostToUnity(() => FireCallback());
            }

            private void FireCallback()
            {
                if (mCallback != null)
                    mCallback(this);
            }

            private void ProcessHttpResponse(HttpWebResponse response)
            {
                if (response == null)
                {
                    ProcessResult(new Exception("No response"), null, 0);
                    return;
                }

                int statusCode = (int)response.StatusCode;

                string result = "";
                try
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                        result = sr.ReadToEnd();
                }
                catch
                { }

                response.Close();
                ProcessResult(null, result, statusCode);
            }

            static private void ResponseCallback(IAsyncResult result)
            {
                Call theCall = (Call)result.AsyncState;
                HttpWebResponse response = (HttpWebResponse)theCall.mRequestInst.EndGetResponse(result);
                theCall.ProcessHttpResponse(response);
            }

            internal void Execute(ExecutionMode execMode, Callback callback)
            {
                mExecutionMode = execMode;
                mCallback = callback;

                Network.Instance.PostAction(() =>
                {
                    try
                    {
                        mRequestInst = WebRequest.Create(mUrl);
                        mRequestInst.Method = "POST";
                        mRequestInst.ContentType = mContentType;
                        mRequestInst.Proxy = null;

                        // Put the post body in
                        byte[] bytes = Encoding.UTF8.GetBytes(mParameters);
                        mRequestInst.ContentLength = bytes.Length;
                        Stream os = mRequestInst.GetRequestStream();
                        os.Write(bytes, 0, bytes.Length);
                        os.Close();

                        // Get the response
                        mRequestInst.BeginGetResponse(mResponseCallback, this);
                    }
                    catch (WebException e)
                    {
                        ProcessHttpResponse((HttpWebResponse)e.Response);
                    }
                    catch (Exception ex)
                    {
                        ProcessResult(ex, null, 0);
                    }
                });
            }
            #endregion Implementation

            #region Interface
            /// <summary>
            /// This method performs the post operation and calls the given callback when the call finishes.
            /// This callback is executed within unity's thread so there is no need for cross-thread functionality panic.
            /// </summary>
            /// <param name="callback">when the call finishes this callback is executed</param>
            public void Execute(Callback callback)
            {
                Execute(ExecutionMode.InUnity, callback);
            }

            /// <summary>
            /// This method executes the post operation and executes the given callback immediatly
            /// in the network thread. The Execute() method should be preferred if there is a need 
            /// to do stuff on unity's thread.
            /// </summary>
            /// <param name="callback">when the call finishes this callback is executed</param>
            public void ExecuteAsync(Callback callback)
            {
                Execute(ExecutionMode.Async, callback);
            }

            public override string ToString()
            {
                if (!Done)
                    return mException.Message;
                else
                    return String.Format("[{0}] {1}", mStatusCode, mResponse);
            }
            #endregion Interface
        }
    }
}
