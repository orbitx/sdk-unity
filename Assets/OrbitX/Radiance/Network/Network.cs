﻿namespace OrbitXSDK
{
    internal class Network : ZeroAQ
    {
        private static Network mSingleton;
        static public Network Instance
        {
            get
            {
                if (mSingleton == null)
                    mSingleton = new Network();

                return mSingleton;
            }
        }

        private Network() : base("Network")
        {

        }
    }
}
