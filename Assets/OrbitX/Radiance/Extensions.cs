﻿using OrbitXSDK.Packets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrbitXSDK
{
    internal static class Extensions
    {
        internal static string ToHumanReadable<TKey, TValue>(this Dictionary<TKey, TValue> input)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            var it = input.GetEnumerator();
            bool first = true;
            while (it.MoveNext())
            {
                if (!first)
                    sb.Append(", ");
                else
                    first = false;

                sb.AppendFormat("\"{0}\": {1}", it.Current.Key, it.Current.Value);
            }
            sb.Append("}");

            return sb.ToString();
        }

        internal static object ConvertToObject(this DOMNode node, DeveloperLogs log)
        {
            if (node == null)
                return null;

            switch (node.Type)
            {
                case Packets.ValueType.BOOL:
                    return node.BoolVal;

                case Packets.ValueType.BYTE:
                    return node.ByteValue;

                case Packets.ValueType.INT_16:
                    return node.Int16Val;

                case Packets.ValueType.INT_32:
                    return node.Int32Val;

                case Packets.ValueType.INT_64:
                    return node.Int64Val;

                case Packets.ValueType.DOUBLE:
                    return node.DoubleVal;

                case Packets.ValueType.STRING:
                    return node.StrVal;

                case Packets.ValueType.ARRAY:
                    {
                        var result = new List<object>();
                        for (int i = 0; i < node.Array.Count; ++i)
                            result.Add(ConvertToObject(node.Array[i], log));
                        return result.ToArray();
                    }

                case Packets.ValueType.MAP:
                    {
                        var result = new Dictionary<string, object>();
                        foreach (var entry in node.Children)
                            result[entry.Key] = ConvertToObject(entry.Value, log);
                        return result;
                    }

                default:
                    log.ef("[DOMNode] Can not convert node value of type {0} to Variant", node.Type.ToString());
                    return null;
            }
        }

        internal static Variant ConvertToVariant(this DOMNode node, DeveloperLogs log, Variant.ChangedCallback callback = null)
        {
            if (node == null)
                return null;

            switch (node.Type)
            {
                case Packets.ValueType.ARRAY:
                    var listResult = Variant.CreateList();
                    for (int i = 0; i < node.Array.Count; ++i)
                        listResult.Add(ConvertToVariant(node.Array[i], log, callback));
                    return listResult;

                case Packets.ValueType.MAP:
                    var dicResult = Variant.CreateDictionary();
                    foreach (var entry in node.Children)
                        dicResult[entry.Key] = ConvertToVariant(entry.Value, log, callback);
                    return dicResult;

                case Packets.ValueType.BOOL:
                    return node.BoolVal;

                case Packets.ValueType.BYTE:
                    return node.ByteValue;

                case Packets.ValueType.INT_16:
                    return node.Int16Val;

                case Packets.ValueType.INT_32:
                    return node.Int32Val;

                case Packets.ValueType.INT_64:
                    return node.Int64Val;

                case Packets.ValueType.DOUBLE:
                    return node.DoubleVal;

                case Packets.ValueType.STRING:
                    return node.StrVal;

                default:
                    log.ef("[DOMNode] Can not convert node value of type {0} to Variant", node.Type);
                    return null;
            }
        }

        internal static Packets.DOMNode ConvertToDOMNode(object value)
        {
            var node = new Packets.DOMNode();
            if (value is int)
            {
                node.Type = Packets.ValueType.INT_32;
                node.Int32Val = (int)value;
            }
            else if (value is string)
            {
                node.Type = Packets.ValueType.STRING;
                node.StrVal = (string)value;
            }
            else if (value is float)
            {
                node.Type = Packets.ValueType.DOUBLE;
                node.DoubleVal = (float)value;
            }
            else if (value is double)
            {
                node.Type = Packets.ValueType.DOUBLE;
                node.DoubleVal = (double)value;
            }
            else if (value is bool)
            {
                node.Type = Packets.ValueType.BOOL;
                node.BoolVal = (bool)value;
            }
            else if (value is IDictionary)
            {
                node.Type = Packets.ValueType.MAP;
                node.Children = new Dictionary<string, Packets.DOMNode>();
                var it = (value as IDictionary).GetEnumerator();
                while (it.MoveNext())
                    node.Children.Add(it.Key as string, ConvertToDOMNode(it.Current));
            }
            else if (value is IEnumerable)
            {
                node.Type = Packets.ValueType.ARRAY;
                node.Array = new List<Packets.DOMNode>();
                foreach (var entry in value as IEnumerable)
                    node.Array.Add(ConvertToDOMNode(entry));
            }

            return node;
        }

        internal static string ToHexString(this byte[] buffer, int length = -1)
        {
            if (length == -1)
                length = buffer.Length;

            StringBuilder hex = new StringBuilder(buffer.Length * 2);
            foreach (byte b in buffer)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        internal static bool Contains<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            try
            {
                enumerable.First(predicate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal static TKey GetKeyFor<TKey, TValue>(this Dictionary<TKey, TValue> dic, TValue value, TKey defaultKey)
        {
            foreach (var entry in dic)
                if (entry.Value.Equals(value))
                    return entry.Key;

            return defaultKey;
        }

        internal static Packet Reply(this Packet packet, PacketType responseType)
        {
            var result = new Packet(responseType);
            result.Smagic = packet.Smagic;
            return result;
        }

        internal static bool IsValid(this Packet packet)
        {
            return packet.MsgType != PacketType.EXCEPTION;
        }

        internal static string GetExceptionString(this Packet packet)
        {
            if (packet.PException.Description == null || packet.PException.Description == "")
                return packet.PException.ErrorCode.ToString();
            else
                return packet.PException.ErrorCode.ToString() + "(" + packet.PException.Description + ")";
        }

        internal static DisconnectionDecision Convert(this DisconnectionActionType action)
        {
            switch (action)
            {
                case DisconnectionActionType.KICK:
                    return DisconnectionDecision.Kick;
                case DisconnectionActionType.TERMINATE:
                    return DisconnectionDecision.TerminateSession;
                case DisconnectionActionType.CONTINUE_AND_KICK_ON_TIMEOUT:
                    return DisconnectionDecision.ContinueAndKickOnTimeout;
                case DisconnectionActionType.CONTINUE_AND_TERMINATE_ON_TIMEOUT:
                    return DisconnectionDecision.ContinueAndTerminateOnTimeout;
                case DisconnectionActionType.LOCK_AND_KICK_ON_TIMEOUT:
                    return DisconnectionDecision.LockAndKickOnTimeout;
                case DisconnectionActionType.LOCK_AND_TERMINATE_ON_TIMEOUT:
                    return DisconnectionDecision.LockAndTerminateOnTimeout;
                default:
                    return DisconnectionDecision.Undefined;
            }
        }

        internal static SessionStartCancelReason Convert(this StartCancelReason reason)
        {
            switch (reason)
            {
                case Packets.StartCancelReason.ALL_NOT_READY:
                    return SessionStartCancelReason.AllNotReady;

                case Packets.StartCancelReason.ON_DEMAND:
                    return SessionStartCancelReason.OnDemand;

                case Packets.StartCancelReason.SOMEONE_LEFT:
                    return SessionStartCancelReason.SomeoneLeft;

                default:
                    return SessionStartCancelReason.Unknown;
            }
        }

        internal static TerminationReason Convert(this Packets.TerminationReason reason)
        {
            switch (reason)
            {
                case Packets.TerminationReason.ON_DEMAND:
                    return TerminationReason.TerminateIsCalled;

                case Packets.TerminationReason.DISCONNECTION:
                    return TerminationReason.Disconnected;

                default:
                    return TerminationReason.Unknown;
            }
        }

        internal static bool IsSuccessful(this JsonRpc.Call response)
        {
            if (!response.OK)
                return false;

            return response.Response["result"].ToString() == "ok";
        }

        internal static string GetErrorMessage(this JsonRpc.Call response)
        {
            return response.Exception != null 
                ? response.Exception.Message
                : (string)response.Response["reason"];
        }

        internal static void Fire(this Event<ProblemArgs> ev, string whatHappened, Packets.Exception exceptionPacket)
        {
            ev.Fire(new ProblemArgs(whatHappened, exceptionPacket.ErrorCode, exceptionPacket.Description));
        }

        internal static void Fire(this Event<ProblemArgs> ev, string whatHappened, ExceptionCode errorCode, string errorString = null)
        {
            ev.Fire(new ProblemArgs(whatHappened, errorCode, errorString));
        }
    }
}
