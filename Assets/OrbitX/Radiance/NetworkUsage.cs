﻿using System;
using System.Collections.Generic;

namespace OrbitXSDK
{
    public class NetworkUsage
    {
        private LinkedList<KeyValuePair<int, int>> mRoundTrips = new LinkedList<KeyValuePair<int, int>>();

        private int mSentBytes;
        public int SentBytes
        {
            get { return mSentBytes; }
            internal set { mSentBytes = value; }
        }

        private int mReceivedBytes;
        public int ReceivedBytes
        {
            get { return mReceivedBytes; }
            internal set { mReceivedBytes = value; }
        }

        private int mCaptureInterval;
        public float CaptureInterval
        {
            get { return mCaptureInterval; }
            set { mCaptureInterval = (int)(value * 1000); }
        }

        public int LastPing
        {
            get
            {
                if (mRoundTrips.Count == 0)
                    return -1;

                var entry = mRoundTrips.Last.Value;
                return entry.Value - entry.Key;
            }
        }

        public float LastCheck
        {
            get { return mRoundTrips.Count == 0 ? -1 : (Environment.TickCount - mRoundTrips.Last.Value.Key) * 0.001f; }
        }

        public int Ping
        {
            get
            {
                var now = Environment.TickCount;
                if (mRoundTrips.Count == 0)
                    return -1;

                for (; mRoundTrips.Count >= 5;)
                {
                    var entry = mRoundTrips.First.Value;
                    if (now - entry.Key > mCaptureInterval)
                        mRoundTrips.RemoveFirst();
                    else
                        break;
                }

                if (mRoundTrips.Count == 0)
                    return -1;

                int totalSum = 0;
                foreach (var entry in mRoundTrips)
                    totalSum += entry.Value - entry.Key;
                return totalSum / mRoundTrips.Count;
            }
        }

        internal void RecordRoundtrip(int startTicks, int endTicks)
        {
            mRoundTrips.AddLast(new KeyValuePair<int, int>(startTicks, endTicks));
        }
    }
}
