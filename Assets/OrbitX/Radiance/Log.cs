﻿namespace OrbitXSDK
{
    static public class Log
    {
        static public void d(object log)
        {
            UnityEngine.Debug.Log(log);
        }

        static public void df(string log, params object [] args)
        {
            d(string.Format(log, args));
        }

        static public void e(object log)
        {
            UnityEngine.Debug.LogError(log);
        }

        static public void ef(string log, params object[] args)
        {
            e(string.Format(log, args));
        }
    }
}