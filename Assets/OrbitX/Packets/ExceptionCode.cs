/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */

namespace OrbitXSDK.Packets
{
  public enum ExceptionCode
  {
    LOGIN_TIMEOUT = 1,
    NOT_IDENTIFIED = 2,
    NOT_AUTHORIZED_APP = 3,
    INVALID_PACKET = 4,
    INVALID_SID = 5,
    INVALID_TID = 6,
    INVALID_AID = 7,
    NOT_MANAGER = 8,
    NOT_A_PARTICIPANT = 9,
    NOT_DISCONNECTED = 10,
    SIDE_NOT_AVAILABLE = 11,
    INVALID_SESSION_STATE = 12,
    ALREADY_STATED_READY = 13,
    LOGIN_ERROR = 14,
    WRONG_OTP = 15,
    WRONG_OPTIONS = 16,
    OPERATION_NOT_PERMITED = 17,
    INVALID_RESTRICTIONS = 18,
    UNEXPECTED_EXCEPTION = 19,
        AUTH_OTP_ERROR = 20,
        PLAYER_INFORMATION_UPDATE_FAILED = 21,
        CONNECTION_ALREADY_SETUP = 22,
        OBJECT_NOT_FOUND = 23,
        GO_WITHOUT_SYNC = 24,
        QUICK_REGISTER_ERROR = 25,
    }
}
