/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Thrift;
using Thrift.Collections;
using System.Runtime.Serialization;
using Thrift.Protocol;
using Thrift.Transport;

namespace OrbitXSDK.Packets
{

  #if !SILVERLIGHT
  [Serializable]
  #endif
  public partial class SessionListResponse : TBase
  {

    public Dictionary<int, OrbitXSDK.Packets.SessionConfigs> IDtoConfigs { get; set; }

    public SessionListResponse() {
    }

    public SessionListResponse(Dictionary<int, OrbitXSDK.Packets.SessionConfigs> IDtoConfigs) : this() {
      this.IDtoConfigs = IDtoConfigs;
    }

    public void Read (TProtocol iprot)
    {
      iprot.IncrementRecursionDepth();
      try
      {
        bool isset_IDtoConfigs = false;
        TField field;
        iprot.ReadStructBegin();
        while (true)
        {
          field = iprot.ReadFieldBegin();
          if (field.Type == TType.Stop) { 
            break;
          }
          switch (field.ID)
          {
            case 1:
              if (field.Type == TType.Map) {
                {
                  IDtoConfigs = new Dictionary<int, OrbitXSDK.Packets.SessionConfigs>();
                  TMap _map9 = iprot.ReadMapBegin();
                  for( int _i10 = 0; _i10 < _map9.Count; ++_i10)
                  {
                    int _key11;
                    OrbitXSDK.Packets.SessionConfigs _val12;
                    _key11 = iprot.ReadI32();
                    _val12 = new OrbitXSDK.Packets.SessionConfigs();
                    _val12.Read(iprot);
                    IDtoConfigs[_key11] = _val12;
                  }
                  iprot.ReadMapEnd();
                }
                isset_IDtoConfigs = true;
              } else { 
                TProtocolUtil.Skip(iprot, field.Type);
              }
              break;
            default: 
              TProtocolUtil.Skip(iprot, field.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        iprot.ReadStructEnd();
        if (!isset_IDtoConfigs)
          throw new TProtocolException(TProtocolException.INVALID_DATA);
      }
      finally
      {
        iprot.DecrementRecursionDepth();
      }
    }

    public void Write(TProtocol oprot) {
      oprot.IncrementRecursionDepth();
      try
      {
        TStruct struc = new TStruct("SessionListResponse");
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        field.Name = "IDtoConfigs";
        field.Type = TType.Map;
        field.ID = 1;
        oprot.WriteFieldBegin(field);
        {
          oprot.WriteMapBegin(new TMap(TType.I32, TType.Struct, IDtoConfigs.Count));
          foreach (int _iter13 in IDtoConfigs.Keys)
          {
            oprot.WriteI32(_iter13);
            IDtoConfigs[_iter13].Write(oprot);
          }
          oprot.WriteMapEnd();
        }
        oprot.WriteFieldEnd();
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }
      finally
      {
        oprot.DecrementRecursionDepth();
      }
    }

    public override string ToString() {
      StringBuilder __sb = new StringBuilder("SessionListResponse(");
      __sb.Append(", IDtoConfigs: ");
      __sb.Append(IDtoConfigs);
      __sb.Append(")");
      return __sb.ToString();
    }

  }

}
