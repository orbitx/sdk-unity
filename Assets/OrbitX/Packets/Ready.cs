/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Thrift;
using Thrift.Collections;
using System.Runtime.Serialization;
using Thrift.Protocol;
using Thrift.Transport;

namespace OrbitXSDK.Packets
{

  #if !SILVERLIGHT
  [Serializable]
  #endif
  public partial class Ready : TBase
  {

    public int SessionID { get; set; }

    public int Timeout { get; set; }

    public Ready() {
    }

    public Ready(int sessionID, int timeout) : this() {
      this.SessionID = sessionID;
      this.Timeout = timeout;
    }

    public void Read (TProtocol iprot)
    {
      iprot.IncrementRecursionDepth();
      try
      {
        bool isset_sessionID = false;
        bool isset_timeout = false;
        TField field;
        iprot.ReadStructBegin();
        while (true)
        {
          field = iprot.ReadFieldBegin();
          if (field.Type == TType.Stop) { 
            break;
          }
          switch (field.ID)
          {
            case 1:
              if (field.Type == TType.I32) {
                SessionID = iprot.ReadI32();
                isset_sessionID = true;
              } else { 
                TProtocolUtil.Skip(iprot, field.Type);
              }
              break;
            case 2:
              if (field.Type == TType.I32) {
                Timeout = iprot.ReadI32();
                isset_timeout = true;
              } else { 
                TProtocolUtil.Skip(iprot, field.Type);
              }
              break;
            default: 
              TProtocolUtil.Skip(iprot, field.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        iprot.ReadStructEnd();
        if (!isset_sessionID)
          throw new TProtocolException(TProtocolException.INVALID_DATA);
        if (!isset_timeout)
          throw new TProtocolException(TProtocolException.INVALID_DATA);
      }
      finally
      {
        iprot.DecrementRecursionDepth();
      }
    }

    public void Write(TProtocol oprot) {
      oprot.IncrementRecursionDepth();
      try
      {
        TStruct struc = new TStruct("Ready");
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        field.Name = "sessionID";
        field.Type = TType.I32;
        field.ID = 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(SessionID);
        oprot.WriteFieldEnd();
        field.Name = "timeout";
        field.Type = TType.I32;
        field.ID = 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(Timeout);
        oprot.WriteFieldEnd();
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }
      finally
      {
        oprot.DecrementRecursionDepth();
      }
    }

    public override string ToString() {
      StringBuilder __sb = new StringBuilder("Ready(");
      __sb.Append(", SessionID: ");
      __sb.Append(SessionID);
      __sb.Append(", Timeout: ");
      __sb.Append(Timeout);
      __sb.Append(")");
      return __sb.ToString();
    }

  }

}
