/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */

namespace OrbitXSDK.Packets
{
  public enum StartCancelReason
  {
    ON_DEMAND = 1,
    SOMEONE_LEFT = 2,
    ALL_NOT_READY = 3,
  }
}
