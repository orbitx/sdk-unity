/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Thrift;
using Thrift.Collections;
using System.Runtime.Serialization;
using Thrift.Protocol;
using Thrift.Transport;

namespace OrbitXSDK.Packets
{

  #if !SILVERLIGHT
  [Serializable]
  #endif
  public partial class SideChanged : TBase
  {

    public int SessionID { get; set; }

    public long UID { get; set; }

    public int NewSide { get; set; }

    public SideChanged() {
    }

    public SideChanged(int sessionID, long UID, int newSide) : this() {
      this.SessionID = sessionID;
      this.UID = UID;
      this.NewSide = newSide;
    }

    public void Read (TProtocol iprot)
    {
      iprot.IncrementRecursionDepth();
      try
      {
        bool isset_sessionID = false;
        bool isset_UID = false;
        bool isset_newSide = false;
        TField field;
        iprot.ReadStructBegin();
        while (true)
        {
          field = iprot.ReadFieldBegin();
          if (field.Type == TType.Stop) { 
            break;
          }
          switch (field.ID)
          {
            case 1:
              if (field.Type == TType.I32) {
                SessionID = iprot.ReadI32();
                isset_sessionID = true;
              } else { 
                TProtocolUtil.Skip(iprot, field.Type);
              }
              break;
            case 2:
              if (field.Type == TType.I64) {
                UID = iprot.ReadI64();
                isset_UID = true;
              } else { 
                TProtocolUtil.Skip(iprot, field.Type);
              }
              break;
            case 3:
              if (field.Type == TType.I32) {
                NewSide = iprot.ReadI32();
                isset_newSide = true;
              } else { 
                TProtocolUtil.Skip(iprot, field.Type);
              }
              break;
            default: 
              TProtocolUtil.Skip(iprot, field.Type);
              break;
          }
          iprot.ReadFieldEnd();
        }
        iprot.ReadStructEnd();
        if (!isset_sessionID)
          throw new TProtocolException(TProtocolException.INVALID_DATA);
        if (!isset_UID)
          throw new TProtocolException(TProtocolException.INVALID_DATA);
        if (!isset_newSide)
          throw new TProtocolException(TProtocolException.INVALID_DATA);
      }
      finally
      {
        iprot.DecrementRecursionDepth();
      }
    }

    public void Write(TProtocol oprot) {
      oprot.IncrementRecursionDepth();
      try
      {
        TStruct struc = new TStruct("SideChanged");
        oprot.WriteStructBegin(struc);
        TField field = new TField();
        field.Name = "sessionID";
        field.Type = TType.I32;
        field.ID = 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(SessionID);
        oprot.WriteFieldEnd();
        field.Name = "UID";
        field.Type = TType.I64;
        field.ID = 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteI64(UID);
        oprot.WriteFieldEnd();
        field.Name = "newSide";
        field.Type = TType.I32;
        field.ID = 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32(NewSide);
        oprot.WriteFieldEnd();
        oprot.WriteFieldStop();
        oprot.WriteStructEnd();
      }
      finally
      {
        oprot.DecrementRecursionDepth();
      }
    }

    public override string ToString() {
      StringBuilder __sb = new StringBuilder("SideChanged(");
      __sb.Append(", SessionID: ");
      __sb.Append(SessionID);
      __sb.Append(", UID: ");
      __sb.Append(UID);
      __sb.Append(", NewSide: ");
      __sb.Append(NewSide);
      __sb.Append(")");
      return __sb.ToString();
    }

  }

}
