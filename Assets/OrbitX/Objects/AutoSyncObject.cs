﻿
namespace OrbitXSDK
{
    public class AutoSyncObject : SyncObject
    {
        private AutoSyncObject()
        {
            mIsManual = false;
        }

        private void Awake()
        { }

        private void Start()
        {
            ScanRPCs();
        }

        public bool IsMine()
        {
            return (ID / 10000) % 10000 == (Session.API.Player.UserID % 10000);
        }

        static internal int GetUniqueBaseID(long playerId)
        {
            return (int)((playerId % 10000) * 10000);
        }
    }
}