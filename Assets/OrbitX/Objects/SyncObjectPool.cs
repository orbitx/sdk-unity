﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace OrbitXSDK
{
    public static class SyncObjectPool
    {
        #region Object Pool Data
        private struct ObjectPoolData
        {
            public int AutoIncrement;
            public HashSet<int> RegisteredIDs;
        }

        static private string mPoolDataPath;
        static private string PoolDataPath
        {
            get
            {
                if (mPoolDataPath != null)
                    return mPoolDataPath;

                mPoolDataPath = Path.Combine(Application.dataPath, "OrbitX");
                mPoolDataPath = Path.Combine(mPoolDataPath, "Resources");
                Directory.CreateDirectory(mPoolDataPath);
                mPoolDataPath = Path.Combine(mPoolDataPath, "SyncObjectPool.json");

                if (!File.Exists(mPoolDataPath))
                    SavePoolData(new ObjectPoolData()
                    {
                        RegisteredIDs = new HashSet<int>(),
                        AutoIncrement = 0,
                    });

                return mPoolDataPath;
            }
        }

        static private ObjectPoolData LoadPoolData()
        {
            return JSON.Parse<ObjectPoolData>(File.ReadAllText(PoolDataPath));
        }

        static private void SavePoolData(ObjectPoolData data)
        {
            File.WriteAllText(PoolDataPath, JSON.ToJson(data));
        }
        #endregion Object Pool Data


        #region Manual Sync Objects
        static public class Manual
        {
            static public void RegisterNewObject(SyncObject syncObject)
            {
                var poolData = LoadPoolData();
                var id = syncObject.SerializedObjectID;

                if (id == 0)
                    syncObject.AssignObjectId(id = ++poolData.AutoIncrement);
                else
                    Log.e("[SyncObjectPool] Register called for an object with ID: " + id);

                if (poolData.RegisteredIDs.Contains(id))
                    Log.e("[SyncObjectPool] There is already a sync object with #" + id);

                poolData.RegisteredIDs.Add(id);
                SavePoolData(poolData);
            }

            static public void UnregisterObjectByID(int id)
            {
                var poolData = LoadPoolData();
                if (!poolData.RegisteredIDs.Contains(id))
                    Log.e("[SyncObjectPool] There is no sync object with #" + id);
                poolData.RegisteredIDs.Remove(id);
                SavePoolData(poolData);
            }

            static public bool ChangeID(int lastValue, int newValue)
            {
                var poolData = LoadPoolData();
                if (poolData.RegisteredIDs.Contains(newValue))
                {
                    Log.e("[SyncObjectPool] There is already a sync object with #" + newValue);
                    return false;
                }
                else
                {
                    poolData.RegisteredIDs.Remove(lastValue);
                    poolData.RegisteredIDs.Add(newValue);
                    poolData.AutoIncrement = Mathf.Max(newValue, poolData.AutoIncrement);
                    return true;
                }
            }
        }
        #endregion Manual Sync Objects
    }
}