﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace OrbitXSDK
{
    [ExecuteInEditMode]
    public class SyncObject : MonoBehaviour
    {
        #region Properties
        [HideInInspector]
        public int SerializedObjectID;
        public int ID
        {
            get { return SerializedObjectID; }
        }

        protected bool mIsManual;
        internal bool IsManual

        {
            get { return mIsManual; }
        }

        private Session mSession;
        public Session Session
        {
            get { return mSession; }
            internal set { mSession = value; }
        }

        private void Awake()
        {
            if (SerializedObjectID == 0 && !Application.isPlaying)
                SyncObjectPool.Manual.RegisterNewObject(this);
        }
        #endregion Properties


        #region Scan callable RPCs
        struct RPCExecutionContext
        {
            public object Instance;
            public MethodInfo Method;
            public bool AcceptsCallInfo;
        }

        private Dictionary<string, RPCExecutionContext> mCachedRPCs = new Dictionary<string, RPCExecutionContext>();

        private void Start()
        {
            if (Application.isPlaying)
                ScanRPCs();
        }

        public void ScanRPCs()
        {
            mCachedRPCs.Clear();
            foreach (var script in gameObject.GetComponents<MonoBehaviour>())
                foreach (var cache in AcquireTypeCache(script.GetType()))
                    mCachedRPCs[cache.Method.Name] = new RPCExecutionContext()
                    {
                        Instance = script,
                        Method = cache.Method,
                        AcceptsCallInfo = cache.AcceptsCallInfo
                    };
        }
        #endregion Scan callable RPCs


        #region Type Cache
        static private Dictionary<Type, List<RPCExecutionContext>> mTypeCache = new Dictionary<Type, List<RPCExecutionContext>>();
        static private List<RPCExecutionContext> AcquireTypeCache(Type type)
        {
            if (!mTypeCache.ContainsKey(type))
            {
                var RpcType = typeof(OrbitXRPC);
                var callInfoType = typeof(RPCCallInfo);
                const BindingFlags SearchFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
                var typeCache = new List<RPCExecutionContext>();

                foreach (var method in type.GetMethods(SearchFlags))
                    if (method.GetCustomAttributes(RpcType, true).Length > 0)
                    {
                        var methodParameters = method.GetParameters();
                        typeCache.Add(new RPCExecutionContext()
                        {
                            Method = method,
                            AcceptsCallInfo =
                                methodParameters.Length > 0 &&
                                methodParameters[methodParameters.Length - 1].ParameterType == callInfoType
                        });
                    }

                mTypeCache[type] = typeCache;
            }

            return mTypeCache[type];
        }
        #endregion Type Cache


        #region RPC execution
        private RPCExecutionContext mPooledRPCContext;
        static private RPCCallInfo mPooledRPCCallInfo = new RPCCallInfo();

        internal void ExecuteRPC(string methodName, Packets.RPCCalled details, PlayerPool playerPool)
        {
            if (!mCachedRPCs.TryGetValue(methodName, out mPooledRPCContext))
                return;

            var prms = new object[details.Parameters.Count + (mPooledRPCContext.AcceptsCallInfo ? 1 : 0)];
            foreach (var entry in details.Parameters)
                prms[entry.Key] = entry.Value.ConvertToObject(null);

            if (mPooledRPCContext.AcceptsCallInfo)
            {
                mPooledRPCCallInfo.Caller = playerPool.GetPlayerByUID(details.UID);
                prms[prms.Length - 1] = mPooledRPCCallInfo;
            }

            mPooledRPCContext.Method.Invoke(mPooledRPCContext.Instance, prms);
        }
        #endregion RPC execution


        #region Parenting
        public void AssignObjectId(int id)
        {
            if(SerializedObjectID == 0)
            {
                SerializedObjectID = id;

                if (mSession != null && OrbitX.APIs.Count == 1 && OrbitX.ActiveAPI.Sessions.Length == 1)
                    AssignSession(OrbitX.ActiveAPI.Session);
            }
            else
                Log.e("[SyncObject] Object already has an ID.");
        }

        internal void AssignSession(Session parent)
        {
            mSession = parent;
        }

        private void OnDestroy()
        {
            if (mSession != null)
                mSession.SyncObjectDestroyed(this);
        }
        #endregion Parenting


        #region Aux
        public override string ToString()
        {
            return string.Format("Object #{0}({1})", ID, gameObject.name);
        }

        static internal string EncodeMethodName(SyncObject syncObject, string methodName)
        {
            return syncObject.SerializedObjectID.ToString() + "|" + methodName;
        }

        static internal void DecodeMethodName(string input, out int objectId, out string methodName)
        {
            var x = input.Split('|');
            objectId = int.Parse(x[0]);
            methodName = x[1];
        }
        #endregion Aux
    }
}
