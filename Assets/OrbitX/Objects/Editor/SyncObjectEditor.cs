﻿using UnityEditor;
using UnityEngine;

namespace OrbitXSDK
{
    [CustomEditor(typeof(SyncObject))]
    public class SyncObjectEditor : Editor
    {
        private int mLastValue;
        private bool mEnabledOnPlayMode;
        private SerializedProperty mIDProp;

        private bool IsEditing
        {
            get { return Application.isEditor && !Application.isPlaying; }
        }

        private void OnEnable()
        {
            mIDProp = serializedObject.FindProperty("SerializedObjectID");
            mEnabledOnPlayMode = Application.isPlaying;
        }

        public void OnDestroy()
        {
            if (IsEditing && target == null && !mEnabledOnPlayMode)
                SyncObjectPool.Manual.UnregisterObjectByID(mLastValue);
        }

        private void ChangeID(int newValue)
        {
            if (Application.isPlaying)
                mIDProp.intValue = newValue;
            else if(SyncObjectPool.Manual.ChangeID(mLastValue, newValue))
                mIDProp.intValue = newValue;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Object ID:");
            mLastValue = mIDProp.intValue;
            var newValue = EditorGUILayout.IntField(mLastValue);
            if (newValue != mLastValue)
                ChangeID(newValue);

            EditorGUILayout.EndHorizontal();
            serializedObject.ApplyModifiedProperties();
        }
    }
}