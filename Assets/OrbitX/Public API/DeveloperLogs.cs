﻿namespace OrbitXSDK
{
    internal class DeveloperLogs
    {
        private OrbitXAPI mAPI;

        LogLevel mMinimumLogLevel = LogLevel.Warning;
        internal LogLevel MinimumLogLevel
        {
            set { mMinimumLogLevel = value; }
        }

        internal DeveloperLogs(OrbitXAPI api)
        {
            mAPI = api;
        }

        public void d(object log)
        {
            if (mMinimumLogLevel == LogLevel.Debug)
                mAPI.Events.OnLog.Fire(new LogArgs(LogLevel.Debug, log.ToString()));
        }

        public void df(string log, params object[] args)
        {
            if (mMinimumLogLevel == LogLevel.Debug)
                d(string.Format(log, args));
        }

        public void w(object log)
        {
            if (mMinimumLogLevel <= LogLevel.Warning)
                mAPI.Events.OnLog.Fire(new LogArgs(LogLevel.Warning, log.ToString()));
        }

        public void wf(string log, params object[] args)
        {
            if (mMinimumLogLevel <= LogLevel.Warning)
                w(string.Format(log, args));
        }

        public void e(object log)
        {
            if (mMinimumLogLevel != LogLevel.None)
                mAPI.Events.OnLog.Fire(new LogArgs(LogLevel.Error, log.ToString()));
        }

        public void ef(string log, params object[] args)
        {
            if (mMinimumLogLevel != LogLevel.None)
                e(string.Format(log, args));
        }
    }
}
