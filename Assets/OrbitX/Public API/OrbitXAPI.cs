﻿using OrbitXSDK;

internal class OrbitXAPI
{
    #region Startup
    internal OrbitXAPI(int apiNumber, string dataFixture)
    {
        mAPINumber = apiNumber;

        mLog = new DeveloperLogs(this);
        UnityBroker.Instance.Start();

        mEvents = new DeveloperEvents();
        MonoBehaviourEventQueue.RegisterAPI(this);

        mPlayerPool = new PlayerPool(this);
        mAuthClient = new AuthClient(this);
        mGameClient = new GameClient(this);
        mLocalData = new LocalData(mAPINumber, dataFixture);
    }
    #endregion Startup


    #region System and Player Information
    private int mAPINumber;

    private PlayerPool mPlayerPool;
    internal PlayerPool PlayerPool
    {
        get { return mPlayerPool; }
    }

    private AuthClient mAuthClient;
    internal AuthClient AuthClient
    {
        get { return mAuthClient; }
    }

    private GameClient mGameClient;
    internal GameClient GameClient
    {
        get { return mGameClient; }
    }

    private LocalData mLocalData;
    internal LocalData LocalData
    {
        get { return mLocalData; }
    }

    internal Player Player
    {
        get { return AuthClient.Self; }
    }

    private bool mOnlyOneSessionAllowed;
    internal bool OnlyOneSessionAllowed
    {
        get { return mOnlyOneSessionAllowed; }
    }

    internal void SetOnlyOneSessionAllowed(bool value)
    {
        mOnlyOneSessionAllowed = value;
    }

    internal void ChangePlayerDetail(PlayerDetail detailType, object newValue)
    {
        var prevValue = Player.GetDetail(detailType);
        AuthClient.UpdatePlayerDetail(detailType, newValue);
        AuthClient.SyncPlayerDetails(success =>
        {
            if (!success)
                AuthClient.UpdatePlayerDetail(detailType, prevValue);
        });
    }

    private DeveloperEvents mEvents;
    internal DeveloperEvents Events
    {
        get { return mEvents; }
    }

    private DeveloperLogs mLog;
    internal DeveloperLogs Log
    {
        get { return mLog; }
    }
    #endregion System and Player Information

    
    #region Network
    private NetworkUsage mNetworkUsage = new NetworkUsage();
    internal NetworkUsage NetworkUsage
    {
        get { return mNetworkUsage; }
    }
    #endregion Network


    #region Connection
    private string mServerAddress = "orbitx.com";
    public string ServerAddress
    {
        get { return mServerAddress; }
        set
        {
            if (ConnectionState != ConnectionState.Disconnected)
                Log.e("[OrbitX] Can not change server address once connected.");
            else
                mServerAddress = value;
        }
    }

    private int mServerPort = 14442;
    public int ServerPort
    {
        get { return mServerPort; }
        set
        {
            if (ConnectionState != ConnectionState.Disconnected)
                Log.e("[OrbitX] Can not change server port once connected.");
            else
                mServerPort = value;
        }
    }

    private string mApplicationToken;
    public string ApplicationToken
    {
        get { return mApplicationToken; }
        set
        {
            if (ConnectionState != ConnectionState.Disconnected)
                Log.e("[OrbitX] Can not change app ID once connected.");
            else
                mApplicationToken = value;
        }
    }

    internal ConnectionState ConnectionState
    {
        get { return mGameClient.ConnectionState; }
    }

    internal void Connect()
    {
        mGameClient.SetupConnection();
    }

    internal void Disconnect()
    {
        mGameClient.StopPersistentConnection(true);
    }
    #endregion Connection


    #region Session Join/Create/List/Leave/Start
    internal Session [] Sessions
    {
        get { return mGameClient.Sessions; }
    }

    internal Session Session
    {
        get
        {
            var sessions = mGameClient.Sessions;
            return sessions.Length > 0 ? sessions[0] : null;
        }
    }

    internal void SimpleMatchmake(MatchmakeOptions options, object tag = null)
    {
        mGameClient.RequestAutoSession(options, -1, 0.5, 1, tag);
    }

    internal void Matchmake(MatchmakeOptions options, double playerScore, double acceptableRange, int poolingTime, object tag = null)
    {
        mGameClient.RequestAutoSession(options, playerScore, acceptableRange, poolingTime, tag);
    }

    internal void CancelMatchmake(object tag)
    {
        mGameClient.CancelAutoSessionRequest(tag);
    }

    internal void CreateSession(MatchmakeOptions options, string label, int startingSide = 0, object tag = null)
    {
        CreateSession(options, label, "", startingSide, tag);
    }

    internal void CreateSession(MatchmakeOptions options, string label, string password, int startingSide = 0, object tag = null)
    {
        mGameClient.RequestCreateSession(options, label, password, startingSide, tag);
    }

    internal void JoinSessionByID(int sessionId, int side = 0, string password = null, object tag = null)
    {
        mGameClient.JoinSessionByID(sessionId, password, side, tag);
    }

    internal void GetSessionList(SessionListCallback callback)
    {
        GetSessionList(OrbitX.DefineMatchOptions(), callback);
    }

    internal void GetSessionList(MatchmakeOptions options, SessionListCallback callback)
    {
        mGameClient.GetSessionList(options, callback);
    }
    #endregion Session Join/Create


    #region Auxilary Utilities
    internal void SetMinimumLogLevel(LogLevel logLevel)
    {
        Log.MinimumLogLevel = logLevel;
    }

    internal void ClearLocalData()
    {
        mLocalData.Clear();
    }

    internal void TearDown()
    {
        mLocalData.Save(false);
        Disconnect();
    }

    public override string ToString()
    {
        return "API #" + mAPINumber;
    }
    #endregion Auxilary Utilities
}
