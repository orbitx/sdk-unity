﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrbitXSDK
{
    public enum LogLevel
    {
        Debug = 0,
        Warning = 1,
        Error = 2,
        None = 3,
    }

    public enum ConnectionState
    {
        Connecting,
        Connected,
        Disconnected,
        Reconnected,
    }

    public enum ConnectionStateDetail
    {
        NoDetail,
        AuthClientNotInitialized,
        ConnectionFailure,
        ServerDisconnected,
        AuthClientOTPFailure,
        DisconnectionRequest,
    }

    public enum SessionState
    {
        Joined,
        StartCall,
        Running,
        Locked,
    }

    public enum Calls
    {
        Post,
        Get,
    }

    public enum ExecutionMode
    {
        InUnity,
        Async,
    }

    public enum PlayerUpdateEvent
    {
        Joined,
        Left,
        ChangedTeam,
    }

    public enum DisconnectionDecision
    {
        Kick,
        TerminateSession,
        ContinueAndKickOnTimeout,
        ContinueAndTerminateOnTimeout,
        LockAndKickOnTimeout,
        LockAndTerminateOnTimeout,
        Undefined,
    }

    public enum ActiveSessionDecision
    {
        Discard,
        Rejoin,
    }

    public enum SessionLeaveReason
    {
        Voluntarily,
        Kicked,
        StartCallTimedOut,
        Temporary,
        Disconnected,
        Terminated,
    }

    public enum TerminationReason
    {
        TerminateIsCalled,
        Disconnected,
        Unknown,
    }

    public enum SessionStartCancelReason
    {
        OnDemand,
        SomeoneLeft,
        AllNotReady,
        Unknown,
    }

    [Flags]
    public enum RPCFlags
    {
        Normal = 0x0,
        ForMeToo = 0x1,
        Persistent = 0x2,
    };

    public class MatchmakeOptions
    {
        internal MatchmakeOptions()
        {
            mManagerOptions = new ManagerMatchmakeOptions(this);
        }

        private Packets.SessionConfigs mBuiltConfigs = null;
        internal void CheckNotBuildYet()
        {
            if (mBuiltConfigs != null)
                throw new ArgumentException("[MatchmakeOptions] Modification not allowed after a session is built.");
        }

        internal Packets.SessionConfigs Build()
        {
            CheckNotBuildYet();
            mBuiltConfigs = new Packets.SessionConfigs();
            mBuiltConfigs.SideToCapacity = mSideCaps;
            mBuiltConfigs.AlwaysAcceptJoin = mManagerOptions.AllowJoinWithinGame;
            mBuiltConfigs.CanChangeSideAfterStart = mManagerOptions.AllowSideChangeWithinGame;
            mBuiltConfigs.DisconnectionBehavior = mManagerOptions.DisconnectionDecision.Serialize();
            mBuiltConfigs.MinPlayerToMatch = mMinimumPlayers;
            mBuiltConfigs.Options = mConditions;
            return mBuiltConfigs;
        }

        static internal MatchmakeOptions FromSessionConfigs(Packets.SessionConfigs configs)
        {
            var result = new MatchmakeOptions();
            result.Manager.AllowSideChangeWithinGame = configs.CanChangeSideAfterStart;
            result.Manager.DisconnectionDecision = configs.DisconnectionBehavior.Convert();
            result.Manager.KickTimoutOnDisconnection = configs.DisconnectionTimeout;
            result.MinimumPlayers = configs.MinPlayerToMatch;
            result.mConditions = new Dictionary<string, string>(configs.Options);
            return result;
        }

        private Dictionary<int, int> mSideCaps = new Dictionary<int, int>();
        public void DefineSide(int sideNumber, int sideCapacity)
        {
            CheckNotBuildYet();

            if (mSideCaps.ContainsKey(sideNumber))
                throw new ArgumentException("[MatchmakeOptions] There is already a side with number " + sideNumber);

            mSideCaps[sideNumber] = sideCapacity;
        }

        public int [] Sides
        {
            get { return mSideCaps.Keys.ToArray(); }
        }

        public Dictionary<int,  int> SideCaps
        {
            get { return new Dictionary<int, int>(mSideCaps); }
        }

        private int mMinimumPlayers = 2;
        public int MinimumPlayers
        {
            get { return mMinimumPlayers; }
            set
            {
                CheckNotBuildYet();

                if (value <= 0)
                    throw new ArgumentException("[MatchmakeOptions] Value " + value + " is not valid for minimum players.");

                mMinimumPlayers = value;
            }
        }

        private Dictionary<string, string> mConditions = new Dictionary<string, string>();
        public void DefineCondition(string conditionName, string value)
        {
            CheckNotBuildYet();

            if (mConditions.ContainsKey(conditionName))
                throw new ArgumentException("[MatchmakeOptions] There is already a condition for " + conditionName);

            mConditions[conditionName] = value;
        }

        public void DefineCondition(string conditionName, int value)
        {
            DefineCondition(conditionName, value.ToString());
        }

        private ManagerMatchmakeOptions mManagerOptions;
        public ManagerMatchmakeOptions Manager
        {
            get { return mManagerOptions; }
        }

        private int mPreferedSide = -1;
        public int PreferedSide
        {
            get { return mPreferedSide; }
            set
            {
                CheckNotBuildYet();
                if (!mSideCaps.ContainsKey(value))
                    throw new ArgumentException("[MatchmakeOptions] Can not favor a side which is not defined: " + value);

                mPreferedSide = value;
            }
        }

        private Action mOnTrackIdReceived = null;
        internal Action OnTrackIdReceived
        {
            get { return mOnTrackIdReceived; }
            set { mOnTrackIdReceived = value; }
        }
    }

    public class ManagerMatchmakeOptions
    {
        private MatchmakeOptions mMatchOptions;
        internal ManagerMatchmakeOptions(MatchmakeOptions matchOptions)
        {
            mMatchOptions = matchOptions;
        }

        private bool mAllowJoinWithinGame;
        public bool AllowJoinWithinGame
        {
            get { return mAllowJoinWithinGame; }
            set
            {
                mMatchOptions.CheckNotBuildYet();
                mAllowJoinWithinGame = value;
            }
        }

        private bool mAllowSideChangeWithinGame;
        public bool AllowSideChangeWithinGame
        {
            get { return mAllowSideChangeWithinGame; }
            set
            {
                mMatchOptions.CheckNotBuildYet();
                mAllowSideChangeWithinGame = value;
            }
        }

        private int mKickTimoutOnDisconnection;
        public int KickTimoutOnDisconnection
        {
            get { return mKickTimoutOnDisconnection; }
            set
            {
                mMatchOptions.CheckNotBuildYet();
                mKickTimoutOnDisconnection = value;
            }
        }

        private DisconnectionDecision mDisconnectionDecision = DisconnectionDecision.Undefined;
        public DisconnectionDecision DisconnectionDecision
        {
            get { return mDisconnectionDecision; }
            set
            {
                mMatchOptions.CheckNotBuildYet();
                mDisconnectionDecision = value;
            }
        }
    }

    public class SessionConfiguration
    {
        public Dictionary<int, int> SideCapacities;
        public bool AllowJoinWithinGame;
        public bool AllowSideChangeWithinGame;
        public DisconnectionDecision DisconnectionDecision;
        public int KickTimeoutOnDisconnection;
        public Dictionary<string, string> Conditions;
        public string Label;
        public string Password;

        internal SessionConfiguration(Packets.SessionConfigs original)
        {
            SideCapacities = new Dictionary<int, int>(original.SideToCapacity);
            AllowJoinWithinGame = original.AlwaysAcceptJoin;
            AllowSideChangeWithinGame = original.CanChangeSideAfterStart;
            DisconnectionDecision = original.DisconnectionBehavior.Convert();
            KickTimeoutOnDisconnection = original.DisconnectionTimeout;
            Conditions = new Dictionary<string, string>(original.Options);
            Label = original.Label;
            Password = original.Password;
        }

        internal Packets.SessionConfigs Build()
        {
            var result = new Packets.SessionConfigs();
            result.SideToCapacity = SideCapacities;
            result.AlwaysAcceptJoin = AllowJoinWithinGame;
            result.CanChangeSideAfterStart = AllowSideChangeWithinGame;
            result.DisconnectionBehavior = DisconnectionDecision.Serialize();
            result.Options = Conditions;
            result.Label = Label;
            result.Password = Password;
            return result;
        }
    }

    public class SessionListingInfo
    {
        private OrbitXAPI mAPI;
        private Packets.SessionConfigs mData;

        private int mSessionID;
        public int SessionID
        {
            get { return mSessionID; }
        }

        public string Label
        {
            get { return mData.Label; }
        }

        public int GetSideCap(int sideId)
        {
            return mData.SideToCapacity[sideId];
        }

        public int [] SideIDs
        {
            get { return mData.SideToCapacity.Keys.ToArray(); }
        }

        public Player [] GetPlayersInSide(int sideId)
        {
            var playerIds = mData.ParticipantsToSide
                .Where(entry => entry.Value == sideId)
                .Select(entry => entry.Key);

            return mAPI.PlayerPool.GetPlayersByUID(playerIds).ToArray();
        }

        public Player [] GetPlayers()
        {
            return mAPI.PlayerPool.GetPlayersByUID(mData.ParticipantsToSide.Keys).ToArray();
        }

        public bool HasPassword
        {
            get { return !string.IsNullOrEmpty(mData.Password); }
        }

        internal SessionListingInfo(int sessionId, Packets.SessionConfigs sessionData, OrbitXAPI api)
        {
            mAPI = api;
            mData = sessionData;
            mSessionID = sessionId;
        }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendFormat("{0}: '{2}{1}'", mSessionID, Label, HasPassword ? "*" : "");
            foreach (var sideId in mData.SideToCapacity.Keys)
                result.AppendFormat(" [{0}, {1}]", GetSideCap(sideId), GetPlayersInSide(sideId).Length);
            result.AppendFormat(" [-, {0}]", GetPlayersInSide(0).Length);
            return result.ToString();
        }
    }

    public delegate void SessionListCallback(SessionListingInfo[] sessions);

    internal struct SessionStartCallParams
    {
        public int Timeout;
        public bool PlayersCanCancel;
        public bool AllShouldAcknowledge;
    }

    internal static class DataStrucuteExtensions
    {
        static internal Packets.DisconnectionActionType Serialize(this DisconnectionDecision decision)
        {
            return (Packets.DisconnectionActionType)((int)decision);
        }
    }
}
