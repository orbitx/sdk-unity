﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace OrbitXSDK
{
    static internal class MonoBehaviourEventQueue
    {
        static private List<MonoBehavior> mFloatingClients;
        static private Dictionary<OrbitXAPI, List<MonoBehavior>> mGroups;
        static MonoBehaviourEventQueue()
        {
            mGroups = new Dictionary<OrbitXAPI, List<MonoBehavior>>();
            mFloatingClients = new List<MonoBehavior>();
        }

        static internal void RegisterClient(MonoBehavior client)
        {
            if (mGroups.Count > 0)
                mGroups.First().Value.Add(client);
            else
                mFloatingClients.Add(client);
        }

        static internal void RegisterAPI(OrbitXAPI api)
        {
            mGroups[api] = new List<MonoBehavior>();
            if(mGroups.Count == 1)
            {
                mGroups.First().Value.AddRange(mFloatingClients);
                mFloatingClients.Clear();
            }

            api.Events.OnConnectionStateChanged += (args) => Call(api, client => client.OnConnectionStateChanged(args));
            api.Events.OnLeftSession += (args) => Call(api, client => client.OnLeftSession(args));
            api.Events.OnSessionStateChanged += (args) => Call(api, client => client.OnSessionStateChanged(args));
            api.Events.OnSessionConfigurationChanged += (args) => Call(api, client => client.OnSessionConfigurationChanged(args));
            api.Events.OnSessionReadyToStart += (args) => Call(api, client => client.OnSessionReadyToStart(args));
            api.Events.OnPlayerListUpdated += (args) => Call(api, client => client.OnPlayerListUpdated(args));
            api.Events.OnSessionPropertyChanged += (args) => Call(api, client => client.OnSessionPropertyChanged(args));
            api.Events.OnActiveSessionFound += (args) => Call(api, client => client.OnActiveSessionFound(args));
            api.Events.OnMadeManager += (args) => Call(api, client => client.OnMadeManager(args));
            api.Events.OnMyPlayerAccountUpdated += (args) => Call(api, client => client.OnMyPlayerAccountUpdated(args));
        }

        private static void Call(OrbitXAPI api, Action<MonoBehavior> action)
        {
            var clientList = mGroups[api];
            for (int i = 0; i < clientList.Count; ++i)
            {
                var client = clientList[i];
                try
                {
                    if (client.isActiveAndEnabled)
                        action(client);
                }
                catch
                {
                    clientList.RemoveAt(i--);
                }
            }
        }
    }

    public class MonoBehavior : UnityEngine.MonoBehaviour
    {
        private AutoSyncObject mSyncObject;
        public AutoSyncObject SyncObject
        {
            get
            {
                if (mSyncObject == null)
                    mSyncObject = GetComponent<AutoSyncObject>();

                return mSyncObject;
            }
        }

        public bool IsMine
        {
            get { return SyncObject != null ? SyncObject.IsMine() : true; }
        }

        protected MonoBehavior()
        {
            MonoBehaviourEventQueue.RegisterClient(this);
        }

        virtual public void OnConnectionStateChanged(ConnectionStateChangedArgs eventArgs)
        { }

        virtual public void OnLeftSession(LeftSessionArgs eventArgs)
        { }

        virtual public void OnSessionStateChanged(SessionStateChangedArgs eventArgs)
        { }

        virtual public void OnSessionConfigurationChanged(SessionConfigurationChangedArgs eventArgs)
        { }

        virtual public void OnSessionReadyToStart(SessionReadyToStartArgs eventArgs)
        { }

        virtual public void OnPlayerListUpdated(PlayerListUpdatedArgs eventArgs)
        { }

        virtual public void OnSessionPropertyChanged(SessionPropertyChangedArgs eventArgs)
        { }

        virtual public void OnActiveSessionFound(ActiveSessionFoundArgs eventArgs)
        { }

        virtual public void OnMadeManager(MadeManagerArgs eventArgs)
        { }

        virtual public void OnMyPlayerAccountUpdated(MyPlayerAccountUpdatedArgs eventArgs)
        { }
    }
}
