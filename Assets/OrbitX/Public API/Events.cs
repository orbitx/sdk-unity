﻿using System.Collections.Generic;
using System;

namespace OrbitXSDK
{
    public class Event<T>
    {
        private event Action<T> mEvent;

        public void TearDown()
        {
            mEvent = null;
        }

        internal void Fire(T args)
        {
            UnityBroker.PostToUnity(() =>
            {
                var ev = mEvent;
                if (ev != null)
                    ev(args);
            });
        }

        static public Event<T> operator +(Event<T> ev, Action<T> subscriber)
        {
            ev.mEvent += subscriber;
            return ev;
        }
    }

    public class DeveloperEvents
    {
        public Event<ProblemArgs> OnProblem = new Event<ProblemArgs>();
        public Event<ConnectionStateChangedArgs> OnConnectionStateChanged = new Event<ConnectionStateChangedArgs>();
        public Event<MatchmakeFailedArgs> OnMatchmakeFailed = new Event<MatchmakeFailedArgs>();
        public Event<MatchmakeDoneArgs> OnMatchmakeDone = new Event<MatchmakeDoneArgs>();
        public Event<LeftSessionArgs> OnLeftSession = new Event<LeftSessionArgs>();
        public Event<SessionStateChangedArgs> OnSessionStateChanged = new Event<SessionStateChangedArgs>();
        public Event<SessionConfigurationChangedArgs> OnSessionConfigurationChanged = new Event<SessionConfigurationChangedArgs>();
        public Event<SessionReadyToStartArgs> OnSessionReadyToStart = new Event<SessionReadyToStartArgs>();
        public Event<SessionStartedArgs> OnSessionStarted = new Event<SessionStartedArgs>();
        public Event<SessionStartCancelledArgs> OnSessionStartCancelled = new Event<SessionStartCancelledArgs>();
        public Event<PlayerListUpdatedArgs> OnPlayerListUpdated = new Event<PlayerListUpdatedArgs>();
        public Event<SessionPropertyChangedArgs> OnSessionPropertyChanged = new Event<SessionPropertyChangedArgs>();
        public Event<ActiveSessionFoundArgs> OnActiveSessionFound = new Event<ActiveSessionFoundArgs>();
        public Event<MadeManagerArgs> OnMadeManager = new Event<MadeManagerArgs>();
        public Event<LogArgs> OnLog = new Event<LogArgs>();
        public Event<MyPlayerAccountUpdatedArgs> OnMyPlayerAccountUpdated = new Event<MyPlayerAccountUpdatedArgs>();
    }

    public class ProblemArgs
    {
        private string mWhatHappened;
        public string WhatHappened
        {
            get { return mWhatHappened; }
        }

        private Packets.ExceptionCode mErrorCode;
        public int ErrorCode
        {
            get { return (int)mErrorCode; }
        }

        private string mErrorString;
        public string ErrorString
        {
            get { return mErrorString; }
        }

        internal ProblemArgs(string whatHappened, Packets.ExceptionCode errorCode, string errorString)
        {
            mWhatHappened = whatHappened;
            mErrorCode = errorCode;
            mErrorString = errorString;
        }

        public override string ToString()
        {
            return string.Format(
                "{0} {1}({2}){3}", 
                string.IsNullOrEmpty(mWhatHappened) ? "[Exception]" : mWhatHappened,
                mErrorCode,
                ErrorCode,
                string.IsNullOrEmpty(ErrorString) ? "" : " with message " + ErrorString
            );
        }
    }

    public class ConnectionStateChangedArgs
    {
        private ConnectionState mState;
        public ConnectionState State
        {
            get { return mState; }
        }

        private ConnectionStateDetail mDetail;
        public ConnectionStateDetail Detail
        {
            get { return mDetail; }
        }

        internal ConnectionStateChangedArgs(ConnectionState state, ConnectionStateDetail detail)
        {
            mState = state;
            mDetail = detail;
        }
    }

    public class SessionEventArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        internal SessionEventArgs(Session session)
        {
            mSession = session;
        }
    }

    public class SessionStateChangedArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        private SessionState mPreviousState;
        public SessionState PreviousState
        {
            get { return mPreviousState; }
        }

        public SessionState NewState
        {
            get { return mSession.State; }
        }

        internal SessionStateChangedArgs(Session session, SessionState prevState)
        {
            mSession = session;
            mPreviousState = prevState;
        }
    }

    public class SessionReadyToStartArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        private int mTimeout;
        public int Timeout
        {
            get { return mTimeout; }
        }

        internal SessionReadyToStartArgs(Session session, int timeout)
        {
            mSession = session;
            mTimeout = timeout;
        }
    }

    public class SessionStartedArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        internal SessionStartedArgs(Session session)
        {
            mSession = session;
        }
    }

    public class SessionStartCancelledArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        private SessionStartCancelReason mReason;
        public SessionStartCancelReason Reason
        {
            get { return mReason; }
        }

        private Player[] mCulprits;
        public Player[] Culprits
        {
            get { return mCulprits; }
        }

        internal SessionStartCancelledArgs(Session session, SessionStartCancelReason reason, Player [] culprits)
        {
            mSession = session;
            mReason = reason;
            mCulprits = culprits;
        }
    }

    public class PlayerListUpdatedArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        private Dictionary<Player, PlayerUpdateEvent> mUpdatedPlayers;
        public Dictionary<Player, PlayerUpdateEvent> UpdatedPlayers
        {
            get { return mUpdatedPlayers; }
        }

        internal PlayerListUpdatedArgs(Session session, Dictionary<Player, PlayerUpdateEvent> updatePlayers)
        {
            mSession = session;
            mUpdatedPlayers = updatePlayers;
        }
    }

    public class SessionPropertyChangedArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        private ModifiedEntry[] mPropertyChanges;
        public ModifiedEntry[] PropertyChanges
        {
            get { return mPropertyChanges; }
        }

        internal SessionPropertyChangedArgs(Session session, ModifiedEntry[] propertyChanges)
        {
            mSession = session;
            mPropertyChanges = propertyChanges;
        }
    }

    public class HandlePlayerDisconnectionArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        private Player mPlayer;
        public Player Player
        {
            get { return mPlayer; }
        }

        private DisconnectionDecision mDecision;
        public DisconnectionDecision Decision
        {
            get { return mDecision; }
            set { mDecision = value; }
        }

        private int mTimeout;
        public int Timeout
        {
            get { return mTimeout; }
            set { mTimeout = value; }
        }

        internal HandlePlayerDisconnectionArgs(Session session, Player player)
        {
            mSession = session;
            mPlayer = player;
            mDecision = DisconnectionDecision.Undefined;
            mTimeout = -1;
        }
    }

    public class MadeManagerArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        internal MadeManagerArgs(Session session)
        {
            mSession = session;
        }
    }

    public class ActiveSessionFoundArgs
    {
        private GameClient mGameClient;

        private SessionListingInfo [] mSessions;
        public SessionListingInfo [] Sessions
        {
            get { return mSessions; }
        }

        internal ActiveSessionFoundArgs(SessionListingInfo [] sessions, GameClient gameClient)
        {
            mGameClient = gameClient;
            mSessions = sessions;
        }

        public void Decide(int sessionId, ActiveSessionDecision decision, object tag = null)
        {
            mGameClient.DeveloperDecidedOnActiveSession(sessionId, decision, tag);
        }
    }

    public class LogArgs
    {
        private LogLevel mLogLevel;
        public LogLevel LogLevel
        {
            get { return mLogLevel; }
        }

        private string mMessage;
        public string Message
        {
            get { return mMessage; }
        }

        internal LogArgs(LogLevel logLevel, string message)
        {
            mLogLevel = logLevel;
            mMessage = message;
        }
    }

    public class MyPlayerAccountUpdatedArgs
    {
        private Player mPlayer;
        public Player Player
        {
            get { return mPlayer; }
        }

        private AccountEvent mEventType;
        public AccountEvent EventType
        {
            get { return mEventType; }
        }

        internal MyPlayerAccountUpdatedArgs(Player player, AccountEvent eventType)
        {
            mPlayer = player;
            mEventType = eventType;
        }
    }

    public class MatchmakeFailedArgs
    {
        private object mTag;
        public object Tag
        {
            get { return mTag; }
        }

        private Packets.ExceptionCode mErrorCode;
        public int ErrorCode
        {
            get { return (int)mErrorCode; }
        }

        private string mErrorString;
        public string ErrorString
        {
            get { return mErrorString; }
        }

        internal MatchmakeFailedArgs(object tag, Packets.Exception exceptionDetails)
        {
            mTag = tag;
            mErrorCode = exceptionDetails.ErrorCode;
            mErrorString = exceptionDetails.Description;
        }

        public override string ToString()
        {
            return string.Format(
                "[Matchmake] Failure to make a match with error {0}({1}) {2}",
                mErrorCode,
                (int)mErrorCode,
                mErrorString
            );
        }
    }

    public class MatchmakeDoneArgs
    {
        private object mTag;
        public object Tag
        {
            get { return mTag; }
        }

        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        internal MatchmakeDoneArgs(Session session, object tag)
        {
            mSession = session;
            mTag = tag;
        }
    }

    public class LeftSessionArgs
    {
        private SessionLeaveReason mReason;
        public SessionLeaveReason Reason
        {
            get { return mReason; }
        }

        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        internal LeftSessionArgs(Session session, SessionLeaveReason reason)
        {
            mReason = reason;
            mSession = session;
        }
    }

    public class SessionConfigurationChangedArgs
    {
        private Session mSession;
        public Session Session
        {
            get { return mSession; }
        }

        public MatchmakeOptions Options
        {
            get { return MatchmakeOptions.FromSessionConfigs(mSession.ConfigData); }
        }

        internal SessionConfigurationChangedArgs(Session session)
        {
            mSession = session;
        }
    }
}
