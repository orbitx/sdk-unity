﻿using OrbitXSDK;
using System.Collections.Generic;

static public class OrbitX
{
    #region Startup
    static OrbitX()
    {
        mAPIs.Add(new OrbitXAPI(1, null));
    }
    #endregion Startup


    #region System and Player Information
    static private List<OrbitXAPI> mAPIs = new List<OrbitXAPI>();
    static internal OrbitXAPI ActiveAPI
    {
        get { return mAPIs[0]; }
    }

    static internal List<OrbitXAPI> APIs
    {
        get { return mAPIs; }
    }

    static public DeveloperEvents Events
    {
        get { return ActiveAPI.Events; }
    }

    static public bool OnlyOneSessionAllowed
    {
        get { return ActiveAPI.OnlyOneSessionAllowed; }
    }

    static public void SetOnlyOneSessionAllowed(bool value)
    {
        ActiveAPI.SetOnlyOneSessionAllowed(value);
    }

    static public Player Me
    {
        get { return ActiveAPI.Player; }
    }

    static public void ChangePlayerDetail(PlayerDetail detailType, object newValue)
    {
        ActiveAPI.ChangePlayerDetail(detailType, newValue);
    }
    #endregion System and Player Information


    #region Network
    static private NetworkUsage mNetworkUsage = new NetworkUsage();
    static public NetworkUsage NetworkUsage
    {
        get { return mNetworkUsage; }
    }
    #endregion Network


    #region Connection
    static public ConnectionState ConnectionState
    {
        get { return ActiveAPI.ConnectionState; }
    }

    static public void Connect()
    {
        ActiveAPI.Connect();
    }

    static public void Connect(string ip, int port, string appToken)
    {
        ActiveAPI.ServerAddress = ip;
        ActiveAPI.ServerPort = port;
        ActiveAPI.ApplicationToken = appToken;
        ActiveAPI.Connect();
    }

    static public void Disconnect()
    {
        ActiveAPI.Disconnect();
    }
    #endregion Connection


    #region Session Join/Create/List/Leave/Start
    static public MatchmakeOptions DefineMatchOptions()
    {
        return new MatchmakeOptions();
    }

    static public Session [] Sessions
    {
        get { return ActiveAPI.Sessions; }
    }

    static public Session Session
    {
        get { return ActiveAPI.Session; }
    }

    static public void SimpleMatchmake(MatchmakeOptions options, object tag = null)
    {
        ActiveAPI.SimpleMatchmake(options, tag);
    }

    static public void Matchmake(MatchmakeOptions options, double playerScore, double acceptableRange, int poolingTime, object tag = null)
    {
        ActiveAPI.Matchmake(options, playerScore, acceptableRange, poolingTime, tag);
    }

    static public void CancelMatchmake(object tag)
    {
        ActiveAPI.CancelMatchmake(tag);
    }

    static public void CreateSession(MatchmakeOptions options, string label, int startingSide = 0, object tag = null)
    {
        ActiveAPI.CreateSession(options, label, startingSide, tag);
    }

    static public void CreateSession(MatchmakeOptions options, string label, string password, int startingSide = 0, object tag = null)
    {
        ActiveAPI.CreateSession(options, label, password, startingSide, tag);
    }

    static public void JoinSessionByID(int sessionId, int side = 0, string password = null, object tag = null)
    {
        ActiveAPI.JoinSessionByID(sessionId, side, password, tag);
    }

    static public void GetSessionList(SessionListCallback callback)
    {
        ActiveAPI.GetSessionList(callback);
    }

    static public void GetSessionList(MatchmakeOptions options, SessionListCallback callback)
    {
        ActiveAPI.GetSessionList(options, callback);
    }
    #endregion Session Join/Create


    #region Auxilary Utilities
    static internal void SaveAllLocalData()
    {
        foreach (var api in mAPIs)
            api.LocalData.Save(true);
    }

    static internal void StopAllConnections(bool manualStop)
    {
        foreach (var api in mAPIs)
            api.GameClient.StopPersistentConnection(manualStop);
    }

    static internal void ResumeAllConnections()
    {
        foreach (var api in mAPIs)
            api.GameClient.ResumePersistentConnection();
    }

    static public void SetMinimumLogLevel(LogLevel logLevel)
    {
        ActiveAPI.SetMinimumLogLevel(logLevel);
    }

    static public void ClearLocalData()
    {
        ActiveAPI.ClearLocalData();
    }
    #endregion Auxilary Utilities
}
