﻿using OrbitXSDK.Packets;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace OrbitXSDK
{
    public partial class Session
    {
        #region Data and Properties
        private const string SDK = "#";
        private const string USER = "$";

        private GameObject mGameObject;

        private int mAutoObjectID;
        private SessionMagics mSessionMagic;

        private OrbitXAPI mAPI;
        internal OrbitXAPI API
        {
            get { return mAPI; }
        }

        private SessionConfigs mData;
        internal SessionConfigs ConfigData
        {
            get { return mData; }
        }

        private SessionState mState;
        public SessionState State
        {
            get { return mState; }
        }

        public bool Started
        {
            get { return mState > SessionState.StartCall; }
        }

        private int mSessionID;
        public int SessionID
        {
            get { return mSessionID; }
        }

        private SessionStartCallParams mStartCallParams;
        internal SessionStartCallParams StartCallParams
        {
            get { return mStartCallParams; }
        }

        private Variant mProperties;
        public Variant Properties
        {
            get { return mProperties[USER]; }
        }

        private int mPropertyRev;
        internal int PropertyRev
        {
            get { return mPropertyRev; }
        }

        private int mRpcRev;
        internal int RpcRev
        {
            get { return mRpcRev; }
        }

        private Manager mManager;
        public Manager Manager
        {
            get { return mManager; }
        }

        public bool IsManager
        {
            get { return mManager != null; }
        }

        private GameClient mGameClient;
        #endregion Data and Properties


        #region Setup
        internal Session(OrbitXAPI api, JoinSessionResponse data, int sessionId)
        {
            mAPI = api;
            mGameClient = api.GameClient;
            mSessionID = sessionId;
            mData = data.Configs;
            CachePlayersFromSessionData();

            mRpcRev = 0;
            mAutoObjectID = AutoSyncObject.GetUniqueBaseID(mAPI.Player.UserID);
            InitializeProperties(data);
            mState = data.Configs.IsStarted ? SessionState.Running : SessionState.Joined;

            UnityBroker.RegisterSession(this);
            UnityBroker.PostToUnity(() =>
            {
                mGameObject = new GameObject("Session #" + mSessionID);
                mGameObject.transform.parent = UnityBroker.Instance.SDKRootObject.transform;
                mSessionMagic = mGameObject.AddComponent<SessionMagics>();
                mSessionMagic.AssignToSession(this);
                AttachAutoObject(mGameObject, 0);
                ScanForOrphanObjects();

                if(data.__isset.cachedRPCs)
                    foreach (var rpc in data.CachedRPCs)
                        QueueRPC(rpc);
            });
        }

        private void CleanupGood(SessionLeaveReason reason)
        {
            UnityBroker.PostToUnity(() =>
            {
                foreach (var entry in mObjects)
                    if (entry.Value.IsManual)
                        entry.Value.AssignSession(null);
                    else
                        UnityEngine.Object.Destroy(entry.Value.gameObject);

                mObjects.Clear();
                UnityEngine.Object.Destroy(mGameObject);
            });

            mGameClient.SessionExited(this);
            mAPI.Events.OnLeftSession.Fire(new LeftSessionArgs(this, reason));

            UnityBroker.UnregisterSession(this);
        }
        #endregion Setup


        #region Manager
        internal void PromoteToManager(Packet packet)
        {
            mAPI.Log.d("[Session] Promoted to manager");
            mManager = new Manager(mAPI, this);
            mAPI.Events.OnMadeManager.Fire(new MadeManagerArgs(this));
        }
        #endregion Manager


        #region Session State
        internal void ChangeState(SessionState state, bool fireEventToo = true)
        {
            SessionState prevState = mState;
            mState = state;

            if(fireEventToo)
                mAPI.Events.OnSessionStateChanged.Fire(new SessionStateChangedArgs(this, prevState));
        }
        #endregion Session State


        #region Session Start
        internal void ProcessStartCall(Packet packet)
        {
            ChangeState(SessionState.StartCall, false);
            mAPI.Events.OnSessionReadyToStart.Fire(new SessionReadyToStartArgs(this, packet.PReady.Timeout));
        }

        internal void ProcessStartEvent(Packet packet)
        {
            mData.ParticipantsToSide = packet.PStarted.UIDToSideMap;
            CachePlayersFromSessionData();
            ChangeState(SessionState.Running, false);
            mAPI.Events.OnSessionStarted.Fire(new SessionStartedArgs(this));
        }

        internal void ProcessStartCancelled(Packet packet)
        {
            var details = packet.PStartCanceled;
            ChangeState(SessionState.Joined, false);
            mAPI.Events.OnSessionStartCancelled.Fire(new SessionStartCancelledArgs(
                this,
                details.Reason.Convert(),
                mAPI.PlayerPool.GetPlayersByUID(details.Culprits).ToArray()
            ));
        }

        public void FireStartCall()
        {
            FireStartCall(true, true, 5);
        }

        public void FireStartCall(bool allShouldAcknowledge, bool cancellable, int timeout)
        {
            if (mState != SessionState.Joined)
                return;

            mStartCallParams = new SessionStartCallParams();
            mStartCallParams.AllShouldAcknowledge = allShouldAcknowledge;
            mStartCallParams.PlayersCanCancel = cancellable;
            mStartCallParams.Timeout = timeout;

            // Do not fire event
            mState = SessionState.StartCall;
            mGameClient.IgniteSessionStartCall(this);
        }

        public void RespondToStartCall(bool proceed)
        {
            mGameClient.RespondToStartCall(this, proceed);
        }
        #endregion Session Start


        #region Instantiation
        internal UnityEngine.Object InstantiateMagic(bool igniteMagic, int objectId, string resourceName, object[] prms)
        {
            if (mObjects.ContainsKey(objectId))
            {
                mAPI.Log.e("[Session] Instantiate: Can not instantiate an object to have ID " + objectId + " since another one with the same id exists");
                return null;
            }

            var resource = Resources.Load(resourceName);
            if (resource == null)
            {
                mAPI.Log.e("[Session] Instantiate: Can not load resource " + resourceName);
                return null;
            }

            UnityEngine.Object result = null;
            if (prms.Length == 0)
                result = GameObject.Instantiate(resource);
            else if (prms.Length == 1)
            {
                var parent = GetObjectByID((int)prms[0]);
                if (parent == null)
                    mAPI.Log.e("[Session] Instantiate: can not find parent by object #" + (int)prms[0]);
                else
                    result = GameObject.Instantiate(resource, parent.gameObject.transform);
            }
            else if (prms.Length == 2)
            {
                var parent = GetObjectByID((int)prms[0]);
                if (parent == null)
                    mAPI.Log.e("[Session] Instantiate: can not find parent by object #" + (int)prms[0]);
                else
                    result = GameObject.Instantiate(resource, parent.gameObject.transform, (bool)prms[1]);
            }
            else if (prms.Length == 7)
            {
                Vector3 position = new Vector3((float)prms[0], (float)prms[1], (float)prms[2]);
                Quaternion rotation = new Quaternion((float)prms[3], (float)prms[4], (float)prms[5], (float)prms[6]);
                result = GameObject.Instantiate(resource, position, rotation);
            }
            else if (prms.Length == 8)
            {
                var parent = GetObjectByID((int)prms[7]);
                if (parent == null)
                    mAPI.Log.e("[Session] Instantiate: can not find parent by object #" + (int)prms[0]);
                else
                {
                    Vector3 position = new Vector3((float)prms[0], (float)prms[1], (float)prms[2]);
                    Quaternion rotation = new Quaternion((float)prms[3], (float)prms[4], (float)prms[5], (float)prms[6]);
                    result = GameObject.Instantiate(resource, position, rotation, parent.gameObject.transform);
                }
            }
            else
                mAPI.Log.e("[Session] unsupported method of instantiation: " + prms.Length);

            if (result != null)
            {
                AttachAutoObject(result as GameObject, objectId);
                if (igniteMagic)
                    (result as GameObject).RPC("InstantiateMagic", objectId, resourceName, prms);
            }

            return result;
        }

        public UnityEngine.Object InstantiateResource(string resourceName)
        {
            var objectId = ++mAutoObjectID;
            var prms = new object[0];
            return InstantiateMagic(true, objectId, resourceName, prms);
        }

        public UnityEngine.Object InstantiateResource(string resourceName, GameObject parentObject)
        {
            var syncObj = parentObject.GetComponent<SyncObject>();
            if (syncObj == null)
            {
                mAPI.Log.e("[Session] Instantiate: Can not instantiate resource for a parent that does not have a sync object.");
                return null;
            }
            else
            {
                var objectId = ++mAutoObjectID;
                var prms = new object[1] { syncObj.ID };
                return InstantiateMagic(true, objectId, resourceName, prms);
            }
        }

        public UnityEngine.Object InstantiateResource(string resourceName, Vector3 position, Quaternion rotation)
        {
            var objectId = ++mAutoObjectID;
            var prms = new object[7] { position.x, position.y, position.z, rotation.x, rotation.y, rotation.z, rotation.w };
            return InstantiateMagic(true, objectId, resourceName, prms);
        }

        public UnityEngine.Object InstantiateResource(string resourceName, GameObject parentObject, bool worldPositionStays)
        {
            var syncObj = parentObject.GetComponent<SyncObject>();
            if (syncObj == null)
            {
                mAPI.Log.e("[Session] Instantiate: Can not instantiate resource for a parent that does not have a sync object.");
                return null;
            }
            else
            {
                var objectId = ++mAutoObjectID;
                var prms = new object[2] { syncObj.ID, worldPositionStays };
                return InstantiateMagic(true, objectId, resourceName, prms);
            }
        }

        public UnityEngine.Object InstantiateResource(string resourceName, Vector3 position, Quaternion rotation, GameObject parentObject)
        {
            var syncObj = parentObject.GetComponent<SyncObject>();
            if (syncObj == null)
            {
                mAPI.Log.e("[Session] Instantiate: Can not instantiate resource for a parent that does not have a sync object.");
                return null;
            }
            else
            {
                var objectId = ++mAutoObjectID;
                var prms = new object[8] { position.x, position.y, position.z, rotation.w, rotation.x, rotation.y, rotation.z, syncObj.ID };
                return InstantiateMagic(true, objectId, resourceName, prms);
            }
        }
        #endregion Instantiation


        #region etc.
        public override string ToString()
        {
            return "Session #" + mSessionID;
        }
        #endregion etc.
    }
}
