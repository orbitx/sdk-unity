﻿using System.Collections.Generic;
using UnityEngine;

namespace OrbitXSDK
{
    public partial class Session
    {
        private Dictionary<int, SyncObject> mObjects = new Dictionary<int, SyncObject>();

        public SyncObject GetObjectByID(int id)
        {
            if (mObjects.ContainsKey(id))
                return mObjects[id];

            return null;
        }

        public void AttachObject(SyncObject syncObj)
        {
            mObjects[syncObj.ID] = syncObj;
            syncObj.AssignSession(this);
        }

        public void AttachObject(GameObject go)
        {
            var syncObject = go.GetComponent<SyncObject>();
            if (syncObject != null)
                AttachObject(syncObject);
            else
                mAPI.Events.OnProblem.Fire("[Session] Can not attach a game object without sync object", Packets.ExceptionCode.GO_WITHOUT_SYNC);
        }

        private void AttachAutoObject(GameObject original, int id)
        {
            var syncObj = original.AddComponent<AutoSyncObject>();
            syncObj.AssignObjectId(id);
            AttachObject(syncObj);
        }

        internal void SyncObjectDestroyed(SyncObject syncObj)
        {
            mObjects.Remove(syncObj.SerializedObjectID);
        }

        private void ScanForOrphanObjects()
        {
            foreach (var syncObject in Object.FindObjectsOfType<SyncObject>())
                if (syncObject.Session == null)
                    AttachObject(syncObject);
        }
    }
}
