﻿using UnityEngine;

namespace OrbitXSDK
{
    internal class SessionMagics : MonoBehaviour
    {
        private Session mSession;

        internal void AssignToSession(Session session)
        {
            mSession = session;
        }

        [OrbitXRPC]
        internal void InstantiateMagic(int objectId, string resourceName, object [] prms)
        {
            mSession.InstantiateMagic(false, objectId, resourceName, prms);
        }
    }
}
