﻿using OrbitXSDK;
using System;

static public class SessionPropertiesExtensions
{
    static internal bool CheckChanges = false;
    static internal Action FailAction = null;

    static public void TryToSet(this Variant variant, string key, Variant value, Action onFail)
    {
        var devCheckSetting = CheckChanges;
        CheckChanges = true;
        FailAction = onFail;

        variant[key] = value;

        CheckChanges = devCheckSetting;
    }

    static public void TryToSet(this Variant variant, int key, Variant value, Action onFail)
    {

    }
}
