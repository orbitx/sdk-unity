﻿using OrbitXSDK.Packets;
using System.Collections.Generic;

namespace OrbitXSDK
{
    public class Manager
    {
        private Session mSession;
        private OrbitXAPI mAPI;

        internal Manager(OrbitXAPI api, Session parent)
        {
            mAPI = api;
            mSession = parent;
        }

        public void Kick(Player player)
        {
            var packet = new Packet(PacketType.MANAGER_KICK);
            packet.PManagerKick = new ManagerKick(mSession.SessionID, player.UserID);
            mAPI.GameClient.SendCasualQuery(packet, response =>
            {
                if(!response.IsValid())
                    mAPI.Events.OnProblem.Fire("[Manager] Failed to kick player", response.PException);
            });
        }

        public void ChangePlayerSide(Player player, int newSide)
        {
            var packet = new Packet(PacketType.CHANGE_SIDE);
            packet.PChangeSide = new ChangeSide(mSession.SessionID, newSide);
            packet.PChangeSide.AsUID = player.UserID;

            var prevSide = mSession.PlayerSides[player];
            mSession.AssignPlayer(player, newSide);

            mAPI.GameClient.SendCasualQuery(packet, response => {
                if (!response.IsValid())
                {
                    mSession.AssignPlayer(player, prevSide);
                    mAPI.Events.OnProblem.Fire("[Manager] Changing side for player failed", response.PException);
                }
            });
        }

        public SessionConfiguration GetSessionConfiguration()
        {
            return new SessionConfiguration(mSession.ConfigData);
        }

        public void ChangeSessionConfiguration(SessionConfiguration newConfig)
        {
            var packet = new Packet(PacketType.CHANGE_CONFIGS);
            packet.PChangeConfigs = new ChangeConfigs(mSession.SessionID, newConfig.Build());
            mAPI.GameClient.SendCasualQuery(packet, response =>
            {
                if (!response.IsValid())
                    mAPI.Events.OnProblem.Fire("[Manager] Changing session configuration failed", response.PException);
            });
        }

        internal void ProcessConfigChanged(Packet packet)
        {
            var p = packet.PConfigsChanged.Configs;

            if (p.__isset.alwaysAcceptJoin)
                mSession.ConfigData.AlwaysAcceptJoin = p.AlwaysAcceptJoin;
            if (p.__isset.canChangeSideAfterStart)
                mSession.ConfigData.CanChangeSideAfterStart = p.CanChangeSideAfterStart;
            if (p.__isset.disconnectionBehavior)
                mSession.ConfigData.DisconnectionBehavior = p.DisconnectionBehavior;
            if (p.__isset.disconnectionTimeout)
                mSession.ConfigData.DisconnectionTimeout = p.DisconnectionTimeout;
            if (p.__isset.minPlayerToMatch)
                mSession.ConfigData.MinPlayerToMatch = p.MinPlayerToMatch;
            if (p.__isset.options)
                mSession.ConfigData.Options = p.Options;
            if (p.__isset.label)
                mSession.ConfigData.Label = p.Label;
            if (p.__isset.password)
                mSession.ConfigData.Password = p.Password;

            mAPI.Events.OnSessionConfigurationChanged.Fire(new SessionConfigurationChangedArgs(mSession));
        }

        public void Terminate()
        {
            Packet packet = new Packet(PacketType.TERMINATE);
            packet.PTerminate = new Terminate(mSession.SessionID);
            mAPI.GameClient.SendCasualQuery(packet, response => 
            {
                if (!response.IsValid())
                    mAPI.Events.OnProblem.Fire("[Manager] Session termination failed", response.PException);
            });
            mSession.ProcessTerminate();
        }
    }
}
