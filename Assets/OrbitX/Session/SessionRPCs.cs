﻿using System.Collections.Generic;

namespace OrbitXSDK
{
    public partial class Session
    {
        private Dictionary<int, Packets.RPCCalled> mRPCsInQueue = new Dictionary<int, Packets.RPCCalled>();

        internal void QueueSelfRPC(int rpcIndex)
        {
            UnityBroker.PostToUnity(() =>
            {
                mRPCsInQueue[rpcIndex] = null;
                ProcessRPCs();
            });
        }

        internal void QueueRPC(Packets.RPCCalled details)
        {
            UnityBroker.PostToUnity(() =>
            {
                mRPCsInQueue[details.RPCindex] = details;
                ProcessRPCs();
            });
        }

        private void ProcessRPCs()
        {
            while (mRPCsInQueue.ContainsKey(mRpcRev + 1))
            {
                ExecuteRPC(mRPCsInQueue[++mRpcRev]);
                mRPCsInQueue.Remove(mRpcRev);
            }
        }

        internal bool ExecuteRPC(Packets.RPCCalled details)
        {
            if (details == null)
                return true;

            var prms = new object[details.Parameters.Count];
            foreach (var entry in details.Parameters)
                prms[entry.Key] = entry.Value.ConvertToObject(mAPI.Log);

            int objectId;
            string methodName;
            SyncObject.DecodeMethodName(details.Methodname, out objectId, out methodName);
            if (mObjects.ContainsKey(objectId))
            {
                mObjects[objectId].ExecuteRPC(methodName, details, mAPI.PlayerPool);
                return true;
            }
            else
            {
                mAPI.Events.OnProblem.Fire("[Session] Can not find object #" + objectId, Packets.ExceptionCode.OBJECT_NOT_FOUND);
                return false;
            }
        }
    }
}
