﻿using OrbitXSDK.Packets;

namespace OrbitXSDK
{
    public partial class Session
    {
        public void Leave()
        {
            Leave(true);
        }

        internal void Leave(bool notifyServer)
        {
            var packet = new Packet(PacketType.LEAVE_SESSION);
            packet.PLeaveSession = new LeaveSession(mSessionID);
            mGameClient.SendPacket(packet);

            CleanupGood(SessionLeaveReason.Voluntarily);
        }

        internal void ProcessTerminate(Packet packet = null)
        {
            CleanupGood(SessionLeaveReason.Terminated);
        }

        public void TemporaryLeave()
        {
            var packet = new Packet(PacketType.TEMP_LEAVE);
            packet.PTemporaryLeave = new Packets.TemporaryLeave(mSessionID);
            mGameClient.SendPacket(packet);

            CleanupGood(SessionLeaveReason.Temporary);
        }
    }
}
