﻿using OrbitXSDK.Packets;
using System.Collections.Generic;
using System.Linq;

namespace OrbitXSDK
{
    public partial class Session
    {
        #region Data
        private Dictionary<Player, int> mPlayerSides = new Dictionary<Player, int>();

        private void CachePlayersFromSessionData()
        {
            mPlayerSides.Clear();

            foreach (var entry in mData.ParticipantsToSide)
                mPlayerSides[mAPI.PlayerPool.GetPlayerByUID(entry.Key)] = entry.Value;

            if (!mData.ParticipantsToSide.ContainsKey(mAPI.Player.UserID))
                mPlayerSides[mAPI.Player] = 0;
        }

        public Dictionary<Player, int> PlayerSides
        {
            get { return new Dictionary<Player, int>(mPlayerSides); }
        }

        public Dictionary<int, int> SideCaps
        {
            get { return new Dictionary<int, int>(mData.SideToCapacity); }
        }

        public Player [] Players
        {
            get { return mPlayerSides.Keys.ToArray(); }
        }

        public int PlayerCount
        {
            get { return mPlayerSides.Count; }
        }
        #endregion Data


        #region Processors
        internal void AssignPlayer(Player player, int sideId)
        {
            var updateType = mPlayerSides.ContainsKey(player) ? PlayerUpdateEvent.ChangedTeam : PlayerUpdateEvent.Joined;
            mPlayerSides[player] = sideId;
            mData.ParticipantsToSide[player.UserID] = sideId;
            BroadcastParticipantUpdate(player, updateType);
        }

        private void BroadcastParticipantUpdate(Player player, PlayerUpdateEvent updateType)
        {
            var data = new Dictionary<Player, PlayerUpdateEvent>();
            data[player] = updateType;
            mAPI.Events.OnPlayerListUpdated.Fire(new PlayerListUpdatedArgs(this, data));
        }

        internal void ProcessPlayerJoined(Packet packet)
        {
            var player = mAPI.PlayerPool.GetPlayerByUID(packet.PJoined.UID);
            AssignPlayer(player, packet.PJoined.Side);
        }

        internal void ProcessPlayerLeft(Packet packet)
        {
            var player = mPlayerSides.Keys.FirstOrDefault(p => p.UserID == packet.PLeft.UID);
            if (player == null)
                return;

            mData.ParticipantsToSide.Remove(player.UserID);
            mPlayerSides.Remove(player);
            BroadcastParticipantUpdate(player, PlayerUpdateEvent.Left);
        }

        internal void ProcessSideChanged(Packet packet)
        {
            var player = mAPI.PlayerPool.GetPlayerByUID(packet.PSideChanged.UID);
            AssignPlayer(player, packet.PSideChanged.NewSide);
        }
        #endregion Processors


        #region Interface
        public void ChangeMySide(int newSide)
        {
            var packet = new Packet(PacketType.CHANGE_SIDE);
            packet.PChangeSide = new ChangeSide(mSessionID, newSide);

            var prevSide = mPlayerSides[mAPI.AuthClient.Self];
            mGameClient.SendCasualQuery(packet, response => {
                if(!response.IsValid())
                    AssignPlayer(mAPI.AuthClient.Self, prevSide);
            });

            AssignPlayer(mAPI.AuthClient.Self, newSide);
        }
        #endregion Interface
    }
}
