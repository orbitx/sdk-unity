﻿using OrbitXSDK.Packets;
using System;
using System.Linq;
using System.Collections.Generic;

namespace OrbitXSDK
{
    public partial class Session
    {
        private List<KeyValuePair<string, PropertyInstance>> mPropertyChanges;
        private Dictionary<string, Action> mFailActions = new Dictionary<string, Action>();

        private void PropertyChangedInternalCallback(ModifiedEntry modifiedEntry)
        {
            // Do not care for top level node assignments
            if (modifiedEntry.Path.Length == 1)
                return;

            // Get or Create
            PropertyInstance propChange;
            if (mPropertyChanges.Exists(e => e.Key == modifiedEntry.Path))
            {
                propChange = mPropertyChanges.First(e => e.Key == modifiedEntry.Path).Value;
                if (modifiedEntry.Modification == Modification.Delete)
                    propChange.Operation = OperationType.DELETE;
            }
            else if (modifiedEntry.Modification != Modification.Delete)
            {
                if (SessionPropertiesExtensions.CheckChanges)
                {
                    propChange = new PropertyInstance(modifiedEntry.Path, OperationType.TRY_MODIFY);
                    mFailActions[modifiedEntry.Path] = SessionPropertiesExtensions.FailAction;
                    if (modifiedEntry.Modification == Modification.Modify && !modifiedEntry.PreviousValue.IsNull)
                        propChange.CachedValue = Extensions.ConvertToDOMNode(modifiedEntry.PreviousValue.AsObject);
                    else
                        propChange.CachedValue = new DOMNode();
                }
                else
                    propChange = new PropertyInstance(modifiedEntry.Path, OperationType.MODIFY);
            }
            else
                propChange = new PropertyInstance(modifiedEntry.Path, OperationType.DELETE);

            // Put value and record the change
            if (modifiedEntry.Modification != Modification.Delete)
                propChange.Value = Extensions.ConvertToDOMNode(modifiedEntry.NewValue.AsObject);

            mPropertyChanges.RemoveAll(e => e.Key == modifiedEntry.Path);
            mPropertyChanges.Add(new KeyValuePair<string, PropertyInstance>(modifiedEntry.Path, propChange));
        }

        internal void FrameRenderUpdate()
        {
            if (mPropertyChanges.Count == 0)
                return;

            var changes = new List<PropertyInstance>();
            var failActions = new Dictionary<int, Action>();
            for (int i = 0; i < mPropertyChanges.Count; ++i)
            {
                changes.Add(mPropertyChanges[i].Value);
                failActions[i] = mFailActions.ContainsKey(mPropertyChanges[i].Key) ? mFailActions[mPropertyChanges[i].Key] : null;
            }

            var packet = new Packet(PacketType.SET_PROPERTIES);
            packet.PSetProperties = new SetProperties(mSessionID, changes);
            mGameClient.SendStrictQuery(packet, response =>
            {
                if (response.IsValid())
                {
                    var failCalls = new List<Action>();
                    var details = response.PSetPropertiesResponse;

                    for (int i = 0; i < details.ResultFlags.Count; ++i)
                        if (details.ResultFlags[i] != SetPropertyResult.OK)
                            if (failActions[i] != null)
                                failCalls.Add(failActions[i]);

                    if (details.RevisionIndex > mData.PropertyRevisionIndex)
                        mData.PropertyRevisionIndex = details.RevisionIndex;

                    if (failCalls.Count > 0)
                        UnityBroker.PostToUnity(() =>
                        {
                            for (int i = 0; i < failCalls.Count; ++i)
                                failCalls[i]();
                        });
                }
                else
                    mAPI.Events.OnProblem.Fire("[Session] Problem when updating session properties", response.PException);
            });

            mPropertyChanges.Clear();
        }

        private ModifiedEntry TraverseVariantIndex(ref Variant node, ref int index, PropertyInstance change)
        {
            ++index;

            int listIndex = 0;
            while (index < change.Path.Length && change.Path[index] != ']')
                listIndex = listIndex * 10 + (change.Path[index++] - '0');

            if (index < change.Path.Length && change.Path[index] == ']')
                ++index;
            if (index < change.Path.Length && change.Path[index] == '.')
                ++index;

            if (index < change.Path.Length)
            {
                if (listIndex < node.Count)
                {
                    node = node[listIndex];
                    return null;
                }
                else
                    throw new InvalidOperationException(string.Format("[Session] Property operation not valid: {0} on path {1}", change.Operation, change.Path));
            }
            else
            {
                ModifiedEntry result = new ModifiedEntry();
                if (change.Operation == OperationType.DELETE)
                {
                    if (listIndex < node.Count)
                    {
                        result.Modification = Modification.Delete;
                        result.PreviousValue = node[listIndex];
                        result.NewValue = Variant.Null;
                        node.RemoveAt(listIndex, false);
                    }
                    else
                    {
                        mAPI.Log.e("[Session] Attempt to remove non-existing session property " + change.Path.Substring(2));
                        result = null;
                    }
                }
                else
                {
                    result.NewValue = change.Value.ConvertToVariant(mAPI.Log);
                    if (listIndex == node.Count)
                    {
                        result.Modification = Modification.Insert;
                        result.PreviousValue = Variant.Null;
                        node.Add(result.NewValue, false);
                    }
                    else
                    {
                        result.Modification = Modification.Modify;
                        result.PreviousValue = node[listIndex];
                        node.SetListItem(listIndex, result.NewValue, false);
                    }
                }

                return result;
            }
        }

        private ModifiedEntry TraverseVariantKey(ref Variant node, ref int index, PropertyInstance change)
        {
            int start = index;
            while (index < change.Path.Length && change.Path[index] != '[' && change.Path[index] != '.')
                ++index;

            var key = change.Path.Substring(start, index - start);
            if (index < change.Path.Length && change.Path[index] == '.')
                ++index;

            if (index < change.Path.Length)
            {
                if (node.ContainsKey(key))
                {
                    node = node[key];
                    return null;
                }
                else
                    throw new InvalidOperationException(string.Format("[Session] Property operation not valid: {0} on path {1}", change.Operation, change.Path));
            }
            else
            {
                ModifiedEntry result = new ModifiedEntry();
                if (change.Operation == OperationType.DELETE)
                {
                    if (node.ContainsKey(key))
                    {
                        result.Modification = Modification.Delete;
                        result.PreviousValue = node[key];
                        result.NewValue = null;
                        node.Remove(key, false);
                    }
                    else
                    {
                        mAPI.Log.e("[Session] Attempt to remove non-existing session property " + change.Path.Substring(2));
                        result = null;
                    }
                }
                else
                {
                    var newValue = change.Value.ConvertToVariant(mAPI.Log);
                    if (node.ContainsKey(key))
                    {
                        result.Modification = Modification.Modify;
                        result.PreviousValue = node[key];
                    }
                    else
                    {
                        result.Modification = Modification.Insert;
                        result.PreviousValue = Variant.Null;
                    }

                    result.NewValue = newValue;
                    node.Add(key, newValue, false);
                }

                return result;
            }
        }

        private ModifiedEntry ProcessPropertyChange(PropertyInstance change)
        {
            int index = 0;
            Variant curNode = mProperties;
            ModifiedEntry result;
            while (index < change.Path.Length)
            {
                try
                {
                    if (change.Path[index] == '[')
                        result = TraverseVariantIndex(ref curNode, ref index, change);
                    else
                        result = TraverseVariantKey(ref curNode, ref index, change);

                    if (result != null)
                    {
                        result.Path = change.Path.Substring(2);
                        return result;
                    }
                }
                catch
                {
                    mAPI.Log.ef("[Session] Property operation not valid: {0} on path {1}", change.Operation, change.Path.Substring(2));
                }
            }

            return null;
        }

        internal void ProcessPropertyChanges(int revisionId, long userId, List<PropertyInstance> changes, bool silentUpdate)
        {
            if (revisionId > mData.PropertyRevisionIndex)
                mData.PropertyRevisionIndex = revisionId;

            int userChangeCount = 0;
            changes.ForEach(change => { if (change.Path.StartsWith(USER)) userChangeCount++; });

            List<ModifiedEntry> userChanges = null;
            if (userChangeCount > 0)
                userChanges = new List<ModifiedEntry>();

            ModifiedEntry mod;
            foreach (var change in changes)
            {
                mod = ProcessPropertyChange(change);
                if (mod != null && change.Path[0] == USER[0])
                    userChanges.Add(mod);
            }

            if (!silentUpdate && userChanges != null)
                mAPI.Events.OnSessionPropertyChanged.Fire(new SessionPropertyChangedArgs(this, userChanges.ToArray()));
        }

        internal void InitializeProperties(JoinSessionResponse packet)
        {
            if (packet.__isset.properties)
                mProperties = packet.Properties.ConvertToVariant(mAPI.Log, PropertyChangedInternalCallback);
            else
            {
                mProperties = Variant.CreateDictionary(PropertyChangedInternalCallback);
                mProperties.Add(USER, Variant.CreateDictionary(), false);
                mProperties.Add(SDK, Variant.CreateDictionary(), false);
            }

            mPropertyRev = 0;
            mPropertyChanges = new List<KeyValuePair<string, PropertyInstance>>();
        }
    }
}
