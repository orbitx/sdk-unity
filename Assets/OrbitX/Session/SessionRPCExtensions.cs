﻿using OrbitXSDK;
using System.Collections.Generic;
using UnityEngine;

static public class SessionRPCExtensions
{
    static public void ScanRPCs(this GameObject go)
    {
        try
        {
            go.GetComponent<SyncObject>().ScanRPCs();
        }
        catch
        { }
    }

    static public void RPC(this GameObject go, string methodName, params object [] args)
    {
        RPC(go, RPCFlags.Normal, null, null, methodName, args);
    }

    static public void RPC(this GameObject go, RPCFlags flags, string methodName, params object[] args)
    {
        RPC(go, flags, null, null, methodName, args);
    }

    static public void RPC(this GameObject go, RPCFlags flags, Player mockPlayer, string methodName, params object[] args)
    {
        RPC(go, flags, mockPlayer, null, methodName, args);
    }

    static public void RPC(this GameObject go, RPCFlags flags, Player mockPlayer, List<Player> recepients, string methodName, params object[] args)
    {
        var syncObject = go.GetComponent<SyncObject>();
        if (syncObject == null)
            throw new System.Exception("[RPC] Can not make RPC on GameObject " + go + " when there is no sync object attached to it");

        var details = new RPCInvokeData()
        {
            Session = syncObject.Session,
            MethodName = SyncObject.EncodeMethodName(syncObject, methodName),
            Parameters = args,
            ForMeToo = (flags & RPCFlags.ForMeToo) == RPCFlags.ForMeToo,
            Persistent = (flags & RPCFlags.Persistent) == RPCFlags.Persistent,
            Recepients = recepients,
            InvokeAs = mockPlayer
        };

        syncObject.Session.API.GameClient.InvokeRPC(details, syncObject.Session);
    }
}
