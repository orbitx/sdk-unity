﻿using OrbitXSDK.Packets;
using System.Collections.Generic;

namespace OrbitXSDK
{
    internal partial class GameClient
    {
        #region Auto Matchmake
        private Dictionary<long, object> mTracks = new Dictionary<long, object>();

        internal void RequestAutoSession(MatchmakeOptions options, double playerScore, double acceptableRange, int poolingTime, object tag)
        {
            var packet = new Packet(PacketType.SUBSCRIBE);
            packet.PSubscribe = new Subscribe();
            packet.PSubscribe.Indicator = playerScore;
            packet.PSubscribe.MaxIndicatorDistance = acceptableRange;
            packet.PSubscribe.PoolTime = poolingTime;
            if (options.PreferedSide != -1)
                packet.PSubscribe.Side = options.PreferedSide;

            packet.PSubscribe.Configs = options.Build();

            SendStrictQuery(packet, (response) =>
            {
                if (response.IsValid())
                {
                    var trackId = response.PSubscribeResponse.TrackID;
                    if (mTracks.ContainsKey(trackId))
                        mAPI.Log.d("[GameClient] Track ID already registered: " + trackId);

                    mTracks[trackId] = tag;
                    if (options.OnTrackIdReceived != null)
                        UnityBroker.PostToUnity(options.OnTrackIdReceived);
                }
                else
                    NotifyMatchmakeFailure(tag, response.PException);
            });
        }

        internal void CancelAutoSessionRequest(object tag)
        {
            var trackId = mTracks.GetKeyFor(tag, -1);
            if(trackId != -1)
            {
                mTracks.Remove(trackId);
                var packet = new Packet(PacketType.UNSUBSCRIBE);
                packet.PUnsubscribe = new Unsubscribe(trackId);
                SendPacket(packet);
            }
            else
                mAPI.Log.ef("[GameClient] Attempted to cancel matchmake but the given tag is not found for any requests.");
        }

        private void FunctionSubscriptionResult(Packet packet)
        {
            var details = packet.PSubscriptionResult;
            var tag = mTracks.ContainsKey(details.TrackID) ? mTracks[details.TrackID] : null;
            mTracks.Remove(details.TrackID);

            SetupSession(details.SessionID, tag, new JoinSessionResponse(details.Configs));
        }
        #endregion Auto Matchmake


        #region Manual matchmake
        internal void RequestCreateSession(MatchmakeOptions options, string label, string password, int startingSide, object tag)
        {
            var configs = options.Build();
            configs.Label = label;
            configs.Password = password;

            var packet = new Packet(PacketType.CREATE_SESSION);
            packet.PCreateSession = new CreateSession(configs);
            packet.PCreateSession.Side = startingSide;

            SendStrictQuery(packet, (response) =>
            {
                if (response.IsValid())
                {
                    configs.ParticipantsToSide = new Dictionary<long, int>();
                    configs.ParticipantsToSide[mAPI.Player.UserID] = startingSide;
                    SetupSession(response.PCreateSessionResponse.SessionID, tag, new JoinSessionResponse(configs));
                }
                else
                    NotifyMatchmakeFailure(tag, response.PException);
            });
        }

        internal void JoinSessionByID(int sessionId, string password, int side, object tag)
        {
            var packet = new Packet(PacketType.JOIN_SESSION);
            var prms = new JoinSession(sessionId);
            if (password != null)
                prms.Password = password;
            prms.Side = side;
            packet.PJoinSession = prms;

            SendStrictQuery(packet, (response) =>
            {
                if (response.IsValid())
                    SetupSession(sessionId, tag, response.PJoinSessionResponse);
                else
                    NotifyMatchmakeFailure(tag, response.PException);
            });
        }

        internal void GetSessionList(MatchmakeOptions options, SessionListCallback callback)
        {
            var configs = options.Build();
            var packet = new Packet(PacketType.SESSION_LIST);
            packet.PSessionList = new SessionList(configs.Options);
            SendStrictQuery(packet, response =>
            {
                if (response.IsValid())
                {
                    var sessions = response.PSessionListResponse.IDtoConfigs;
                    var result = new SessionListingInfo[sessions.Count];

                    int i = 0;
                    foreach (var entry in sessions)
                        result[i++] = new SessionListingInfo(entry.Key, entry.Value, mAPI);

                    UnityBroker.PostToUnity(() => callback(result));
                }
                else
                    mAPI.Log.ef("[GameClient] Session list retrieval failure: " + response.GetExceptionString());
            });
        }
        #endregion Manual matchmake


        #region Aux
        private void NotifyMatchmakeFailure(object tag, Exception exceptionPacket)
        {
            mAPI.Events.OnMatchmakeFailed.Fire(new MatchmakeFailedArgs(tag, exceptionPacket));
        }
        #endregion Aux
    }
}
