﻿using OrbitXSDK.Packets;

namespace OrbitXSDK.Clients
{
    internal abstract class GameClientConnection
    {
        internal delegate void ConnectionEstablishedCallback();
        private ConnectionEstablishedCallback mConnectCallback;

        internal delegate void DisconnecedCallback(ConnectionStateDetail detail);
        private DisconnecedCallback mDisconnectCallback;

        internal delegate void PacketReceivedCallback(Packet thePacket);
        private PacketReceivedCallback mPacketReceivedCallback;

        internal void PlugCallbacks(ConnectionEstablishedCallback connect, DisconnecedCallback disconnect, PacketReceivedCallback recv)
        {
            mConnectCallback = connect;
            mDisconnectCallback = disconnect;
            mPacketReceivedCallback = recv;
        }

        protected void FireConnectedCallback()
        {
            mConnectCallback();
        }

        protected void FireDisconnectedCallback(ConnectionStateDetail detail)
        {
            mDisconnectCallback(detail);
        }

        protected void FirePacketRecvCallback(Packet thePacket)
        {
            if(IsConnected())
                mPacketReceivedCallback(thePacket);
        }

        internal abstract void Connect(string ip, int port);

        internal abstract void Disconnect();

        internal abstract void Send(Packet thePacket);

        internal abstract bool IsConnected();
    }
}
