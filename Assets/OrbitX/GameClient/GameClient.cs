﻿using System;
using OrbitXSDK.Packets;
using System.Collections.Generic;

namespace OrbitXSDK
{
    internal partial class GameClient
    {
        #region Data and Properties
        internal const int RETRY_INTERVAL = 2000;
        internal const int RETRY_COUNT = 3;

        private OrbitXAPI mAPI;

        private delegate void ProcessPacketDelegate(Packet thePacket);
        private Dictionary<PacketType, ProcessPacketDelegate> mProcessors;

        private Dictionary<long, QueryInfo> mQueries;
        private Queue<long> mLastReceivedPackets;

        private ConnectionState mConnectionState;
        internal ConnectionState ConnectionState
        {
            get { return mConnectionState; }
        }

        private Clients.GameClientConnection mConnection;

        private object mPersistConnectionLock;
        private bool mStartedOnce;

        internal delegate void QueryResponseCallback(Packet thePacket);
        private long mNextClientMagic = 1;
        #endregion Data and Properties

        internal GameClient(OrbitXAPI api)
        {
            mAPI = api;
            mPersistConnectionLock = false;
            mStartedOnce = false;
            mQueries = new Dictionary<long, QueryInfo>();
            mNextClientMagic = 1;
            mLastReceivedPackets = new Queue<long>(10);
            mSessions = new List<Session>();
            mConnectionState = ConnectionState.Disconnected;

            mProcessors = new Dictionary<PacketType, ProcessPacketDelegate>();
            mProcessors[PacketType.IDENTIFY] = FunctionIdentify;
            mProcessors[PacketType.SUBSCRIPTION_RESULT] = FunctionSubscriptionResult;
            mProcessors[PacketType.PROPERTIES_CHANGED] = FunctionPropertiesChanged;
            mProcessors[PacketType.AVAILABLE_ACTIVE_SESSIONS] = FunctionHandleActiveSession;
            mProcessors[PacketType.EXCEPTION] = FunctionException;

            mProcessors[PacketType.BE_MANAGER] = packet => ExecuteOnSession(packet, packet.PBeManager.SessionID, session => session.PromoteToManager(packet));
            mProcessors[PacketType.JOINED] = packet => ExecuteOnSession(packet, packet.PJoined.SessionID, session => session.ProcessPlayerJoined(packet));
            mProcessors[PacketType.LEFT] = packet => ExecuteOnSession(packet, packet.PLeft.SessionID, session => session.ProcessPlayerLeft(packet));
            mProcessors[PacketType.SIDE_CHANGED] = packet => ExecuteOnSession(packet, packet.PSideChanged.SessionID, session => session.ProcessSideChanged(packet));
            mProcessors[PacketType.CONFIG_CHANGED] = packet => ExecuteOnSessionManager(packet, packet.PConfigsChanged.SessionID, manager => manager.ProcessConfigChanged(packet));
            mProcessors[PacketType.READY] = packet => ExecuteOnSession(packet, packet.PReady.SessionID, session => session.ProcessStartCall(packet));
            mProcessors[PacketType.START_CANCELED] = packet => ExecuteOnSession(packet, packet.PStartCanceled.SessionID, session => session.ProcessStartCancelled(packet));
            mProcessors[PacketType.STARTED] = packet => ExecuteOnSession(packet, packet.PStarted.SessionID, session => session.ProcessStartEvent(packet));
            mProcessors[PacketType.TERMINATED] = packet => ExecuteOnSession(packet, packet.PTerminated.SessionID, session => session.ProcessTerminate(packet));
            mProcessors[PacketType.RPC_CALLED] = packet => ExecuteOnSession(packet, packet.PRPCCalled.SessionID, session => session.QueueRPC(packet.PRPCCalled));
        }


        #region Connection
        internal void UpdateConnectionState(ConnectionState newState, ConnectionStateDetail detail)
        {
            if (newState == mConnectionState)
                return;

            mConnectionState = newState;
            mAPI.Events.OnConnectionStateChanged.Fire(new ConnectionStateChangedArgs(newState, detail));
        }

        internal void SetupConnection()
        {
            if (mConnectionState == ConnectionState.Connected || mConnectionState == ConnectionState.Connecting)
            {
                mAPI.Events.OnProblem.Fire(
                    "[GameClient] Can not update connection parameters",
                    ExceptionCode.CONNECTION_ALREADY_SETUP,
                    "Connection is already setup"
                );
            }
            else
            {
                UpdateConnectionState(ConnectionState.Connecting, ConnectionStateDetail.NoDetail);

                mAPI.AuthClient.Initialize(aaaSuccess =>
                {
                    if (!aaaSuccess)
                        UpdateConnectionState(ConnectionState.Disconnected, ConnectionStateDetail.AuthClientNotInitialized);
                    else
                        StartPersistentConnection();
                });
            }
        }

        internal void ResumePersistentConnection()
        {
            if (mStartedOnce && mConnectionState == ConnectionState.Disconnected)
                StartPersistentConnection();
        }

        internal void StartPersistentConnection()
        {
            lock (mPersistConnectionLock)
            {
                mStartedOnce = true;
                mConnection = new Clients.GameClientTcpConnection(mAPI.NetworkUsage, mAPI.Log);
                mConnection.PlugCallbacks(ConnectionImpConnected, ConnectionImpDisconnected, ProcessPacket);

                Network.Instance.PostAction(() =>
                {
                    mConnection.Connect(mAPI.ServerAddress, mAPI.ServerPort);
                });
            }
        }

        internal void StopPersistentConnection(bool manualStop)
        {
            lock (mPersistConnectionLock)
            {
                if (mConnection != null)
                    mConnection.Disconnect();

                mConnection = null;

                if (manualStop)
                    mStartedOnce = false;
            }
        }

        private void ConnectionImpConnected()
        {
        }

        private void ConnectionImpDisconnected(ConnectionStateDetail detail)
        {
            if (mConnectionState != ConnectionState.Disconnected)
                UpdateConnectionState(ConnectionState.Disconnected, detail);
        }
        #endregion Connection


        #region Packets and Queries
        internal delegate bool PacketIntercepter(Packet packet);
        private List<PacketIntercepter> mInterceptors = new List<PacketIntercepter>();

        internal void RegisterPacketIntercepter(PacketIntercepter callback)
        {
            mInterceptors.Add(callback);
        }

        internal void UnregisterPacketIntercepter(PacketIntercepter callback)
        {
            mInterceptors.Remove(callback);
        }

        internal void ClearPacketIntercepters()
        {
            mInterceptors.Clear();
        }

        internal void SendPacket(Packet thePacket)
        {
            if (mConnection != null && mConnection.IsConnected())
                Network.Instance.PostAction(() => mConnection.Send(thePacket));
            else
                mAPI.Log.e("[GameClient] Connection is not setup");
        }

        internal void SendCasualQuery(Packet thePacket, QueryResponseCallback callback)
        {
            SendQuery(thePacket, true, callback);
        }

        internal void SendStrictQuery(Packet thePacket, QueryResponseCallback callback)
        {
            SendQuery(thePacket, false, callback);
        }

        private void SendQuery(Packet thePacket, bool optionalResponse, QueryResponseCallback callback)
        {
            long cmagic = ++mNextClientMagic;
            mQueries[cmagic] = new QueryInfo(cmagic, thePacket, callback, optionalResponse);
            SendPacket(thePacket);

            if (optionalResponse)
                Network.Instance.PostActionDelayed(RETRY_INTERVAL * RETRY_COUNT, () => mQueries.Remove(cmagic));
            else
                Network.Instance.PostActionDelayed(RETRY_INTERVAL, () => CheckQuery(thePacket, 1));
        }

        private void CheckQuery(Packet thePacket, int tryCount)
        {
            if (mConnectionState == ConnectionState.Connected && mQueries.ContainsKey(thePacket.Cmagic))
                if (mConnectionState == ConnectionState.Connected && tryCount < RETRY_COUNT)
                {
                    mAPI.Log.e("[GameClient] Query not resolved yet: " + thePacket.MsgType);
                    Network.Instance.PostActionDelayed(RETRY_INTERVAL, () => CheckQuery(thePacket, tryCount + 1));
                }
                else
                {
                    mAPI.Log.e("[GameClient] Disconneting after three checks for query resolution for " + thePacket.MsgType);
                    ConnectionImpDisconnected(ConnectionStateDetail.ServerDisconnected);
                }
        }

        private void ProcessPacket(Packet packet)
        {
            for (int i = 0; i < mInterceptors.Count; ++i)
                if (!mInterceptors[i](packet))
                    return;

            if (packet.Cmagic != 0)
            {
                if (mQueries.ContainsKey(packet.Cmagic))
                {
                    var query = mQueries[packet.Cmagic];
                    mQueries.Remove(packet.Cmagic);
                    mAPI.NetworkUsage.RecordRoundtrip(query.InitiateTime, Environment.TickCount);
                    query.Callback(packet);
                }
                else
                    mAPI.Log.ef("[GameClient] Query response received for non-existing query. Type: {0}", packet.MsgType.ToString());
            }
            else
            {
                if (packet.__isset.smagic)
                {
                    foreach (var smagic in mLastReceivedPackets)
                        if (smagic == packet.Smagic)
                        {
                            mAPI.Log.d("[GameClient] Dropping packet because it is received previously: " + packet.MsgType.ToString());
                            return;
                        }
                    mLastReceivedPackets.Enqueue(packet.Smagic);
                }

                if (mProcessors.ContainsKey(packet.MsgType))
                    mProcessors[packet.MsgType](packet);
                else
                    mAPI.Log.ef("[GameClient] ProcessPacket is not implemented for {0}", packet.MsgType.ToString());
            }
        }

        private void ExecuteOnSession(Packet packet, int sessionId, Action<Session> action)
        {
            var session = GetSessionByID(sessionId);
            if (session != null)
                try
                {
                    action(session);
                }
                catch (System.Exception ex)
                {
                    mAPI.Log.ef("[GameClient] Exception while processing packet of type {0} on session #{1}: {2}", packet.MsgType, sessionId, ex);
                }
            else
                mAPI.Log.ef("[GameClient] Session #{0} not found to handle packet of type {1}", sessionId, packet.MsgType);
        }

        private void ExecuteOnSessionManager(Packet packet, int sessionId, Action<Manager> action)
        {
            var session = GetSessionByID(sessionId);
            if (session == null)
                mAPI.Log.ef("[GameClient] Session #{0} not found to handle packet of type {1}", sessionId, packet.MsgType);
            else if (!session.IsManager)
                mAPI.Log.ef("[GameClient] Session #{0} says player is not manager to handle packet of type {1}", sessionId, packet.MsgType);
            else
                try
                {
                    action(session.Manager);
                }
                catch (System.Exception ex)
                {
                    mAPI.Log.ef("[GameClient] Exception while processing packet of type {0} on session #{1}: {2}", packet.MsgType, sessionId, ex);
                }

        }
        #endregion Packets and Queries


        #region Private Classes
        private class QueryInfo
        {
            internal Packet Packet;
            internal QueryResponseCallback Callback;
            internal int InitiateTime;
            internal bool OptionalResponse;

            internal QueryInfo(long magic, Packet packet, QueryResponseCallback callback, bool optionalResponse)
            {
                Packet = packet;
                Callback = callback;
                InitiateTime = Environment.TickCount;
                OptionalResponse = optionalResponse;
                Packet.Cmagic = magic;
            }
        }
        #endregion Private Classes
    }
}
