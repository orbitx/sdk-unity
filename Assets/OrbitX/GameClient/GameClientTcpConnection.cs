﻿using System;
using OrbitXSDK.Packets;
using System.IO;
using System.Net.Sockets;
using Thrift.Protocol;
using Thrift.Transport;
using System.Net;

namespace OrbitXSDK.Clients
{
    internal class GameClientTcpConnection : GameClientConnection
    {
        private NetworkUsage mNetworkUsageRecorder;
        private Socket mSocket;
        private object mLock = new object();
        private AsyncCallback mRecvCallback;

        private Packet mPacketToProcess;
        private byte[] mPrefix = new byte[2];
        private byte[] mInBuf = new byte[65536];
        private byte[] mOutBuf = new byte[65536];
        private MemoryStream mReadStream;
        private MemoryStream mWriteStream;
        private TProtocol mThrift;

        private int mReceivedSize;
        private int mExpectedSize;
        private bool mReceivingHeader;
        private DeveloperLogs mLog;

        internal GameClientTcpConnection(NetworkUsage networkUsageRecorder, DeveloperLogs log)
        {
            mLog = log;
            mNetworkUsageRecorder = networkUsageRecorder;
            mReadStream = new MemoryStream(mInBuf);
            mWriteStream = new MemoryStream(mOutBuf);
            mThrift = new TCompactProtocol(new TStreamTransport(mReadStream, mWriteStream));
            mRecvCallback = new AsyncCallback(ReceiveCallback);
        }

        override internal bool IsConnected()
        {
            if (mSocket == null)
                return false;

            bool part1 = mSocket.Poll(1000, SelectMode.SelectRead);
            bool part2 = (mSocket.Available == 0);
            if (part1 && part2)
                return false;
            else
                return true;
        }

        override internal void Connect(string host, int port)
        {
            lock (mLock)
            {
                try
                {
                    IPAddress ipAddress = IPAddress.Parse(host);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                    mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    mSocket.NoDelay = true;
                    mSocket.BeginConnect(remoteEP, new AsyncCallback(ar =>
                    {
                        lock (mLock)
                        {
                            try
                            {
                                Socket client = (Socket)ar.AsyncState;
                                if (mSocket != null)
                                {
                                    client.EndConnect(ar);

                                    FireConnectedCallback();
                                    BeginRecv(true, 2);
                                }
                            }
                            catch (System.Exception e)
                            {
                                mLog.ef("[TcpClient] Connection failed to game server with error {0}", e.ToString());
                                FireDisconnectedCallback(ConnectionStateDetail.ConnectionFailure);
                            }
                        }
                    }), mSocket);
                }
                catch (System.Exception e)
                {
                    FireDisconnectedCallback(ConnectionStateDetail.ConnectionFailure);
                    Log.e(e);
                }
            }
        }

        override internal void Disconnect()
        {
            lock (mLock)
            {
                if (mSocket == null)
                    return;

                if (mSocket.Connected)
                    mSocket.Close();

                mSocket = null;
                FireDisconnectedCallback(ConnectionStateDetail.DisconnectionRequest);
            }
        }

        override internal void Send(Packet thePacket)
        {
            try
            {
                mWriteStream.Seek(0, SeekOrigin.Begin);
                thePacket.Write(mThrift);
                int count = (int)mWriteStream.Position;

                mPrefix[0] = (byte)(count >> 8);
                mPrefix[1] = (byte)(count & 0xFF);
                mSocket.Send(mPrefix, 2, SocketFlags.None);
                mSocket.Send(mOutBuf, count, SocketFlags.None);
                mNetworkUsageRecorder.SentBytes += count;
            }
            catch (System.Exception ex)
            {
                mLog.ef("[TcpClient] Could not send packet with exception: " + ex.ToString());
                FireDisconnectedCallback(ConnectionStateDetail.ServerDisconnected);
            }
        }

        private void BeginRecv(bool isHeader, int expectedBodySize)
        {
            mReceivingHeader = isHeader;
            mExpectedSize = expectedBodySize;
            mReceivedSize = 0;
            ContinueRecv();
        }

        private void ContinueRecv()
        {
            mSocket.BeginReceive(mInBuf, mReceivedSize, mExpectedSize - mReceivedSize, 0, mRecvCallback, null);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                int read = mSocket.EndReceive(ar);
                if (read == 0)
                    throw new System.Exception("[TcpClient] Connection is gracefully closed by the remote host.");

                mReceivedSize += read;
                mNetworkUsageRecorder.ReceivedBytes += read;

                // Not enough bytes
                if (mReceivedSize < mExpectedSize)
                    ContinueRecv();

                // Process header and read body
                else if (mReceivingHeader)
                    BeginRecv(false, mInBuf[0] << 8 | mInBuf[1]);

                // process body and read header
                else
                {
                    var buf_tr = new TBufferedTransport(new TStreamTransport(new MemoryStream(mInBuf), new MemoryStream(mOutBuf)), 56536);
                    mPacketToProcess = new Packet();
                    mPacketToProcess.Read(new TCompactProtocol(buf_tr));

                    FirePacketRecvCallback(mPacketToProcess);
                    BeginRecv(true, 2);
                }
            }
            catch (System.Exception e)
            {
                if (e is TProtocolException)
                    mLog.ef("[TcpClient] Thrift protocol exception from buffer: ", mInBuf.ToHexString(mExpectedSize));
                else if (IsConnected())
                    mLog.ef("[TcpClient] Exception in receive callback: {0}", e.ToString());

                FireDisconnectedCallback(ConnectionStateDetail.ServerDisconnected);
            }
        }
    }
}

