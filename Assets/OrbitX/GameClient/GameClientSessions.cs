﻿using OrbitXSDK.Packets;
using System.Collections.Generic;
using System.Linq;

namespace OrbitXSDK
{
    internal partial class GameClient
    {
        private List<Session> mSessions;
        internal Session[] Sessions
        {
            get { return mSessions.ToArray(); }
        }

        private Session GetSessionByID(int sessionId)
        {
            foreach (var session in mSessions)
                if (session.SessionID == sessionId)
                    return session;

            return null;
        }

        private void SetupSession(int sessionId, object tag, JoinSessionResponse sessionData)
        {
            mAPI.Log.d("[GameClient] Setting up session #" + sessionId);

            var session = mSessions.FirstOrDefault(s => s.SessionID == sessionId);
            if (session == null)
                mSessions.Add(session = new Session(mAPI, sessionData, sessionId));

            mAPI.Events.OnMatchmakeDone.Fire(new MatchmakeDoneArgs(session, tag));
        }

        internal void SessionExited(Session session)
        {
            mAPI.Log.d("[GameClient] Removed session #" + session.SessionID);
            mSessions.Remove(session);
        }
    }
}
