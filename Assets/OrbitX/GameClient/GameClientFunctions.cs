﻿using OrbitXSDK.Packets;
using System.Collections.Generic;
using System.Linq;

namespace OrbitXSDK
{
    internal partial class GameClient
    {
        #region Exception
        private void FunctionException(Packet packet)
        {
            mAPI.Events.OnProblem.Fire("[OrbitX] Received a problem message from server", packet.PException);
            if (packet.PException.ErrorCode == ExceptionCode.LOGIN_TIMEOUT)
                UpdateConnectionState(ConnectionState.Disconnected, ConnectionStateDetail.AuthClientOTPFailure);
        }
        #endregion Exception


        #region Login
        private Packet mIdentifyChallange;

        private void FunctionIdentify(Packet packet)
        {
            mIdentifyChallange = packet;
            mAPI.AuthClient.RequestOTP((success, otp) =>
            {
                if (success)
                {
                    var reply = mIdentifyChallange.Reply(PacketType.IDENTIFY_RESPONSE);
                    reply.PIdentifyResponse = new IdentifyResponse(otp);
                    SendPacket(reply);
                    UpdateConnectionState(ConnectionState.Connected, ConnectionStateDetail.NoDetail);
                }
                else
                    UpdateConnectionState(ConnectionState.Disconnected, ConnectionStateDetail.AuthClientOTPFailure);
            });
        }
        #endregion Login


        #region Active Sessions
        private void FunctionHandleActiveSession(Packet packet)
        {
            var details = packet.PActiveSessions;
            var sessionInfos = details.SessionIDToConfigs
                .Select(entry => new SessionListingInfo(entry.Key, entry.Value, mAPI))
                .ToArray();
            mAPI.Events.OnActiveSessionFound.Fire(new ActiveSessionFoundArgs(sessionInfos, this));
        }

        internal void DeveloperDecidedOnActiveSession(int sessionId, ActiveSessionDecision decision, object tag)
        {
            ActiveSessionActionType action = decision == ActiveSessionDecision.Discard ?
                ActiveSessionActionType.LEAVE : ActiveSessionActionType.JOIN;

            var session = GetSessionByID(sessionId);
            var result = new Packet(PacketType.HANDLE_ACTIVE_SESSION);
            result.PHandleActiveSession = new HandleActiveSession(sessionId, action);
            result.PHandleActiveSession.LastCachedRPCIndex = session == null ? 0 : session.RpcRev;
            result.PHandleActiveSession.LastPropertiesRevisionIndex = session == null ? 0 : session.PropertyRev;

            if (session != null && decision == ActiveSessionDecision.Discard)
                session.Leave(false);

            SendCasualQuery(result, response =>
            {
                if (response.IsValid() && action == ActiveSessionActionType.JOIN)
                    SetupSession(sessionId, tag, null);
            });
        }
        #endregion Active Sessions


        #region Session Start
        internal void IgniteSessionStartCall(Session session)
        {
            var packet = new Packet(PacketType.START);
            var prms = session.StartCallParams;
            packet.PStart = new Start(session.SessionID, prms.Timeout, prms.AllShouldAcknowledge, prms.PlayersCanCancel);
            SendCasualQuery(packet, response =>
            {
                if (!response.IsValid())
                    mAPI.Events.OnProblem.Fire("[GameClient] Firing start call failed", response.PException);
            });
        }

        internal void RespondToStartCall(Session session, bool proceed)
        {
            Packet packet = new Packet();
            if (proceed)
            {
                packet.MsgType = PacketType.SET_READY;
                packet.PSetReady = new SetReady(session.SessionID);
            }
            else
            {
                packet.MsgType = PacketType.CANCEL_START;
                packet.PCancelStart = new CancelStart(session.SessionID);
            }

            SendCasualQuery(packet, response =>
            {
                if (response.IsValid())
                {
                    if (!proceed)
                        session.ChangeState(SessionState.Joined);
                }
                else
                    mAPI.Events.OnProblem.Fire("[GameClient] Response to start call failed", response.PException);
            });
        }
        #endregion Session Start


        #region Session Properties
        private void FunctionPropertiesChanged(Packet packet)
        {
            var details = packet.PPropertiesChanged;
            var session = GetSessionByID(details.SessionID);
            if (session != null)
                session.ProcessPropertyChanges(details.RevisionIndex, details.UID, details.Changes, false);
            else
                mAPI.Log.e("[GameClient] Can not process session property change for its id is not valid: " + details.SessionID);
        }
        #endregion Session Properties


        #region RPC
        internal void InvokeRPC(RPCInvokeData rpcData, Session session)
        {
            var methodName = rpcData.MethodName;
            var domPrms = new Dictionary<sbyte, DOMNode>();
            for (sbyte i = 0; i < rpcData.Parameters.Length; ++i)
                domPrms[i] = Extensions.ConvertToDOMNode(rpcData.Parameters[i]);

            var packet = new Packet(PacketType.RPC_CALL);
            packet.PRPCCall = new RPCCall(rpcData.Session.SessionID, methodName, domPrms);
            if (rpcData.Persistent)
                packet.PRPCCall.Cached = true;
            if (rpcData.ForMeToo)
                packet.PRPCCall.Receptors = new List<long>();
            else if (rpcData.Recepients != null)
            {
                packet.PRPCCall.Receptors = new List<long>();
                foreach (var player in rpcData.Recepients)
                    packet.PRPCCall.Receptors.Add(player.UserID);
            }

            SendStrictQuery(packet, response =>
            {
                if (!response.IsValid())
                    mAPI.Events.OnProblem.Fire("[GameClient] Could not invoke RPC", response.PException);
                else
                    session.QueueSelfRPC(response.PRPCCallResponse.RPCindex);
            });
        }
        #endregion RPC
    }
}
