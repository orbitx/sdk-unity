﻿using System.Collections.Generic;
using UnityEngine;

namespace OrbitXSDK
{
    internal struct RPCInvokeData
    {
        internal string MethodName;
        internal object[] Parameters;
        internal bool ForMeToo;
        internal bool Persistent;
        internal List<Player> Recepients;
        internal Player InvokeAs;
        internal Session Session;
    }
}
